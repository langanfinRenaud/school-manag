<?php
/**
 * Created by PhpStorm.
 * User: Muffy
 * Date: 30/09/2018
 * Time: 07:52
 */

namespace AppBundle\Service;

//use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface as Templating;
use Symfony\Component\DependencyInjection\Container as Container;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class Email
{

    //On crée deux variables qui vont récupérer les deux services appelé dans la définition du service owo_msp.email
    private $template;//POur récupérer le service templating
    private $mailer;//Pour récupérer le service mailer
    //Le constructeur initialise au départ les deux services et les stocke dans les deux variables créées ci-dessus
    public function __construct(Templating $template,\Swift_Mailer $mailer, Container $container)
    {
        $this->mailer = $mailer;// Le service Swift_Mailer
        $this->template = $template;//Le service templating
        $this->container = $container;
    }

    public function envoiEmail($sujet, $emailDestinataires, $template, $data = array())
    {
        //$emailExpediteur = $this->getParameter('mailer_user');//On récupère l'email de l'expéditeur
        $emailExpediteur = "admission@esgt-benin.com";
        //On crée un objet message du type Swift_Message
        $mes = $this->message = \Swift_Message::newInstance()
            ->setSubject($sujet)
            ->setFrom(array($emailExpediteur=>'ESGT-BENIN'))
            ->setTo($emailDestinataires)
            ->setBody(
                $this->template->render($template,$data),'text/html');
        $this->mailer->send($mes);
        //return $this->template->renderResponse('Message envoyé');
    }

    public function envoiLwsEmail($sujet, $emailDestinataires, $template, $data = array())
    {
        //$emailExpediteur = $this->getParameter('mailer_user');//On récupère l'email de l'expéditeur
        $emailExpediteur = "do_not_reply@mail.systempay.fr";
        //On crée un objet message du type Swift_Message
        $mes = $this->message = \Swift_Message::newInstance()
            ->setSubject($sujet)
            ->setFrom(array($emailExpediteur=>'Systempay'))
            ->setTo($emailDestinataires)
            ->setBody(
                $this->template->render($template,$data),'text/html');
        $this->mailer->send($mes);
        //return $this->template->renderResponse('Message envoyé');
    }
}