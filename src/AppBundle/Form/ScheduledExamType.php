<?php

namespace AppBundle\Form;

use AppBundle\Entity\Students;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScheduledExamType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'Start date',
                'required' => true,
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'End date',
                'required' => true,
            ))
            ->remove('notificationDate', DateType::class, array(
                'widget' => 'single_text',
                'html5' => false,
                'label' => 'Notification date',
                'required' => true,
            ))

        ->remove('exams', EntityType::class, array(
                'class' => 'AppBundle\Entity\Exams',
                'choice_label' => 'title',
            'multiple'=>true,
            'placeholder' => '',
                'required' => true
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ScheduledExam'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_scheduledexam';
    }


}
