<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'label' => 'Staff\'s First name',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Staff\'s Last name',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('state', TextType::class, array(
                'label' => 'Staff\'s State',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('location', TextType::class, array(
                'label' => 'Location',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('telephone', IntegerType::class, array(
                'label' => 'Staff\'s Telephone number',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('country', EntityType::class, array(
                'class' => 'AppBundle\Entity\Countries',
                'choice_label' => 'en',
                'required' => true,

                'placeholder' => 'Country of origin?'))
            ->add('wtelephone', IntegerType::class, array(
                'label' => 'Staff\'s Whatsapp number',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Staff\'s Email',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('fonction', ChoiceType::class, array(
                'choices' => array(
                    'Super Admin staff' => 'ROLE_SUPER_ADMIN',
                    'Admin staff' => 'ROLE_ADMIN',
                    'Agent' => 'ROLE_AGENT',
                    'Simple staff' => 'ROLE_SIMPLE'
                ),
                'required' => true,
                'placeholder' => 'Staff\'s Fontion (i.e what is there role)'
            ))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 1,
                    'Female' => 0
                ),
                'placeholder' => 'Staff gender'))
            ->add('salary', IntegerType::class, array(
                'label' => 'Staff\'s Salary or Wage',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('media', FileType::class, array('label' => 'Image', 'data_class' => null,'required' => false))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Staffs'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_staffs';
    }


}
