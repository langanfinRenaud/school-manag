<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LecturersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'label' => 'Lecturer\'s First name',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Lecturer\'s Last name',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('telephone', IntegerType::class, array(
                'label' => 'Lecturer\'s Telephone number',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Lecturer\'s Email',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('seniority', ChoiceType::class, array(
                'choices' => array(
                    'Less than 6 months' => 'Less than 6 months',
                    'More than 6 months' => 'More than 6 month',
                    'Over a year' => 'Over a year',
                ),
                'placeholder' => 'How long has this lecturer been in the school?',
                'required' => true,
//                'label_attr' => array('class' => 'mdl-textfield__input')
            ))
            ->add('hod', ChoiceType::class, array(
                'choices' => array(
                    'Yes' => 1,
                    'No' => 0
                ),
                'label' => 'Head of department',
                'placeholder' => ''))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 1,
                    'Female' => 0
                ),
                'placeholder' => ''))
            ->add('department', EntityType::class, array(
                'class' => 'AppBundle\Entity\Department',
                'choice_label' => 'title',
                'required' => false,
                'placeholder' => 'Attribute a department'));
//            ->add('created')
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Lecturers'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_lecturers';
    }


}
