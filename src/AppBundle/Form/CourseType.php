<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CourseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Course title *',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('code', TextType::class, array(
                'label' => 'Course code *',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('units', ChoiceType::class, array(
                'choices' => array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6
                ),
                'label' => 'Course Unit *',
                'placeholder' => 'How many unit is the course?'))
            ->add('level', EntityType::class, array(
                'class' => 'AppBundle\Entity\Levels',
                'choice_label' => 'title',
                'required' => true,
                'label' => 'Level *',
                'placeholder' => 'Attribute a level !'))
            ->add('department', EntityType::class, array(
                'class' => 'AppBundle\Entity\Department',
                'choice_label' => 'title',
                'required' => true,
                'label' => 'Department *',
                'placeholder' => 'Select a department !'))
            ->add('description', TextareaType::class,array(
                'label' => 'Description',
                'required' => false,
            ))

        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Course'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_course';
    }


}
