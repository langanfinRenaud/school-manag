<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Title of the event',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__input')
            ))
            ->add('eventDate', TextType::class, array(
                'label' => 'Event date',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__input')
            ))
            ->add('description', TextType::class, array(
                'label' => 'Title of the event',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__input')
            ))
            ->add('statut', TextType::class, array(
                'label' => 'Title of the event',
                'required' => false,
                'label_attr' => array('class' => 'mdl-textfield__input')
            ))
            ->add('media', FileType::class, array('label' => 'Image', 'data_class' => null))
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Events'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_events';
    }


}
