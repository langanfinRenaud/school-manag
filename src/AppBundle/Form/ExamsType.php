<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExamsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Title *',
                'label_attr' => array('class' => 'mdl-textfield__label'),
                'required' => true,))
//
            ->add('difficulty', ChoiceType::class, array(
                'choices' => array(
                    'Hard' => 'Hard',
                    'Medium' => 'Medium',
                    'Easy' => 'Easy'
                ),
                'label' => 'Difficulty level *',
                'required' => true,
                'placeholder' => ''))

            ->add('course', EntityType::class, array(
                'class' => 'AppBundle\Entity\Course',
                'choice_label' => 'title',
                'required' => false,
                'label' => 'Course',
                'placeholder' => ''))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Exams'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_exams';
    }


}
