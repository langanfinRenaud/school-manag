<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FAQType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('question', TextType::class, array(
                'label' => 'Question',
                'required' => true,
            ))
        // ->add('answer', TextareaType::class,array(
        //         'label' => 'Answer',
        //         'required' => true,
        //     ))
        ->add('type', ChoiceType::class, array(
                'choices' => array(
                    ''=> '',
                    'General' => 1,
                    'Student portal' => 2,
                    'Admission' => 3,
                ),
                'required' => true,
                'label' => 'Question Category'
            ))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FAQ'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_faq';
    }


}
