<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LibraryAssetsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Title',
                'required' => true,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('department', EntityType::class, array(
                'class' => 'AppBundle\Entity\Department',
                'choice_label' => 'title',
                'required' => true,
                'label' => 'Department *',
                'placeholder' => 'Select a department !'))
            ->add('assetType', ChoiceType::class, array(
                'choices' => array(
                    'PDF' => 'PDF',
                    'WordDOC' => 'DOC',
                    'PowerPoint' => 'PPTX',
//                    'Video' => 'Video'
                ),
                'label' => 'Asset type',
                'required' => true,
                'placeholder' => ''))
            ->add('authorName', TextType::class, array(
                'label' => 'Author Name',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            )
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\LibraryAssets'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_libraryassets';
    }


}
