<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'label' => 'Student\'s Other names',
                'required' => true,
            ))
            ->add('lastname', TextType::class, array(
                'label' => 'Student\'s Surname',
                'required' => true,
            ))
//            ->add('dateOfBirth', TextType::class, array(
//                'label' => 'Date of Birth',
//                'required' => true,
////                'label_attr' => array('class' => 'floating-label mdl-textfield__input')
//            ))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 1,
                    'Female' => 0
                ),
                'required' => true,
            ))
            ->add('admissionType', ChoiceType::class, array(
                'choices' => array(
                    'Regular Registration' => 'regular',
                    'Online Registration' => 'online'
                ),
                'required' => false,
                'placeholder' => 'Admission type'))
            ->add('levelOfEntry', ChoiceType::class, array(
                'choices' => array(
                    '100 Level' => '100',
                    '200 Level' => '200',
                    '300 Level' => '300',
                    '400 Level' => '400'
                ),
                'required' => true,
                'placeholder' => 'Student\'s level'))
            ->add('oLevelResult', ChoiceType::class, array(
                'choices' => array(
                    'WAEC' => 'WAEC',
                    'NECO' => 'NECO',
                    'GCE' => 'GCE',
                    'NABTEB' => 'NABTEB'
                ),
                'required' => false,
                'placeholder' => 'O\'Level Result'))
            ->add('noOfSitting', ChoiceType::class,[
                'choices' => array(
                    '1' => 1,
                    '2' => 2
                ),
                'required' => false,
                'placeholder' => 'No of sittings (How many times did the student take the exam)'
            ])
            ->add('admission', ChoiceType::class,[
                'choices' => array(
                    'Bachelor (Bsc)' => 0,
                    'Masters (Msc)' => 1
                ),
                'required' => false,
                'label' => 'Admission program',
                'placeholder' => 'Select degree program'
            ])

            ->add('nationality', EntityType::class, array(
                'class' => 'AppBundle\Entity\Countries',
                'choice_label' => 'en',
                'required' => false,

                'placeholder' => 'Country of origin?'))

            ->add('department', EntityType::class, array(
                'class' => 'AppBundle\Entity\Department',
                'choice_label' => 'title',
                'required' => true,

                'placeholder' => 'What department are they in'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Students'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_students';
    }


}
