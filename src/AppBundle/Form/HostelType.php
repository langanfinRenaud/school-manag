<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Repository\StudentsRepository;
class HostelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('student', EntityType::class, array(
                'class' => 'AppBundle\Entity\Students',
                'query_builder' => function(StudentsRepository $student) {
                    return $student->orderByDesc();
                },
                'choice_label' => function ($student){
                    return $student->getLastName().' '.$student->getFirstName().'('.$student->getMatricNo().')';
                },
                'required' => true,
                'label' => 'Student ',
                'placeholder' => 'Select a Student !'))
        ->add('bedNo', TextType::class, array(
                'label' => 'Bed code',
                'required' => true,
                
            ))
        ->add('room', ChoiceType::class, array(
                'choices' => array(
                    'Room 1' => '1',
                    'Room 2' => '2',
                    'Room 3' => '3',
                    'Room 4' => '4',
                ),
                'required' => true,
                'label' => 'Select room'
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Hostel'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_hostel';
    }


}
