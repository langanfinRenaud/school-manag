<?php

namespace AppBundle\Form;

use AppBundle\Repository\StudentsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArchiveType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('student', EntityType::class, array(
                'class' => 'AppBundle\Entity\Students',
                'query_builder' => function(StudentsRepository $student) {
                    return $student->orderByDesc();
                },
                'choice_label' => function ($student){
                    return $student->getLastName().' '.$student->getFirstName().'('.$student->getMatricNo().')';
                },
                'required' => true,
                'label' => 'Student ',
                'placeholder' => 'Select a Student !'))
            ->add('file', FileType::class, array('label' => 'File ', 'data_class' => null,'required' => true));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Archive'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_archive';
    }


}
