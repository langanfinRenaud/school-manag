<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('telephone', IntegerType::class, [
                'label' => 'Student\'s Telephone',
                'required' => true,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ])
            ->add('address', TextareaType::class, [
                'label' => 'Student\'s Home address',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ])
            ->add('email', EmailType::class, [
                'label' => 'Student\'s Email',
                'required' => true,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\StudentContact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_studentcontact';
    }


}
