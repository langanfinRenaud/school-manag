<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DepartmentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,array(
                'label' => 'Department Title *',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('departmentCode', TextType::class,array(
                'label' => 'Department Code *',
                'required' => true,
                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('description', TextareaType::class,array(
                'label' => 'Description',
                'required' => false,
            ))
            ->add('hasHod', ChoiceType::class, array(
                'choices' => array(
                    ''=> '',
                    'Yes' => 1,
                    'No' => 0,
                ),
                'required' => true,
                'label' => 'Does this department have an HOD *'
            ))
//            ->add('created')
//            ->add('createdBy')
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Department'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_department';
    }


}
