<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use AppBundle\Repository\StudentsRepository;
use AppBundle\Repository\FeesTypeRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FeesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amount', TextType::class, array(
                'label' => 'Amount to be paid',
                'required' => true,
            ))
            ->add('currency', ChoiceType::class, array(
                'choices' => array(
                    'Naira' => 'Naira',
                    'FCFA' => 'FCFA',
                ),
                'required' => true,
                'label' => 'Currency'
            ))
            ->add('type', EntityType::class, array(
                'class' => 'AppBundle\Entity\FeesType',
                'query_builder' => function (FeesTypeRepository $feesType) {
                    return $feesType->orderByDesc();
                },
                'choice_label' => function ($feesType) {
                    return $feesType->getTitle() ;
                },
                'required' => true,
                'label' => 'Type of fees'
            ))
            ->add('semester', ChoiceType::class, array(
                'choices' => array(
                    '1st semester' => '1st semester',
                    '2nd semester' => '2nd semester',
                    'Summer session' => 'Summer session',
                ),
                'required' => true,
                'label' => 'Select Semester'
            ))
            // ->add('createdBy')
            // ->add('createdOn')
            ->add('academicYear', TextType::class, array(
                'label' => 'Academic Year',
                'required' => true,
            ))
            ->add('student', EntityType::class, array(
                'class' => 'AppBundle\Entity\Students',
                'query_builder' => function (StudentsRepository $student) {
                    return $student->orderByDesc();
                },
                'choice_label' => function ($student) {
                    return $student->getLastName() . ' ' . $student->getFirstName() . '(' . $student->getMatricNo() . ')';
                },
                'required' => true,
                'label' => 'Student ',
                'placeholder' => 'Select a Student !'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Fees'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_fees';
    }


}
