<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentGuardianType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, array(
                'label' => 'Fullname ',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ))
            ->add('telephone',IntegerType::class,[
                'label' => 'Telephone',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ])
            ->add('email',EmailType::class,[
                'label' => 'Email ',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ])
            ->add('address',TextareaType::class,[
                'label' => 'Address ',
                'required' => false,
//                'label_attr' => array('class' => 'mdl-textfield__label')
            ])
            ->add('relationshipType',ChoiceType::class,[
                'choices' => array(
                    'Father' => 'Father',
                    'Mother' => 'Mother',
                    'Uncle' => 'Uncle',
                    'Aunty' => 'Aunty',
                    'Brother' => 'Brother',
                    'Sister' => 'Sister',
                    'Cousin' => 'Cousin',
                    'Husband' => 'Husband',
                    'Legal Tutor' => 'Legal Tutor'
                ),
                'required' => false,
                'label' => 'Relation to student ',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\StudentGuardian'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_studentguardian';
    }


}
