<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentSponsorship
 *
 * @ORM\Table(name="student_sponsorship")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentSponsorshipRepository")
 */
class StudentSponsorship
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sponsor;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Staffs")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sponsorAgent;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $admitted;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $paid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paidDate", type="datetime",nullable=true)
     */
    private $paidDate;
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->admitted = false;
        $this->paid = false;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set admitted.
     *
     * @param bool $admitted
     *
     * @return StudentSponsorship
     */
    public function setAdmitted($admitted)
    {
        $this->admitted = $admitted;

        return $this;
    }

    /**
     * Get admitted.
     *
     * @return bool
     */
    public function getAdmitted()
    {
        return $this->admitted;
    }

    /**
     * Set paid.
     *
     * @param bool $paid
     *
     * @return StudentSponsorship
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid.
     *
     * @return bool
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StudentSponsorship
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set paidDate.
     *
     * @param \DateTime|null $paidDate
     *
     * @return StudentSponsorship
     */
    public function setPaidDate($paidDate = null)
    {
        $this->paidDate = $paidDate;

        return $this;
    }

    /**
     * Get paidDate.
     *
     * @return \DateTime|null
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * Set sponsor.
     *
     * @param \AppBundle\Entity\Students|null $sponsor
     *
     * @return StudentSponsorship
     */
    public function setSponsor(\AppBundle\Entity\Students $sponsor = null)
    {
        $this->sponsor = $sponsor;

        return $this;
    }

    /**
     * Get sponsor.
     *
     * @return \AppBundle\Entity\Students|null
     */
    public function getSponsor()
    {
        return $this->sponsor;
    }

    /**
     * Set sponsorAgent.
     *
     * @param \AppBundle\Entity\Staffs|null $sponsorAgent
     *
     * @return StudentSponsorship
     */
    public function setSponsorAgent(\AppBundle\Entity\Staffs $sponsorAgent = null)
    {
        $this->sponsorAgent = $sponsorAgent;

        return $this;
    }

    /**
     * Get sponsorAgent.
     *
     * @return \AppBundle\Entity\Staffs|null
     */
    public function getSponsorAgent()
    {
        return $this->sponsorAgent;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return StudentSponsorship
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }
}
