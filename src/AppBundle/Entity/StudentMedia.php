<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentMedia
 *
 * @ORM\Table(name="student_media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentMediaRepository")
 */
class StudentMedia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students", inversedBy="media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkphoto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $certificate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkcertificate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $birthcertificate;

    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photo.
     *
     * @param string $photo
     *
     * @return StudentMedia
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set linkphoto.
     *
     * @param string|null $linkphoto
     *
     * @return StudentMedia
     */
    public function setLinkphoto($linkphoto = null)
    {
        $this->linkphoto = $linkphoto;

        return $this;
    }

    /**
     * Get linkphoto.
     *
     * @return string|null
     */
    public function getLinkphoto()
    {
        return $this->linkphoto;
    }

    /**
     * Set certificate.
     *
     * @param string $certificate
     *
     * @return StudentMedia
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * Get certificate.
     *
     * @return string
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * Set linkcertificate.
     *
     * @param string|null $linkcertificate
     *
     * @return StudentMedia
     */
    public function setLinkcertificate($linkcertificate = null)
    {
        $this->linkcertificate = $linkcertificate;

        return $this;
    }

    /**
     * Get linkcertificate.
     *
     * @return string|null
     */
    public function getLinkcertificate()
    {
        return $this->linkcertificate;
    }

    /**
     * Set birthcertificate.
     *
     * @param string|null $birthcertificate
     *
     * @return StudentMedia
     */
    public function setBirthcertificate($birthcertificate = null)
    {
        $this->birthcertificate = $birthcertificate;

        return $this;
    }

    /**
     * Get birthcertificate.
     *
     * @return string|null
     */
    public function getBirthcertificate()
    {
        return $this->birthcertificate;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return StudentMedia
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }
}
