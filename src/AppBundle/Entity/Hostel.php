<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hostel
 *
 * @ORM\Table(name="hostel")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HostelRepository")
 */
class Hostel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students", inversedBy="media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @var string
     *
     * @ORM\Column(name="bedNo", type="string", length=100)
     */
    private $bedNo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="room", type="integer", nullable=true)
     */
    private $room;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }
   

   
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bedNo.
     *
     * @param string $bedNo
     *
     * @return Hostel
     */
    public function setBedNo($bedNo)
    {
        $this->bedNo = $bedNo;

        return $this;
    }

    /**
     * Get bedNo.
     *
     * @return string
     */
    public function getBedNo()
    {
        return $this->bedNo;
    }

    /**
     * Set room.
     *
     * @param int|null $room
     *
     * @return Hostel
     */
    public function setRoom($room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room.
     *
     * @return int|null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Hostel
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return Hostel
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }
}
