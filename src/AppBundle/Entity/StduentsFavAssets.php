<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StduentsFavAssets
 *
 * @ORM\Table(name="stduents_fav_assets")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StduentsFavAssetsRepository")
 */
class StduentsFavAssets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\LibraryAssets", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $asset;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StduentsFavAssets
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return StduentsFavAssets
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set asset.
     *
     * @param \AppBundle\Entity\LibraryAssets $asset
     *
     * @return StduentsFavAssets
     */
    public function setAsset(\AppBundle\Entity\LibraryAssets $asset)
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * Get asset.
     *
     * @return \AppBundle\Entity\LibraryAssets
     */
    public function getAsset()
    {
        return $this->asset;
    }
}
