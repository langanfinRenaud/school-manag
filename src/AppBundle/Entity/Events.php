<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventsRepository")
 */
class Events
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $language;

    /**
     * @ORM\Column(type="date")
     */
    private $eventStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $eventEndDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statut;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categories", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $media;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mediaLink;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifieddate;
    /**
     * @ORM\Column(type="datetime")
     */
    private $created;
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->statut = true;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Events
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set language.
     *
     * @param string $language
     *
     * @return Events
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set eventStartDate.
     *
     * @param \DateTime $eventStartDate
     *
     * @return Events
     */
    public function setEventStartDate($eventStartDate)
    {
        $this->eventStartDate = $eventStartDate;

        return $this;
    }

    /**
     * Get eventStartDate.
     *
     * @return \DateTime
     */
    public function getEventStartDate()
    {
        return $this->eventStartDate;
    }

    /**
     * Set eventEndDate.
     *
     * @param \DateTime|null $eventEndDate
     *
     * @return Events
     */
    public function setEventEndDate($eventEndDate = null)
    {
        $this->eventEndDate = $eventEndDate;

        return $this;
    }

    /**
     * Get eventEndDate.
     *
     * @return \DateTime|null
     */
    public function getEventEndDate()
    {
        return $this->eventEndDate;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Events
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set statut.
     *
     * @param bool|null $statut
     *
     * @return Events
     */
    public function setStatut($statut = null)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut.
     *
     * @return bool|null
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set media.
     *
     * @param string|null $media
     *
     * @return Events
     */
    public function setMedia($media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media.
     *
     * @return string|null
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set mediaLink.
     *
     * @param string|null $mediaLink
     *
     * @return Events
     */
    public function setMediaLink($mediaLink = null)
    {
        $this->mediaLink = $mediaLink;

        return $this;
    }

    /**
     * Get mediaLink.
     *
     * @return string|null
     */
    public function getMediaLink()
    {
        return $this->mediaLink;
    }

    /**
     * Set modifieddate.
     *
     * @param \DateTime|null $modifieddate
     *
     * @return Events
     */
    public function setModifieddate($modifieddate = null)
    {
        $this->modifieddate = $modifieddate;

        return $this;
    }

    /**
     * Get modifieddate.
     *
     * @return \DateTime|null
     */
    public function getModifieddate()
    {
        return $this->modifieddate;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Events
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set category.
     *
     * @param \AppBundle\Entity\Categories|null $category
     *
     * @return Events
     */
    public function setCategory(\AppBundle\Entity\Categories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return \AppBundle\Entity\Categories|null
     */
    public function getCategory()
    {
        return $this->category;
    }
}
