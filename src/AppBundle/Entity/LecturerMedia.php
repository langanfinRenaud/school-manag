<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LecturerMedia
 *
 * @ORM\Table(name="lecturer_media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LecturerMediaRepository")
 */
class LecturerMedia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Lecturers", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lecturer;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename.
     *
     * @param string $filename
     *
     * @return LecturerMedia
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename.
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return LecturerMedia
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set lecturer.
     *
     * @param \AppBundle\Entity\Lecturers $lecturer
     *
     * @return LecturerMedia
     */
    public function setLecturer(\AppBundle\Entity\Lecturers $lecturer)
    {
        $this->lecturer = $lecturer;

        return $this;
    }

    /**
     * Get lecturer.
     *
     * @return \AppBundle\Entity\Lecturers
     */
    public function getLecturer()
    {
        return $this->lecturer;
    }
}
