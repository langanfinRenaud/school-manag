<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Students
 *
 * @ORM\Table(name="students")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentsRepository")
 */
class Students
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $matricNo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $department;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $dateOfBirth;
    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $noOfSitting;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Countries", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $nationality;

    /**
     * @ORM\Column(type="string", length=50,nullable=true)
     */
    private $admissionType;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $oLevelResult;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $levelOfEntry;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $level;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $waiting;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasAccount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $admission;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isdeleted;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear", cascade={"persist"})
     */
    private $academicYear;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudentMedia", mappedBy="student", orphanRemoval=true)
     */
    private $media;
    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return Students
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return Students
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set matricNo.
     *
     * @param string|null $matricNo
     *
     * @return Students
     */
    public function setMatricNo($matricNo = null)
    {
        $this->matricNo = $matricNo;

        return $this;
    }

    /**
     * Get matricNo.
     *
     * @return string|null
     */
    public function getMatricNo()
    {
        return $this->matricNo;
    }

    /**
     * Set dateOfBirth.
     *
     * @param \DateTime|null $dateOfBirth
     *
     * @return Students
     */
    public function setDateOfBirth($dateOfBirth = null)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth.
     *
     * @return \DateTime|null
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set gender.
     *
     * @param bool|null $gender
     *
     * @return Students
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return bool|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set noOfSitting.
     *
     * @param string|null $noOfSitting
     *
     * @return Students
     */
    public function setNoOfSitting($noOfSitting = null)
    {
        $this->noOfSitting = $noOfSitting;

        return $this;
    }

    /**
     * Get noOfSitting.
     *
     * @return string|null
     */
    public function getNoOfSitting()
    {
        return $this->noOfSitting;
    }

    /**
     * Set admissionType.
     *
     * @param string|null $admissionType
     *
     * @return Students
     */
    public function setAdmissionType($admissionType = null)
    {
        $this->admissionType = $admissionType;

        return $this;
    }

    /**
     * Get admissionType.
     *
     * @return string|null
     */
    public function getAdmissionType()
    {
        return $this->admissionType;
    }

    /**
     * Set oLevelResult.
     *
     * @param string|null $oLevelResult
     *
     * @return Students
     */
    public function setOLevelResult($oLevelResult = null)
    {
        $this->oLevelResult = $oLevelResult;

        return $this;
    }

    /**
     * Get oLevelResult.
     *
     * @return string|null
     */
    public function getOLevelResult()
    {
        return $this->oLevelResult;
    }

    /**
     * Set levelOfEntry.
     *
     * @param string $levelOfEntry
     *
     * @return Students
     */
    public function setLevelOfEntry($levelOfEntry)
    {
        $this->levelOfEntry = $levelOfEntry;

        return $this;
    }

    /**
     * Get levelOfEntry.
     *
     * @return string
     */
    public function getLevelOfEntry()
    {
        return $this->levelOfEntry;
    }

    /**
     * Set level.
     *
     * @param string|null $level
     *
     * @return Students
     */
    public function setLevel($level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return string|null
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set waiting.
     *
     * @param bool|null $waiting
     *
     * @return Students
     */
    public function setWaiting($waiting = null)
    {
        $this->waiting = $waiting;

        return $this;
    }

    /**
     * Get waiting.
     *
     * @return bool|null
     */
    public function getWaiting()
    {
        return $this->waiting;
    }

    /**
     * Set hasAccount.
     *
     * @param bool|null $hasAccount
     *
     * @return Students
     */
    public function setHasAccount($hasAccount = null)
    {
        $this->hasAccount = $hasAccount;

        return $this;
    }

    /**
     * Get hasAccount.
     *
     * @return bool|null
     */
    public function getHasAccount()
    {
        return $this->hasAccount;
    }

    /**
     * Set admission.
     *
     * @param bool|null $admission
     *
     * @return Students
     */
    public function setAdmission($admission = null)
    {
        $this->admission = $admission;

        return $this;
    }

    /**
     * Get admission.
     *
     * @return bool|null
     */
    public function getAdmission()
    {
        return $this->admission;
    }

    /**
     * Set isdeleted.
     *
     * @param bool|null $isdeleted
     *
     * @return Students
     */
    public function setIsdeleted($isdeleted = null)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted.
     *
     * @return bool|null
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Students
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set department.
     *
     * @param \AppBundle\Entity\Department|null $department
     *
     * @return Students
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department.
     *
     * @return \AppBundle\Entity\Department|null
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set nationality.
     *
     * @param \AppBundle\Entity\Countries|null $nationality
     *
     * @return Students
     */
    public function setNationality(\AppBundle\Entity\Countries $nationality = null)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get nationality.
     *
     * @return \AppBundle\Entity\Countries|null
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return Students
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }

    /**
     * Add medium.
     *
     * @param \AppBundle\Entity\StudentMedia $medium
     *
     * @return Students
     */
    public function addMedia(\AppBundle\Entity\StudentMedia $medium)
    {
        $this->media[] = $medium;

        return $this;
    }

    /**
     * Remove medium.
     *
     * @param \AppBundle\Entity\StudentMedia $medium
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMedia(\AppBundle\Entity\StudentMedia $medium)
    {
        return $this->media->removeElement($medium);
    }

    /**
     * Get media.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedia()
    {
        return $this->media;
    }
}
