<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduledExam
 *
 * @ORM\Table(name="scheduled_exam")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ScheduledExamRepository")
 */
class ScheduledExam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     */
    private $student;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Exams")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $exams;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="notificationDate", type="date",nullable=true)
     */
    private $notificationDate;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear")
     */
    private $academicYear;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="attemptDate", type="datetime",nullable=true)
     */
    private $attemptDate;
    /**
     *
     * @ORM\Column(name="isdeleted", type="boolean",nullable=true)
     */
    private $isdeleted;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->isdeleted = 0;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set examDate.
     *
     * @param \DateTime $examDate
     *
     * @return ScheduledExam
     */
    public function setExamDate($examDate)
    {
        $this->examDate = $examDate;

        return $this;
    }

    /**
     * Get examDate.
     *
     * @return \DateTime
     */
    public function getExamDate()
    {
        return $this->examDate;
    }

    /**
     * Set notificationDate.
     *
     * @param \DateTime $notificationDate
     *
     * @return ScheduledExam
     */
    public function setNotificationDate($notificationDate)
    {
        $this->notificationDate = $notificationDate;

        return $this;
    }

    /**
     * Get notificationDate.
     *
     * @return \DateTime
     */
    public function getNotificationDate()
    {
        return $this->notificationDate;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ScheduledExam
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students|null $student
     *
     * @return ScheduledExam
     */
    public function setStudent(\AppBundle\Entity\Students $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set exams.
     *
     * @param \AppBundle\Entity\Exams|null $exams
     *
     * @return ScheduledExam
     */
    public function setExams(\AppBundle\Entity\Exams $exams = null)
    {
        $this->exams = $exams;

        return $this;
    }

    /**
     * Get exams.
     *
     * @return \AppBundle\Entity\Exams|null
     */
    public function getExams()
    {
        return $this->exams;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return ScheduledExam
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return ScheduledExam
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return ScheduledExam
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set isdeleted.
     *
     * @param bool|null $isdeleted
     *
     * @return ScheduledExam
     */
    public function setIsdeleted($isdeleted = null)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted.
     *
     * @return bool|null
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set attemptDate.
     *
     * @param \DateTime $attemptDate
     *
     * @return ScheduledExam
     */
    public function setAttemptDate($attemptDate)
    {
        $this->attemptDate = $attemptDate;

        return $this;
    }

    /**
     * Get attemptDate.
     *
     * @return \DateTime
     */
    public function getAttemptDate()
    {
        return $this->attemptDate;
    }
}
