<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fees
 *
 * @ORM\Table(name="fees")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeesRepository")
 */
class Fees
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students", inversedBy="media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;



    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FeesType",)
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id", name="type")
     */

    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string")
     */

    private $semester;

    /**
     * @var string
     *
     * @ORM\Column(name="invoiceNo", type="string")
     */

    private $invoiceNo;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(name="totalPaid", type="integer", nullable=true)
     */
    private $totalPaid;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=30)
     */
    private $currency;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="createdBy", type="string", length=100)
     */
    private $createdBy;

    /**
     * @var int
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=true)
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdOn", type="datetime")
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedOn", type="datetime", nullable=true)
     */
    private $modifiedOn;

    /**
     * @var string
     *
     * @ORM\Column(name="academicYear", type="string", nullable=true)
     */
    private $academicYear;

    public function __construct()
    {
        $this->createdOn = new \DateTime();
        $this->status = FALSE;
        $this->invoiceNo = "ESGT" . substr(mt_rand(), 0, 5);

    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set semester.
     *
     * @param string $semester
     *
     * @return Fees
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester.
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set invoiceNo.
     *
     * @param string $invoiceNo
     *
     * @return Fees
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;

        return $this;
    }

    /**
     * Get invoiceNo.
     *
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * Set amount.
     *
     * @param int $amount
     *
     * @return Fees
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set totalPaid.
     *
     * @param int|null $totalPaid
     *
     * @return Fees
     */
    public function setTotalPaid($totalPaid = null)
    {
        $this->totalPaid = $totalPaid;

        return $this;
    }

    /**
     * Get totalPaid.
     *
     * @return int|null
     */
    public function getTotalPaid()
    {
        return $this->totalPaid;
    }

    /**
     * Set currency.
     *
     * @param string $currency
     *
     * @return Fees
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set status.
     *
     * @param bool $status
     *
     * @return Fees
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Fees
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedBy.
     *
     * @param string|null $modifiedBy
     *
     * @return Fees
     */
    public function setModifiedBy($modifiedBy = null)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy.
     *
     * @return string|null
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set createdOn.
     *
     * @param \DateTime $createdOn
     *
     * @return Fees
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn.
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set modifiedOn.
     *
     * @param \DateTime|null $modifiedOn
     *
     * @return Fees
     */
    public function setModifiedOn($modifiedOn = null)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn.
     *
     * @return \DateTime|null
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Set academicYear.
     *
     * @param string|null $academicYear
     *
     * @return Fees
     */
    public function setAcademicYear($academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return string|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return Fees
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set type.
     *
     * @param \AppBundle\Entity\FeesType|null $type
     *
     * @return Fees
     */
    public function setType(\AppBundle\Entity\FeesType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return \AppBundle\Entity\FeesType|null
     */
    public function getType()
    {
        return $this->type;
    }
}
