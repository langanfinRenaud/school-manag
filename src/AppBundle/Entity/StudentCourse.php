<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentCourse
 *
 * @ORM\Table(name="student_course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentCourseRepository")
 */
class StudentCourse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $course;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear")
     */
    private $academicYear;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime", length=255)
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }
 
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StudentCourse
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students|null $student
     *
     * @return StudentCourse
     */
    public function setStudent(\AppBundle\Entity\Students $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students|null
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set course.
     *
     * @param \AppBundle\Entity\Course|null $course
     *
     * @return StudentCourse
     */
    public function setCourse(\AppBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course.
     *
     * @return \AppBundle\Entity\Course|null
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return StudentCourse
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }
}
