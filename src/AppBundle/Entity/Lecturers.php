<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lecturers
 *
 * @ORM\Table(name="lecturers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LecturersRepository")
 */
class Lecturers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="integer")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $seniority;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hod;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasAccount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeleted= false;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department")
     * @ORM\JoinColumn(nullable=true)
     */
    private $department;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;


    public function __construct()
    {
        $this->created = new \DateTime();
        $rand= substr(str_shuffle('0123456789'), 0, 6);
        $this->code= 'LECT-'.$rand;
    }
   
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return Lecturers
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return Lecturers
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set telephone.
     *
     * @param int $telephone
     *
     * @return Lecturers
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Lecturers
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set seniority.
     *
     * @param string|null $seniority
     *
     * @return Lecturers
     */
    public function setSeniority($seniority = null)
    {
        $this->seniority = $seniority;

        return $this;
    }

    /**
     * Get seniority.
     *
     * @return string|null
     */
    public function getSeniority()
    {
        return $this->seniority;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Lecturers
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set gender.
     *
     * @param bool|null $gender
     *
     * @return Lecturers
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return bool|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set hod.
     *
     * @param bool|null $hod
     *
     * @return Lecturers
     */
    public function setHod($hod = null)
    {
        $this->hod = $hod;

        return $this;
    }

    /**
     * Get hod.
     *
     * @return bool|null
     */
    public function getHod()
    {
        return $this->hod;
    }

    /**
     * Set hasAccount.
     *
     * @param bool|null $hasAccount
     *
     * @return Lecturers
     */
    public function setHasAccount($hasAccount = null)
    {
        $this->hasAccount = $hasAccount;

        return $this;
    }

    /**
     * Get hasAccount.
     *
     * @return bool|null
     */
    public function getHasAccount()
    {
        return $this->hasAccount;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Lecturers
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set department.
     *
     * @param \AppBundle\Entity\Department|null $department
     *
     * @return Lecturers
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department.
     *
     * @return \AppBundle\Entity\Department|null
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set isDeleted.
     *
     * @param bool|null $isDeleted
     *
     * @return Lecturers
     */
    public function setIsDeleted($isDeleted = null)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted.
     *
     * @return bool|null
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
}
