<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamAnswers
 *
 * @ORM\Table(name="exam_answers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamAnswersRepository")
 */
class ExamAnswers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Exams")
     * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
     */
    private $exam;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ExamQuestion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $answer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isanswer;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer.
     *
     * @param string $answer
     *
     * @return ExamAnswers
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set isanswer.
     *
     * @param bool|null $isanswer
     *
     * @return ExamAnswers
     */
    public function setIsanswer($isanswer = null)
    {
        $this->isanswer = $isanswer;

        return $this;
    }

    /**
     * Get isanswer.
     *
     * @return bool|null
     */
    public function getIsanswer()
    {
        return $this->isanswer;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ExamAnswers
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set exam.
     *
     * @param \AppBundle\Entity\Exams $exam
     *
     * @return ExamAnswers
     */
    public function setExam(\AppBundle\Entity\Exams $exam)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam.
     *
     * @return \AppBundle\Entity\Exams
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set question.
     *
     * @param \AppBundle\Entity\ExamQuestion $question
     *
     * @return ExamAnswers
     */
    public function setQuestion(\AppBundle\Entity\ExamQuestion $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \AppBundle\Entity\ExamQuestion
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
