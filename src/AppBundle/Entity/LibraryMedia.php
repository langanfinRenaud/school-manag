<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LibraryMedia
 *
 * @ORM\Table(name="library_media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LibraryMediaRepository")
 */
class LibraryMedia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $alt;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\LibraryAssets", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $asset;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alt.
     *
     * @param string $alt
     *
     * @return LibraryMedia
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt.
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set asset.
     *
     * @param \AppBundle\Entity\LibraryAssets $asset
     *
     * @return LibraryMedia
     */
    public function setAsset(\AppBundle\Entity\LibraryAssets $asset)
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * Get asset.
     *
     * @return \AppBundle\Entity\LibraryAssets
     */
    public function getAsset()
    {
        return $this->asset;
    }
}
