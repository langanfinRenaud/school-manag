<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payments
 *
 * @ORM\Table(name="payments")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentsRepository")
 */
class Payments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fees", inversedBy="media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $theFee;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=30)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=30)
     */
    private $reference;

    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", nullable=true)
     */
    private $validated;
    
    /**
     * @var int
     *
     * @ORM\Column(name="createdBy",type="string", length=100)
     */
    private $createdBy;
    
    /**
     * @var int
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=true)
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedOn", type="datetime", nullable=true)
     */
    private $modifiedOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->validated = FALSE;
        $this->reference= substr(mt_rand(), 0, 8);

    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount.
     *
     * @param int $amount
     *
     * @return Payments
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency.
     *
     * @param string $currency
     *
     * @return Payments
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set reference.
     *
     * @param string $reference
     *
     * @return Payments
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set validated.
     *
     * @param bool|null $validated
     *
     * @return Payments
     */
    public function setValidated($validated = null)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated.
     *
     * @return bool|null
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Payments
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedBy.
     *
     * @param string|null $modifiedBy
     *
     * @return Payments
     */
    public function setModifiedBy($modifiedBy = null)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy.
     *
     * @return string|null
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set modifiedOn.
     *
     * @param \DateTime|null $modifiedOn
     *
     * @return Payments
     */
    public function setModifiedOn($modifiedOn = null)
    {
        $this->modifiedOn = $modifiedOn;

        return $this;
    }

    /**
     * Get modifiedOn.
     *
     * @return \DateTime|null
     */
    public function getModifiedOn()
    {
        return $this->modifiedOn;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Payments
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set theFee.
     *
     * @param \AppBundle\Entity\Fees $theFee
     *
     * @return Payments
     */
    public function setTheFee(\AppBundle\Entity\Fees $theFee)
    {
        $this->theFee = $theFee;

        return $this;
    }

    /**
     * Get theFee.
     *
     * @return \AppBundle\Entity\Fees
     */
    public function getTheFee()
    {
        return $this->theFee;
    }
}
