<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScoreSheet
 *
 * @ORM\Table(name="score_sheet")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ScoreSheetRepository")
 */
class ScoreSheet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @var int|null
     *
     *  @ORM\Column(type="integer")
     */
    private $test = 0;

    /**
     * @var int|null
     *
     *  @ORM\Column(type="integer")
     */
    private $attendance = 0;

    /**
     * @var int|null
     *
     *  @ORM\Column(type="integer")
     */
    private $assignment = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $exam = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $total = 0;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear")
     */
    private $academicYear;

    /**
     *  @ORM\Column(type="integer")
     */
    private $semester;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
//        $obj = new \DateTime();
//        $year = $obj->format('Y');
//        $this->academicYear = ($year-1).'/'. $year;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set test.
     *
     * @param int $test
     *
     * @return ScoreSheet
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test.
     *
     * @return int
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set attendance.
     *
     * @param int $attendance
     *
     * @return ScoreSheet
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;

        return $this;
    }

    /**
     * Get attendance.
     *
     * @return int
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * Set assignment.
     *
     * @param int $assignment
     *
     * @return ScoreSheet
     */
    public function setAssignment($assignment)
    {
        $this->assignment = $assignment;

        return $this;
    }

    /**
     * Get assignment.
     *
     * @return int
     */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * Set exam.
     *
     * @param int $exam
     *
     * @return ScoreSheet
     */
    public function setExam($exam)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam.
     *
     * @return int
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set total.
     *
     * @param int $total
     *
     * @return ScoreSheet
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set semester.
     *
     * @param int $semester
     *
     * @return ScoreSheet
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester.
     *
     * @return int
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ScoreSheet
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set student.
     *
     * @param \AppBundle\Entity\Students $student
     *
     * @return ScoreSheet
     */
    public function setStudent(\AppBundle\Entity\Students $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set course.
     *
     * @param \AppBundle\Entity\Course $course
     *
     * @return ScoreSheet
     */
    public function setCourse(\AppBundle\Entity\Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course.
     *
     * @return \AppBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return ScoreSheet
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }

    /**
     * Set createdBy.
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return ScoreSheet
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
