<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExamQuestion
 *
 * @ORM\Table(name="exam_question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamQuestionRepository")
 */
class ExamQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Exams")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $exam;

    /**
     * @ORM\Column(type="text")
     */
    private $question;

    /**
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isdeleted;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->isdeleted = 0;
        $this->ref= 'EQ-'.strtoupper(substr(str_shuffle(MD5(microtime())), 0, 6));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question.
     *
     * @param string $question
     *
     * @return ExamQuestion
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set points.
     *
     * @param int $points
     *
     * @return ExamQuestion
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points.
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set duration.
     *
     * @param int $duration
     *
     * @return ExamQuestion
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return ExamQuestion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return ExamQuestion
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set exam.
     *
     * @param \AppBundle\Entity\Exams $exam
     *
     * @return ExamQuestion
     */
    public function setExam(\AppBundle\Entity\Exams $exam)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam.
     *
     * @return \AppBundle\Entity\Exams
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set isdeleted.
     *
     * @param bool|null $isdeleted
     *
     * @return ExamQuestion
     */
    public function setIsdeleted($isdeleted = null)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted.
     *
     * @return bool|null
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set ref.
     *
     * @param string|null $ref
     *
     * @return ExamQuestion
     */
    public function setRef($ref = null)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref.
     *
     * @return string|null
     */
    public function getRef()
    {
        return $this->ref;
    }
}
