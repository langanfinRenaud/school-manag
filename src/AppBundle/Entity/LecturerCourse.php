<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LecturerCourse
 *
 * @ORM\Table(name="lecturer_course")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LecturerCourseRepository")
 */
class LecturerCourse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Lecturers")
     */
    private $lecturers;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $courses;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasScoreSheet;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear")
     */
    private $academicYear;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assignedDate", type="datetime")
     */
    private $assignedDate;

    public function __construct()
    {
        $this->assignedDate = new \DateTime();
    }


    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hasScoreSheet.
     *
     * @param bool|null $hasScoreSheet
     *
     * @return LecturerCourse
     */
    public function setHasScoreSheet($hasScoreSheet = null)
    {
        $this->hasScoreSheet = $hasScoreSheet;

        return $this;
    }

    /**
     * Get hasScoreSheet.
     *
     * @return bool|null
     */
    public function getHasScoreSheet()
    {
        return $this->hasScoreSheet;
    }

    /**
     * Set assignedDate.
     *
     * @param \DateTime $assignedDate
     *
     * @return LecturerCourse
     */
    public function setAssignedDate($assignedDate)
    {
        $this->assignedDate = $assignedDate;

        return $this;
    }

    /**
     * Get assignedDate.
     *
     * @return \DateTime
     */
    public function getAssignedDate()
    {
        return $this->assignedDate;
    }

    /**
     * Set lecturers.
     *
     * @param \AppBundle\Entity\Lecturers|null $lecturers
     *
     * @return LecturerCourse
     */
    public function setLecturers(\AppBundle\Entity\Lecturers $lecturers = null)
    {
        $this->lecturers = $lecturers;

        return $this;
    }

    /**
     * Get lecturers.
     *
     * @return \AppBundle\Entity\Lecturers|null
     */
    public function getLecturers()
    {
        return $this->lecturers;
    }

    /**
     * Set courses.
     *
     * @param \AppBundle\Entity\Course|null $courses
     *
     * @return LecturerCourse
     */
    public function setCourses(\AppBundle\Entity\Course $courses = null)
    {
        $this->courses = $courses;

        return $this;
    }

    /**
     * Get courses.
     *
     * @return \AppBundle\Entity\Course|null
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return LecturerCourse
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }
}
