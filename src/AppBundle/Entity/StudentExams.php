<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentExams
 *
 * @ORM\Table(name="student_exams")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentExamsRepository")
 */
class StudentExams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Exams")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $exam;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="carryOver", type="boolean", nullable=true)
     */
    private $carryOver;

    /**
     * @var int|null
     *
     * @ORM\Column(name="noOfCarryOver", type="integer", nullable=true)
     */
    private $noOfCarryOver;


    /**
     * @var int|null
     *
     * @ORM\Column(name="studentpoint", type="integer", nullable=true)
     */
    private $studentpoint;


    /**
     * @var int|null
     *
     * @ORM\Column(name="totalpoint", type="integer", nullable=true)
     */
    private $totalpoint;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AcademicYear")
     */
    private $academicYear;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carryOver.
     *
     * @param bool|null $carryOver
     *
     * @return StudentExams
     */
    public function setCarryOver($carryOver = null)
    {
        $this->carryOver = $carryOver;

        return $this;
    }

    /**
     * Get carryOver.
     *
     * @return bool|null
     */
    public function getCarryOver()
    {
        return $this->carryOver;
    }

    /**
     * Set noOfCarryOver.
     *
     * @param int|null $noOfCarryOver
     *
     * @return StudentExams
     */
    public function setNoOfCarryOver($noOfCarryOver = null)
    {
        $this->noOfCarryOver = $noOfCarryOver;

        return $this;
    }

    /**
     * Get noOfCarryOver.
     *
     * @return int|null
     */
    public function getNoOfCarryOver()
    {
        return $this->noOfCarryOver;
    }

    /**
     * Set students.
     *
     * @param \AppBundle\Entity\Students $students
     *
     * @return StudentExams
     */
    public function setStudents(\AppBundle\Entity\Students $students)
    {
        $this->students = $students;

        return $this;
    }

    /**
     * Get students.
     *
     * @return \AppBundle\Entity\Students
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Set exam.
     *
     * @param \AppBundle\Entity\Exams $exam
     *
     * @return StudentExams
     */
    public function setExam(\AppBundle\Entity\Exams $exam)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam.
     *
     * @return \AppBundle\Entity\Exams
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set studentpoint.
     *
     * @param int|null $studentpoint
     *
     * @return StudentExams
     */
    public function setStudentpoint($studentpoint = null)
    {
        $this->studentpoint = $studentpoint;

        return $this;
    }

    /**
     * Get studentpoint.
     *
     * @return int|null
     */
    public function getStudentpoint()
    {
        return $this->studentpoint;
    }

    /**
     * Set totalpoint.
     *
     * @param int|null $totalpoint
     *
     * @return StudentExams
     */
    public function setTotalpoint($totalpoint = null)
    {
        $this->totalpoint = $totalpoint;

        return $this;
    }

    /**
     * Get totalpoint.
     *
     * @return int|null
     */
    public function getTotalpoint()
    {
        return $this->totalpoint;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return StudentExams
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set academicYear.
     *
     * @param \AppBundle\Entity\AcademicYear|null $academicYear
     *
     * @return StudentExams
     */
    public function setAcademicYear(\AppBundle\Entity\AcademicYear $academicYear = null)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear.
     *
     * @return \AppBundle\Entity\AcademicYear|null
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }
}
