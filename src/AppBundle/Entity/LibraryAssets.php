<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LibraryAssets
 *
 * @ORM\Table(name="library_assets")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LibraryAssetsRepository")
 */
class LibraryAssets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=150)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $color;

    /**
     * @ORM\Column(type="date")
     */
    private $datePublished;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department")
     * @ORM\JoinColumn(nullable=true)
     */
    private $department;
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $publishedBy;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $assetType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $authorName;
    /**
     * @ORM\Column(type="datetime")
     */
    private $created;
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->code = '#' . substr(str_shuffle(MD5(microtime())), 0, 4);;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return LibraryAssets
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return LibraryAssets
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return LibraryAssets
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set datePublished
     *
     * @param \DateTime $datePublished
     *
     * @return LibraryAssets
     */
    public function setDatePublished($datePublished)
    {
        $this->datePublished = $datePublished;

        return $this;
    }

    /**
     * Get datePublished
     *
     * @return \DateTime
     */
    public function getDatePublished()
    {
        return $this->datePublished;
    }

    /**
     * Set publishedBy
     *
     * @param integer $publishedBy
     *
     * @return LibraryAssets
     */
    public function setPublishedBy($publishedBy)
    {
        $this->publishedBy = $publishedBy;

        return $this;
    }

    /**
     * Get publishedBy
     *
     * @return integer
     */
    public function getPublishedBy()
    {
        return $this->publishedBy;
    }

    /**
     * Set assetType
     *
     * @param string $assetType
     *
     * @return LibraryAssets
     */
    public function setAssetType($assetType)
    {
        $this->assetType = $assetType;

        return $this;
    }

    /**
     * Get assetType
     *
     * @return string
     */
    public function getAssetType()
    {
        return $this->assetType;
    }

    /**
     * Set authorName
     *
     * @param string $authorName
     *
     * @return LibraryAssets
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * Get authorName
     *
     * @return string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return LibraryAssets
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set department.
     *
     * @param \AppBundle\Entity\Department|null $department
     *
     * @return LibraryAssets
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department.
     *
     * @return \AppBundle\Entity\Department|null
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
