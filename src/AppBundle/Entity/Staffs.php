<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Staffs
 *
 * @ORM\Table(name="staffs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StaffsRepository")
 */
class Staffs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string",length=100)
     */
    private $wtelephone;

    /**
     * @ORM\Column(type="string",length=100, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $salary;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fonction;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $media;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasAccount;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Countries")
     * @ORM\JoinColumn(nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;
    public function __construct()
    {
        $rand= substr(str_shuffle('0123456789'), 0, 5);
        $this->code= 'STAFF-'.$rand;
        $this->created = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return Staffs
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return Staffs
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Staffs
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set wtelephone.
     *
     * @param string $wtelephone
     *
     * @return Staffs
     */
    public function setWtelephone($wtelephone)
    {
        $this->wtelephone = $wtelephone;

        return $this;
    }

    /**
     * Get wtelephone.
     *
     * @return string
     */
    public function getWtelephone()
    {
        return $this->wtelephone;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Staffs
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Staffs
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set location.
     *
     * @param string|null $location
     *
     * @return Staffs
     */
    public function setLocation($location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set salary.
     *
     * @param string|null $salary
     *
     * @return Staffs
     */
    public function setSalary($salary = null)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary.
     *
     * @return string|null
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set gender.
     *
     * @param bool|null $gender
     *
     * @return Staffs
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return bool|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set fonction.
     *
     * @param string|null $fonction
     *
     * @return Staffs
     */
    public function setFonction($fonction = null)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction.
     *
     * @return string|null
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set state.
     *
     * @param string|null $state
     *
     * @return Staffs
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set media.
     *
     * @param string|null $media
     *
     * @return Staffs
     */
    public function setMedia($media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media.
     *
     * @return string|null
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set hasAccount.
     *
     * @param bool|null $hasAccount
     *
     * @return Staffs
     */
    public function setHasAccount($hasAccount = null)
    {
        $this->hasAccount = $hasAccount;

        return $this;
    }

    /**
     * Get hasAccount.
     *
     * @return bool|null
     */
    public function getHasAccount()
    {
        return $this->hasAccount;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Staffs
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set country.
     *
     * @param \AppBundle\Entity\Countries|null $country
     *
     * @return Staffs
     */
    public function setCountry(\AppBundle\Entity\Countries $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \AppBundle\Entity\Countries|null
     */
    public function getCountry()
    {
        return $this->country;
    }
}
