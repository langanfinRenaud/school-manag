<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
//use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
//use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
//    public function __construct(RegistryInterface $registry)
//    {
//        parent::__construct($registry, User::class);
//    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByUserName($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }


    /**
     * @return mixed
     */
    public function getlisteAdmin()
    {

        $em = $this->getEntityManager();

        $sql = "Select A.last_login as lastLogin,A.roles,A.username,A.enabled,A.id, A.email
                      From user A
                      WHERE  A.roles LIKE '%ROLE_ADMIN%' 
					  OR 
					  A.roles LIKE '%ROLE_SUPER_ADMIN%'
        ";
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $result = $statement->FetchAll();

        return $result;
    }
}
