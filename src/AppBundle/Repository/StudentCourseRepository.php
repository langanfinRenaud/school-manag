<?php

namespace AppBundle\Repository;

/**
 * StudentCourseRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StudentCourseRepository extends \Doctrine\ORM\EntityRepository
{
    public function courselist($page, $nbMaxParPage,$studentID,$academicYear)
    {


        $premierResultat = ($page - 1) * $nbMaxParPage;
        $sql = "SELECT S.id,D.id as courseId,D.title,D.code,D.units,S.created,A.year,A.semester FROM AppBundle:StudentCourse S 
        LEFT JOIN AppBundle:Course as D
        WITH S.course = D.id 
        LEFT JOIN AppBundle:AcademicYear as A
        WITH A.id= S.academicYear WHERE S.student= :studentID AND S.academicYear= :academicYear ORDER BY S.id DESC ";

        $query = $this->getEntityManager()->createQuery($sql);

        $query->setFirstResult($premierResultat)->setMaxResults($nbMaxParPage);
        $query->setParameter(':studentID', $studentID);
        $query->setParameter(':academicYear', $academicYear);
        $query->getResult();
//        $paginator = new Paginator($query);


        return $query->getResult();
    }
}
