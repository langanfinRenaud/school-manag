<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Department;
use AppBundle\Entity\Exams;
use AppBundle\Entity\ScheduledExam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Scheduledexam controller.
 *
 * @Route("scheduledexam")
 */
class ScheduledExamController extends Controller
{
    /**
     * Lists all scheduledExam entities.
     *
     * @Route("/", name="scheduledexam_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        $scheduledExams = $em->getRepository('AppBundle:ScheduledExam')->findBy([], ['id' => 'DESC']);

        return $this->render('scheduledexam/index.html.twig', array(
            'scheduledExams' => $scheduledExams,
        ));
    }

    /**
     * Creates a new scheduledExam entity.
     *
     * @Route("/new", name="scheduledexam_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $scheduledExam = new Scheduledexam();
        $form = $this->createForm('AppBundle\Form\ScheduledExamType', $scheduledExam);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $schdexams = $em->getRepository('AppBundle:ScheduledExam')->findBy([], ['id' => 'DESC']);
        $students = $em->getRepository('AppBundle:Students')->findBy([
            'admissionType' => 'online',
            'isdeleted' => null,
            'waiting' => 0
        ], ['id' => 'DESC'], 500);
        $departments = $em->getRepository(Department::class)->findAll();
        $exams = $em->getRepository(Exams::class)->findAll();
        $academicYears = $em->getRepository(AcademicYear::class)->findBy([], ['id' => 'DESC']);
        if ($form->isSubmitted()) {
//dump($request);die();
            $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
                'status' => 'active'
            ]);
            $students = $request->get('selStudent');
            $selexams = $request->get('exams');
//            dump($students,$selexams);die();
            foreach ($students as $key => $studentsid) {
                $student = $em->getRepository('AppBundle:Students')->findOneBy([
                    'matricNo' => $studentsid
                ]);
                foreach ($selexams as $exam) {
                    $theExam = $em->getRepository('AppBundle:Exams')->find($exam);
//                dump($theExam);
                    $scheduled = new Scheduledexam();
                    $scheduled->setStudent($student)
                        ->setAcademicYear($academicYear)
                        ->setExams($theExam)
//                        ->setExamDate($scheduledExam->getExamDate())
                        ->setStartDate($scheduledExam->getStartDate())
                        ->setEndDate($scheduledExam->getEndDate())
                        ->setNotificationDate($scheduledExam->getNotificationDate());
                    $em->persist($scheduled);
                }
//                die();
            }
            $em->flush();
            $message = 'The exam was sucessfully scheduled for ' . $student->getLastname() . ' ' . $student->getFirstname();
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('scheduledexam_new');
        }

        return $this->render('scheduledexam/new.html.twig', array(
            'scheduledExams' => $scheduledExam,
            'academicYears' => $academicYears,
            'schdexams' => $schdexams,
            'form' => $form->createView(),
            'students' => $students,
            'exams' => $exams,
            'department' => $departments
        ));
    }


    /**
     * Displays a form to edit an existing scheduledExam entity.
     *
     * @Route("/{id}/edit", name="scheduledexam_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ScheduledExam $scheduledExam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($scheduledExam);
        $form = $this->createForm('AppBundle\Form\ScheduledExamType', $scheduledExam);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $schdexams = $em->getRepository('AppBundle:ScheduledExam')->findAll();
        $students = $em->getRepository('AppBundle:Students')->findBy([
            'admissionType' => 'online',
            'isdeleted' => null
        ]);
        $departments = $em->getRepository(Department::class)->findAll();
        $exams = $em->getRepository(Exams::class)->findAll();
        $academicYears = $em->getRepository(AcademicYear::class)->findBy([], ['id' => 'DESC']);
        if ($form->isSubmitted()) {

            $studentsid = $request->get('student');
            $academicYearsid = $request->get('academicYear');
            $student = $em->getRepository('AppBundle:Students')->find($studentsid);
            $acadeicYears = $em->getRepository(AcademicYear::class)->find($academicYearsid);
            $scheduledExam->setStudent($student)
                ->setAcademicYear($academicYears)
                ->setExamDate($scheduledExam->getExamDate())
                ->setNotificationDate($scheduledExam->getNotificationDate());
            $em->persist($scheduledExam);
            $em->flush();
            $message = 'The exam was sucessfully scheduled for ' . $student->getLastname() . ' ' . $student->getFirstname();
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('scheduledexam_new');
        }
        return $this->render('scheduledexam/edit.html.twig', array(
            'scheduledExam' => $scheduledExam,
            'academicYears' => $academicYears,
            'edit_form' => $form->createView(),
            'schdexams' => $schdexams,
            'form' => $form->createView(),
            'students' => $students,
            'delete_form' => $deleteForm->createView(),
            'exams' => $exams,
            'department' => $departments
        ));
    }

    /**
     * Creates a form to delete a scheduledExam entity.
     *
     * @param ScheduledExam $scheduledExam The scheduledExam entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ScheduledExam $scheduledExam)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('scheduledexam_delete', array('id' => $scheduledExam->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a scheduledExam entity.
     *
     * @Route("/{id}/scheduledexam_delete", name="scheduledexam_delete")
     */
    public function deleteAction(Request $request, ScheduledExam $scheduledExam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $form = $this->createDeleteForm($scheduledExam);
        $form->handleRequest($request);

//        if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($scheduledExam);
        $em->flush();
        $message = 'The scheduled exam was sucessfully deleted';
        $this->get('session')->getFlashBag()->add('success', $message);


        return $this->redirectToRoute('scheduledexam_new');
    }

    /**
     * Change scheduledExam status.
     *
     * @Route("/{id}/status", name="scheduledexam_status")
     * @Method({"GET", "POST"})
     */
    public function statusAction(ScheduledExam $scheduledExam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        if ($scheduledExam->getIsdeleted() == true) {
            $scheduledExam->setIsdeleted(false);
        } else {
            $scheduledExam->setIsdeleted(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($scheduledExam);
        $em->flush();

        $message = 'The status of scheduled exam was sucessfully changed';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('scheduledexam_new');
    }


    /**
     * Change scheduledExam status.
     *
     * @Route("/end/all/pending/exams", name="end_exams")
     * @Method({"GET", "POST"})
     */
    public function endAllExamsAction($scheduledExam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $unfinshedExam = $em->getRepository(ScheduledExam::class)->deleteSchedl();
//        foreach ($unfinshedExam as $exam) {
//            $exam->setIsdeleted(1);
//            $em->persist($exam);
//        }
//        $em->flush();
        $this->redirectToRoute('scheduledexam_new');
    }
}
