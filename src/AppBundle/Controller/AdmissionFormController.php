<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Department;
use AppBundle\Entity\FeesType;
use AppBundle\Entity\Levels;
use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentGuardian;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use AppBundle\Entity\StudentSponsorship;
use AppBundle\Form\StudentContactType;
use AppBundle\Form\StudentGuardianType;
use AppBundle\Form\StudentsType;
use AppBundle\Service\Email;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Service\CryptData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdmissionFormController extends Controller
{
    /**
     * Controller to generate adminstion form
     * @Route("/generate_admission_form", name="generate_admission_form")
     */
    public function admission_form(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository(Students::class)->last100(1, 1000);
        $departments = $em->getRepository(Department::class)->findAll();
        if ($request->isMethod('POST')) {
            $date = $request->get('date');
            $date = new \DateTime($date);
            $admissionYear = $request->get('admissionYear');
            $feeamount = $request->get('feeamount');
            $feesType = $request->get('feesType');
//           dump($feeamount,json_encode($feesType));die();
            $selStudent = $request->get('selStudent');
            $selStudentLevel = $request->get('level');
            $selStudent = $em->getRepository(Students::class)->find($selStudent);
            if ($selStudent == null) {
                $message = 'The student has not been admitted yet !';
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('generate_admission_form');
            }
            foreach ($feeamount as $fee) {
                $feeamounts[] = intval(str_replace(' ', '', $fee));
            };

//            dump($feeamounts);
//            die();
            $selDept = $request->get('selDept');
            $selDept = $em->getRepository(Department::class)->find($selDept);
//            $date = $request->get('date');
            if($selStudent->getAdmission() == 0){
                return $this->render('students/preview_admission_form_template.html.twig', [
                    'admissionYear' => $admissionYear,
                    'js_feesType' => json_encode($feesType),
                    'js_feeamounts' => json_encode($feeamounts),
                    'feeamount' => $feeamounts,
                    'feesType' => $feesType,
//                'tuitionfee' => $tuitionfee,
                    'selStudent' => $selStudent,
                    'selDept' => $selDept,
                    'selStudentLevel' => $selStudentLevel,
                    'date' => $date
                ]);
            }else{
                return $this->render('students/preview_masters_admission_form_template.html.twig', [
                    'admissionYear' => $admissionYear,
                    'js_feesType' => json_encode($feesType),
                    'js_feeamounts' => json_encode($feeamounts),
                    'feeamount' => $feeamounts,
                    'feesType' => $feesType,
//                'tuitionfee' => $tuitionfee,
                    'selStudent' => $selStudent,
                    'selDept' => $selDept,
                    'selStudentLevel' => $selStudentLevel,
                    'date' => $date
                ]);
            }

        }
        $feeType = $em->getRepository(FeesType::class)->findAll();

        return $this->render('students/generateAdmissionForm.html.twig', [
            'students' => $students,
            'department' => $departments,
            'feeType' => $feeType
        ]);
    }

    /**
     * Send generated admission to student
     *
     * @Route("/sendGeneratedAdmissionForm", name="sendGeneratedAdmissionForm")
     */
    public function sendGeneratedAdmissionFormAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $selStudent = $em->getRepository(Students::class)->find($request->get('selStudent'));
        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $selStudent
        ]);
//        dump(json_decode($request->get('feesType')),json_decode($request->get('feeamount')));die();

        $selStudent = $this->container->get("app.crypt_data")->encrpytData($request->get('selStudent'));
        $admissionYear = $this->container->get("app.crypt_data")->encrpytData($request->get('admissionYear'));
        $feeamount = $this->container->get("app.crypt_data")->encrpytData($request->get('feeamount'));
        $feesType = $this->container->get("app.crypt_data")->encrpytData($request->get('feesType'));
//        $tuitionfee = $this->container->get("app.crypt_data")->encrpytData($request->get('tuitionfee'));
        $selDept = $this->container->get("app.crypt_data")->encrpytData($request->get('selDept'));
        $selStudentLevel = $this->container->get("app.crypt_data")->encrpytData($request->get('selStudentLevel'));
        $date = $this->container->get("app.crypt_data")->encrpytData($request->get('date'));
        $arrayInfo = [
            'selStudent' => $selStudent,
            'admissionYear' => $admissionYear,
            'feeamount' => $feeamount,
            'feesType' => $feesType,
            'selDept' => $selDept,
            'selStudentLevel' => $selStudentLevel,
            'date' => $date,
        ];
        $serialize = serialize($arrayInfo);
        $target = $this->getParameter("printGeneratedAdmissionForm");
        $link = $target . base64_encode($serialize);
        $this->container->get("app.mailer")->envoiEmail("ADMISSION FORM !", $studentContact->getEmail(), "emails/admissionForm.html.twig", array(
            'link' => $link

        ));
        return $this->redirectToRoute('dashboard');
    }

    /**
     * Send generated admission to student
     *
     * @Route("/tacheCheck", name="tacheCheck")
     */
    public function tacheCheckAction(Request $request)
    {
        $this->container->get("app.mailer")->envoiEmail("AUTO SENDER", 'myclash401@gmail.com', "emails/test.html.twig");
        return true;
    }

    /**
     * Send generated admission to student
     *
     * @Route("/printGeneratedAdmissionForm/{code}/", name="printGeneratedAdmissionForm")
     */
    public function printGeneratedAdmissionFormAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $code = $request->get('code');
        $decode = base64_decode($code);
        $arrayInfo = unserialize($decode);
        $selStudent = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['selStudent']['encrypted'], $arrayInfo['selStudent']['key']);
        $admissionYear = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['admissionYear']['encrypted'], $arrayInfo['admissionYear']['key']);
        $feeamount = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['feeamount']['encrypted'], $arrayInfo['feeamount']['key']);
        $feesType = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['feesType']['encrypted'], $arrayInfo['feesType']['key']);
        $selDept = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['selDept']['encrypted'], $arrayInfo['selDept']['key']);
        $selStudentLevel = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['selStudentLevel']['encrypted'], $arrayInfo['selStudentLevel']['key']);
        $date = $this->container->get("app.crypt_data")->decrpytData($arrayInfo['date']['encrypted'], $arrayInfo['date']['key']);

        $selStudent = $em->getRepository(Students::class)->find($selStudent);


        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $selStudent
        ]);
        if($selStudent->getAdmission() == 0){
            $render = $this->renderView('students/admission_form_template.html.twig', array(
                'selStudent' => $selStudent,
                'admissionYear' => $admissionYear,
                'feeamount' => json_decode($feeamount),
                'feesType' => json_decode($feesType),
                'selDept' => $selDept,
                'selStudentLevel' => $selStudentLevel,
                'date' => $date,
            ));
        }else{
            $render = $this->renderView('students/admission_masters_form_template.html.twig', array(
                'selStudent' => $selStudent,
                'admissionYear' => $admissionYear,
                'feeamount' => json_decode($feeamount),
                'feesType' => json_decode($feesType),
                'selDept' => $selDept,
                'selStudentLevel' => $selStudentLevel,
                'date' => $date,
            ));
        }
        $filename = $selStudent->getLastname() . '_' . $selStudent->getFirstname() . '_(' . $selStudent->getMatricNo() . ')' . '_Admission_form.pdf';
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=' . $filename . '.pdf'
            )
        );
        return $this->redirectToRoute('dashboard');
    }

}
