<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ScoreBoardController extends Controller
{
    /**
     * @Route("scores/board", name="scores_board")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $scores = [];
        $em = $this->getDoctrine()->getManager();

        $departments = $em->getRepository('AppBundle:Department')->findAll();
        $levels = $em->getRepository('AppBundle:Levels')->findAll();

        return $this->render('scoreboard/index.html.twig', [
            'scores' => $scores,
            'levels' => $levels,
            'departments' => $departments,
        ]);
    }

}
