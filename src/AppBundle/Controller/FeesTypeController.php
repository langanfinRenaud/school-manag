<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FeesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Feestype controller.
 *
 * @Route("feestype")
 */
class FeesTypeController extends Controller
{
    /**
     * Lists all feesType entities.
     *
     * @Route("/", name="feestype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $feesTypes = $em->getRepository('AppBundle:FeesType')->findBy([
            'isdeleted'=>0
        ],['id'=>'DESC']);

        return $this->render('feestype/index.html.twig', array(
            'feesTypes' => $feesTypes,
        ));
    }

    /**
     * Creates a new feesType entity.
     *
     * @Route("/new", name="feestype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $feesType = new Feestype();
        $form = $this->createForm('AppBundle\Form\FeesTypeType', $feesType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($feesType);
            $em->flush();

            return $this->redirectToRoute('feestype_index');
        }

        return $this->render('feestype/new.html.twig', array(
            'feesType' => $feesType,
            'form' => $form->createView(),
        ));
    }



    /**
     * Displays a form to edit an existing feesType entity.
     *
     * @Route("/{id}/edit", name="feestype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, FeesType $feesType)
    {

        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $editForm = $this->createForm('AppBundle\Form\FeesTypeType', $feesType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('feestype_index');
        }

        return $this->render('feestype/edit.html.twig', array(
            'feesType' => $feesType,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a feesType entity.
     *
     * @Route("/delete_feeType/{id}", name="feestype_delete")
     * @Method("DELETE")
     */
    public function deleteAction( FeesType $feesType)
    {

        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
            $em = $this->getDoctrine()->getManager();
            $feesType->setIsdeleted(1);
            $em->persist($feesType);
            $em->flush();

        return $this->redirectToRoute('feestype_index');
    }


}
