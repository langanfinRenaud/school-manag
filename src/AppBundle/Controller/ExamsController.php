<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Course;
use AppBundle\Entity\ExamAnswers;
use AppBundle\Entity\ExamQuestion;
use AppBundle\Entity\Exams;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Exam controller.
 *
 * @Route("exams")
 */
class ExamsController extends Controller
{
    /**
     * Lists all exam entities.
     *
     * @Route("/", name="exams_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $exams = $em->getRepository('AppBundle:Exams')->findBy([], ['id' => 'DESC']);
// dump();die();
        return $this->render('exams/index.html.twig', array(
            'exams' => $exams,
        ));
    }

    /**
     * Creates a new exam entity.
     *
     * @Route("/new", name="exams_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $exam = new Exams();
        $form = $this->createForm('AppBundle\Form\ExamsType', $exam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
//            Check if exam already exits for course
            $examination = $entityManager->getRepository(Exams::class)->findOneBy([
                'course' => $exam->getCourse()
            ]);
            if ($examination != null) {
                $message = 'There is an exam that exits for this course kindly delete it before recreating another';
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('exams_index');
            }
            $exam->setDescription($request->get('formsummernote'));
            $exam->setNumberofquestions(0);
            $entityManager->persist($exam);
            $entityManager->flush();
            $message = 'The exam ' . $exam->getTitle() . ' has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);

            return $this->redirectToRoute('exams_index');
        }

        return $this->render('exams/new.html.twig', array(
            'exam' => $exam,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a exam entity.
     *
     * @Route("/details/{id}", name="exams_show", methods={"GET"})
     *
     */
    public function showAction(Exams $exam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($exam);

        return $this->render('exams/show.html.twig', array(
            'exam' => $exam,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a exam entity.
     *
     * @param Exams $exam The exam entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Exams $exam)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('exams_delete', array('id' => $exam->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing exam entity.
     *
     * @Route("/{id}/edit", name="exams_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Exams $exam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($exam);
        $editForm = $this->createForm('AppBundle\Form\ExamsType', $exam);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $exam->setDescription($request->get('formsummernote'));
            $this->getDoctrine()->getManager()->persist($exam);
            $this->getDoctrine()->getManager()->flush();


            return $this->redirectToRoute('exams_index');
        }

        return $this->render('exams/edit.html.twig', array(
            'exam' => $exam,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a exam entity.
     *
     * @Route("/deleteExam/{id}", name="exams_delete")
     */
    public function deleteAction(Request $request, Exams $exam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($exam);
        $em->flush();

        return $this->redirectToRoute('exams_index');
    }

    /**
     * @Route("/createExamField", options= {"expose" = true}, name="createExamField", methods={"POST"})
     */
    public function createExamField(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $counter = $request->get('counter');

        return $this->render('examquestion/examfield.html.twig', [
            'counter' => $counter,
        ]);
    }

    /**
     * @Route("/set_question/exam/{id}", name="set_question", methods={"GET"})
     */
    public function set_question(Exams $exam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
//        dump(in_array('ROLE_LECTURER', $currentuser->getRoles()));die();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_LECTURER', $currentuser->getRoles())) {
            return $this->render('examquestion/questionset.html.twig', [
                'exam' => $exam,
            ]);

        } else {
            throw $this->createNotFoundException('You cannot access this page!');
        }
    }

    /**
     * @Route("/edit_question/{id}", name="edit_question")
     */
    public function edit_question(ExamQuestion $question, Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_LECTURER', $currentuser->getRoles())) {

            $entityManager = $this->getDoctrine()->getManager();
            $examanswer = $entityManager->getRepository(ExamAnswers::class)->findBy([
                'question' => $question
            ]);

            return $this->render('examquestion/editquestion.html.twig', [
                'examquestion' => $question,
                'examanswers' => $examanswer,
            ]);
        } else {
            throw $this->createNotFoundException('You cannot access this page!');

        }
    }

    /**
     * @Route("/modify_question/", name="modify_question")
     */
    public function modify_question(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_LECTURER', $currentuser->getRoles())) {

            $entityManager = $this->getDoctrine()->getManager();

            $questionsid = $request->get('questionid');
            $ranswers = $request->get('realAnswerQ1');
            $answers = $request->get('answerQ1');
            $questn = $request->get('question');
            $durations = $request->get('duration');
            $examquestion = $entityManager->getRepository(ExamQuestion::class)->findOneBy([
                'id' => $questionsid
            ]);
//        dump($request->get('duration'),$request->get('question'));die();
            $examquestion->setQuestion($questn);
            $examquestion->setDuration($durations);
            $entityManager->persist($examquestion);
            $examanswer = $entityManager->getRepository(ExamAnswers::class)->findBy([
                'question' => $examquestion
            ]);
            foreach ($examanswer as $keys => $answer) {
                $answer->setExam($examquestion->getExam());
                $answer->setQuestion($examquestion);
                $answer->setAnswer($answers[$keys]);
                if ($ranswers == $keys) {
                    $answer->setIsanswer(1);
                }
                $entityManager->persist($answer);
                $entityManager->flush();
            }

            $message = 'The exam question has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            if(in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())){
                return $this->redirectToRoute('question_list', array(
                    'id' => $examquestion->getExam()->getId()
                ));
            }elseif (in_array('ROLE_LECTURER', $currentuser->getRoles())){
                return $this->redirectToRoute('lecturer_question_list', array(
                    'course' => $examquestion->getExam()->getCourse()->getId()
                ));
            }
        } else {

            throw $this->createNotFoundException('You cannot access this page!');

        }

    }

    /**
     * @Route("/delete_question/{id}", name="delete_question")
     */
    public function delete_question(ExamQuestion $examquestion, Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
//        dump($examquestion);die();

        $entityManager = $this->getDoctrine()->getManager();
        $examquestion->setIsdeleted(true);
        $newNumberOfQuestion = $examquestion->getExam()->getNumberofquestions() - 1;
        if ($newNumberOfQuestion < 0) {
            $newNumberOfQuestion = 0;
        }
        $examquestion->getExam()->setNumberofquestions($newNumberOfQuestion);
        $entityManager->persist($examquestion->getExam());

        $entityManager->persist($examquestion);
        $entityManager->flush();
        $message = 'The exam question has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('question_list', [
            'id' => $examquestion->getExam()->getId()
        ]);
    }

    /**
     * @Route("/lecturer_question_list/{course}", name="lecturer_question_list")
     * @Method("GET")
     */
    public function lecturer_question_list(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository(Course::class)->find($request->get('course'));
        $exam = $em->getRepository(Exams::class)->findOneBy([
            'course' => $course
        ]);
        if ($exam) {
            $examquestion = $em->getRepository(ExamQuestion::class)->findBy([
                'exam' => $exam,
                'isdeleted' => 0
            ]);
//        dump($exam);die();
            return $this->render('examquestion/questionlist.html.twig', [
                'examquestions' => $examquestion,
                'exam' => $exam
            ]);
        } else {
            $message = 'No exam for this course yet!';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }

    }

    /**
     * @Route("/question_list/{id}", name="question_list")
     * @Method("GET")
     */
    public function question_list(Exams $exam)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $examquestion = $em->getRepository(ExamQuestion::class)->findBy([
            'exam' => $exam,
            'isdeleted' => 0
        ]);
//        dump($exam);die();
        return $this->render('examquestion/questionlist.html.twig', [
            'examquestions' => $examquestion,
            'exam' => $exam
        ]);
    }

    /**
     * @Route("/save_question", name="save_question")
     */
    public function save_questiona(Request $request)
    {
//        dump($request);die();
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $examid = $request->get('exam');
        $questions = $request->get('question');
        $durations = $request->get('duration');
        $points = $request->get('points');
        $exam = $em->getRepository(Exams::class)->find($examid);
        $exam->setNumberofquestions(count($questions) + $exam->getNumberofquestions());
        $em->persist($exam);
//        Set Questions
        $k = 1;
        foreach ($questions as $keys => $question) {
//            dump($question,$keys);die();
            $examquestions = new ExamQuestion();
            $examquestions->setExam($exam);
            $examquestions->setType('Multi-choice');
            $examquestions->setQuestion($question);
            $examquestions->setDuration($durations[$keys]);
            $examquestions->setPoints($points[$keys]);
            $em->persist($examquestions);


            $ans = 'answerQ' . $k;
            $realans = 'realAnswerQ' . $k;
            $answers = $request->get($ans);
            $ranswers = $request->get($realans);
//            Set Answers
            for ($j = 0; $j < count($answers); $j++) {
                $examanswers = new ExamAnswers();
//                dump($realans,$ranswers,$key);die();
                $examanswers->setExam($exam);
                $examanswers->setQuestion($examquestions);
                $examanswers->setAnswer($answers[$j]);
                if ($ranswers == $j) {
                    $examanswers->setIsanswer(1);
                }
                $em->persist($examanswers);
                $em->flush();
            }

            $k = $k + 1;
//            dump($request,$examquestions, $examanswers, $j, $ans, $answers);
        }
        $message = 'The exam question has been added sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('exams_index');
    }

    /**
     * @Route("/gen_ref", name="genref")
     */
    public function genref(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $examquestion = $em->getRepository(ExamQuestion::class)->findAll();
        foreach ($examquestion as $item) {
            $item->setRef('EQ-' . strtoupper(substr(str_shuffle(MD5(microtime())), 0, 6)));
            $em->persist($item);
        }
        $em->flush();
        return $this->redirectToRoute('exams_index');
    }
}
