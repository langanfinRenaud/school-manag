<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Categories;
use AppBundle\Entity\Events;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Event controller.
 *
 * @Route("events")
 */
class EventsController extends Controller
{
    /**
     * Lists all event entities.
     *
     * @Route("/", name="events_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository(Events::class)->findAll();
        $category = $em->getRepository(Categories::class)->findAll();
//        $events = json_encode($events);
        $eventsArry = [];
        foreach ($events as $event) {
            if (!empty($event->getEventEndDate())) {
                $eventsArry[] = [
                    "title" => $event->getTitle(),
                    "start" => $event->getEventStartDate()->format('Y-m-d'),
                    "end" => $event->getEventEndDate()->format('Y-m-d'),
                    "url" => 'events/edit/' . $event->getId()
                ];
            } else {
                $eventsArry[] = [
                    "title" => $event->getTitle(),
                    "start" => $event->getEventStartDate()->format('Y-m-d'),
                    "url" => 'events/edit/' . $event->getId()
                ];
            }
        }
        $events = json_encode($eventsArry);
        return $this->render('events/index.html.twig', array(
            'events' => $events,
            'category' => $category,
        ));
    }
    /**
     * @Route("/events/save", name="events_new")
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $event = new Events();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
           // dump($category);die();
            $id = $request->get('id');
            $category = $request->get('category');
            $category = $em->getRepository(Categories::class)->findOneBy([
                'id' => $category
            ]);
            if (!empty($id)) {
                $event = $em->getRepository(Events::class)->findOneBy([
                    'id' => $id
                ]);
            }

            if ($category->getTitle()=='Admission'){
                 $admisionEvent= $em->getRepository(Events::class)->findOneBy([
                       'category' => $category,
                       'statut'=> true
                   ]);
               if(!empty($id) && $admisionEvent->getId() != $event->getId()){
                  
                   $admisionEvent->setStatut(false);
                   $em->persist($admisionEvent);
               }
            }
            $enddate = $request->get('enddate');
            $image = $request->files->get('image');
            if ($image != null) {
                $target = $this->getParameter("repertoire_events");
                $lienordre = $fileUploader->uploadImage($image, $target);
                $linkPP = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/events/" . $lienordre;
                $event->setLanguage($request->get('lang'));
                $event->setMedia($lienordre);
                $event->setMediaLink($linkPP);
            }
            $event->setCategory($category);
            $event->setTitle($request->get('title'));
            $event->setModifieddate(new \DateTime());
            $event->setEventStartDate(new \DateTime($request->get('startdate')));
            if ($enddate != '') {
                $event->setEventEndDate(new \DateTime($enddate));
            }
            $event->setDescription($request->get('formsummernote'));
            $em->persist($event);
            $em->flush();

            $message = 'The event has been saved sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('events_index');
        }

    }

    /**
     * @Route("/events/edit/{id}", name="events_edit")
     */
    public function editAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $events = $em->getRepository(Events::class)->findAll();
        $singleevents = $em->getRepository(Events::class)->findOneBy([
            'id' => $id
        ]);
        $category = $em->getRepository(Categories::class)->findAll();
//        $events = json_encode($events);
        $eventsArry = [];
        foreach ($events as $event) {
            if (!empty($event->getEventEndDate())) {
                $eventsArry[] = [
                    "title" => $event->getTitle(),
                    "start" => $event->getEventStartDate()->format('Y-m-d'),
                    "end" => $event->getEventEndDate()->format('Y-m-d'),
                    "url" => 'events/edit/' . $event->getId()
                ];
            } else {
                $eventsArry[] = [
                    "title" => $event->getTitle(),
                    "start" => $event->getEventStartDate()->format('Y-m-d'),
                    "url" => 'events/edit/' . $event->getId()
                ];
            }
        }
        $events = json_encode($eventsArry);
       // dump($singleevents);die();
        return $this->render('events/edit.html.twig', [
            'events' => $events,
            'single' => $singleevents,
            'category'=> $category
        ]);
    }

    /**
     * @Route("/events/category", name="events_category")
     */
    public function creatCategory(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $title = $request->get('titleCategory');
        $popular = $request->get('popular');

        $category = new Categories();
        $category->setTitle($title);
        $category->setPoular($popular);
        $em->persist($category);
        $em->flush();
        $message = 'The event category has been created sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('events_index');
    }



    /**
     * Deletes a event entity.
     *
     * @Route("/{id}", name="events_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Events $event)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }

        return $this->redirectToRoute('events_index');
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param Events $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Events $event)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('events_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
