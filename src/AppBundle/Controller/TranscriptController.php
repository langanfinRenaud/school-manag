<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\StudentCourse;


/**
 * Transcript controller.
 *
 * @Route("transcript")
 */
class TranscriptController extends Controller
{
    /**
     * Lists all course entities.
     *
     * @Route("/", name="transcript_index")
     * @Method("GET")
     */
    public function indexAction()
    {
    	$auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
    	if (!$auth) {
    		return $this->redirectToRoute('fos_user_security_login');
    	}
    	$currentuser = $this->getUser();
    	if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
    		throw $this->createNotFoundException('You cannot access this page!');
    	}
    	$em = $this->getDoctrine()->getManager();
    	$students = $em->getRepository('AppBundle:Students')->findBy([],['id'=>'DESC']);
    	return $this->render('transcript/index.html.twig', array(
    		'students' => $students
    	));
    }
}
