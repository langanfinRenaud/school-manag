<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Payments;
use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentSponsorship;
use NumberToWords\NumberToWords;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Payment controller.
 *
 * @Route("payments")
 */
class PaymentsController extends Controller
{
    /**
     * Lists all payment entities.
     *
     * @Route("/", name="payments_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        // $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        $em = $this->getDoctrine()->getManager();

        $payments = $em->getRepository('AppBundle:Payments')->findBy([], ['id' => ' DESC']);

        return $this->render('payments/index.html.twig', array(
            'payments' => $payments,
        ));
    }

    /**
     * Creates a new payment entity.
     *
     * @Route("/new", name="payments_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        $em = $this->getDoctrine()->getManager();

        $payment = new Payments();
        // Check to see if fees have been cleared
        $feeid = $request->get('feeid');
        $fee = $em->getRepository('AppBundle:Fees')->find($feeid);

        if ($fee->getStatus() == true) {
            $message = 'Sorry but this fee has been cleared';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('payments_index');
        }


        $amount = $request->get('amount');
        $currency = $request->get('currency');
//        if ($currency == 'FCFA') {
//            $aer = $request->get('aer');
//            $amount = (1000 / $aer) * $amount;
//        }
        if (($amount + $fee->getTotalPaid()) > $fee->getAmount()) {
            $message = 'Sorry but the amount about to paid will surpass the required amount for this fee';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('payments_index');
        }
        $user = $this->getUser();
        $payment->setAmount($amount)
            ->setCurrency($currency)
            ->setCreatedBy($user->getUsername())
            ->setTheFee($fee);
        $em->persist($payment);
//        $em->flush();
        // dump($payment);die();
        $studEmail = $em->getRepository('AppBundle:StudentContact')->findOneBy([
            'student' => $payment->getTheFee()->getStudent(),
        ]);
//        $allPayments = $em->getRepository('AppBundle:Payments')->findBy([
//            'theFee' => $payment->getTheFee(),
//        ], ['id' => ' DESC']);

        $totalNewPaid = $amount + $fee->getTotalPaid();
        $fee->setTotalPaid($totalNewPaid);

//        foreach ($allPayments as $paid) {
//            $totalPaid += $paid->getAmount();
//        }
        if ($totalNewPaid == $payment->getTheFee()->getAmount()) {
            $fee->setStatus(1);

        }
        $em->persist($fee);
//        dump($fee->getType()->getId());
//        die();
        $em->flush();
//        check if the student sponsored
        $sponsorStudent = $em->getRepository(StudentSponsorship::class)->findOneBy(['student' => $fee->getStudent()]);
        if ($sponsorStudent != null && $fee->getType()->getId() == 1) {
            $this->paysSponsor($fee->getStudent());
        }

        $link = $this->getParameter("printPaymentReciept") . $payment->getId();
        $recepiants = [$studEmail->getEmail(), 'esgtbenin@gmail.com'];
        $this->container->get("app.mailer")->envoiEmail("PAYMENT ALERT!", $recepiants, "emails/paymentAlert.html.twig", array(
            'paymentinfo' => $payment,
            'link' => $link
        ));
        $message = 'Payment has been made successfully and a payment alert has been sent to the student!';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('fees_show', array('id' => $fee->getId()));

    }

    public function paysSponsor($student)
    {
        $em = $this->getDoctrine()->getManager();
        $sponsor = $em->getRepository(StudentSponsorship::class)->findOneBy(['student' => $student,'paid'=>0]);
        if ($sponsor != null && $sponsor->getSponsor() != null) {
            $sponsorContact = $em->getRepository(StudentContact::class)->findOneBy(['student' => $sponsor]);
            $this->container->get("app.mailer")->envoiEmail("PAYMENT ALERT!", $sponsorContact->getEmail(), "emails/sponsorAlert.html.twig", array(
                'student' => $student
            ));

        } elseif ($sponsor != null && $sponsor->getSponsorAgent()) {
            $this->container->get("app.mailer")->envoiEmail("PAYMENT ALERT!", $sponsor->getSponsorAgent()->getEmail(), "emails/sponsorAlert.html.twig", array(
                'student' => $student
            ));

        }
        return true;
    }

    /**
     * Displays a form to edit an existing payment entity.
     *
     * @Route("/{id}/edit", name="payments_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Payments $payment)
    {
        return $this->redirectToRoute('fos_user_security_login');

        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        $deleteForm = $this->createDeleteForm($payment);
        $editForm = $this->createForm('AppBundle\Form\PaymentsType', $payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('payments_edit', array('id' => $payment->getId()));
        }

        return $this->render('payments/edit.html.twig', array(
            'payment' => $payment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a payment entity.
     *
     * @Route("/delete_payment/{id}", name="payments_delete")
     */
    public function deleteAction(Request $request, Payments $payment)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }

        $em = $this->getDoctrine()->getManager();
        $em->remove($payment);
        $em->flush();
        $message = 'Payment has been deleted successfully !';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('payments_index');
    }

    /**
     * Pirnt recepit.
     *
     * @Route("/printRecipte/{id}", name="printRecipte")
     * @Method("GET")
     */
    public function printRecipteAction(Payments $payment)
    {
        $em = $this->getDoctrine()->getManager();

        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');
        $wordNumber = $numberTransformer->toWords($payment->getAmount());

        $allPayments = $em->getRepository('AppBundle:Payments')->findBy([
            'theFee' => $payment->getTheFee(),
        ], ['id' => ' DESC']);
        $totalPaid = 0;
        foreach ($allPayments as $paid) {
            $totalPaid += $paid->getAmount();
        }
        $render = $this->renderView('payments/includes/singleRecipte.html.twig', array(
            'wordNumber' => $wordNumber,
            'paymentinfo' => $payment,
            'totalPaid' => $totalPaid,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render, array(
                'orientation' => 'portrait',
                'page-height' => 114.4,
                'page-width' => 214.6,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 300,

            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=Payment_Reciept.pdf',
            )
        );

    }

    /**
     * Creates a form to delete a payment entity.
     *
     * @param Payments $payment The payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payments $payment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('payments_delete', array('id' => $payment->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
