<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Categories;
use AppBundle\Entity\Countries;
use AppBundle\Entity\Course;
use AppBundle\Entity\Department;
use AppBundle\Entity\Events;
use AppBundle\Entity\ExamAnswers;
use AppBundle\Entity\ExamQuestion;
use AppBundle\Entity\Exams;
use AppBundle\Entity\FAQ;
use AppBundle\Entity\Fees;
use AppBundle\Entity\LibraryAssets;
use AppBundle\Entity\Payments;
use AppBundle\Entity\ScheduledExam;
use AppBundle\Entity\Staffs;
use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentCourse;
use AppBundle\Entity\StudentExams;
use AppBundle\Entity\StudentGuardian;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use AppBundle\Entity\StudentSponsorship;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends FOSRestController
{
    private $finalView;
    private $header = array('Access-Control-Allow-Origin' => '*',);


    /**
     * @Route("/get_countries_department", name="get_countries")
     */
    public function get_countries()
    {
        $countries = $this->get("doctrine")->getManager()->getRepository(Countries::class)->findAll();
        $departments = $this->get("doctrine")->getManager()->getRepository(Department::class)->findAll();
        $conDepar = [
            'countries' => $countries,
            'departments' => $departments
        ];
        $view = $this->view($conDepar)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }


    /**
     * @Route("/send_latest_events", name="send_latest_events")
     */
    public function send_latest_events()
    {
        $events = $this->get("doctrine")->getManager()->getRepository(Events::class)->latestEvent(1, 3);
        $admissionCat = $this->get("doctrine")->getManager()->getRepository(Categories::class)->findOneBy([
            'title' => 'Admission']);
        $admissionevents = $this->get("doctrine")->getManager()->getRepository(Events::class)->findBy([
            'category' => $admissionCat,
            'statut' => true
        ]);
//        dump($events);die();
        $result = [
            'admissions' => $admissionevents,
            'others' => $events
        ];
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * @Route("/get_assets", name="get_assets")
     */
    public function get_assets()
    {
        $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->latestAsset(1, 12);
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * @Route("/get_faq", name="get_faq")
     */
    public function get_faq()
    {
        $general = $this->get("doctrine")->getManager()->getRepository(FAQ::class)->findBy([
            'type' => 1
        ], ['id' => 'DESC'], 20);
        $studentPortal = $this->get("doctrine")->getManager()->getRepository(FAQ::class)->findBy([
            'type' => 2
        ], ['id' => 'DESC'], 20);
        $admission = $this->get("doctrine")->getManager()->getRepository(FAQ::class)->findBy([
            'type' => 3
        ], ['id' => 'DESC'], 20);
        $faq = [
            'general' => $general,
            'studentPortal' => $studentPortal,
            'admission' => $admission
        ];
        $view = $this->view($faq)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * @Route("/single_events", name="single_events")
     */
    public function single_event(Request $request)
    {
        $data = json_decode($request->getContent());
        $events = $this->get("doctrine")->getManager()->getRepository(Events::class)->findOneBy([
            'id' => $data->id,
            'statut' => true
        ]);
        $view = $this->view($events)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * @Route("/categorylist", name="categorylist")
     */
    public function categorylist(Request $request)
    {
        $category = $this->get("doctrine")->getManager()->getRepository(Categories::class)->findAll();
        $view = $this->view($category)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get event by category
     * @Route("/event_by_category", name="event_by_category")
     */
    public function event_by_category(Request $request)
    {
        $data = json_decode($request->getContent());
        $category = $this->get("doctrine")->getManager()->getRepository(Categories::class)->find($data->id);
        $events = $this->get("doctrine")->getManager()->getRepository(Events::class)->getOtherCat(1, 50, $category);
        $view = $this->view($events)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get course for a particular student
     * @Route("/course_list", name="course_list")
     */
    public function course_list(Request $request)
    {
        $data = json_decode($request->getContent());

//        Find Student
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);
        $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $result = $this->get("doctrine")->getManager()->getRepository(StudentCourse::class)->courselist(1, 300, $student->getId(), $academicYear->getID());

        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get course sheet for a particular student
     * @Route("/course_sheet_pdf", name="course_sheet_pdf")
     */
    public function download_Course_Sheet(Request $request)
    {
        $data = json_decode($request->getContent());

//        Find Student
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);
        $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $result = $this->get("doctrine")->getManager()->getRepository(StudentCourse::class)->courselist(1, 300, $student->getId(), $academicYear->getID());
        $render = $this->renderView('course/printCourse.html.twig', array(
            'courses' => $result,
            'student' => $student,
            'academicYear' => $academicYear,

        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=Course-sheet.pdf',
            )
        );
    }

    /**
     * Get course descriptions
     * @Route("/course_decsription", name="course_decsription")
     */
    public function course_decsription(Request $request)
    {
        $data = json_decode($request->getContent());

//        Find Course
        $result = $this->get("doctrine")->getManager()->getRepository(Course::class)->find($data->id);
//dump($result);die();
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get list of all courses service
     * @Route("/all_course_list", name="all_course_list")
     */
    public function all_course_list(Request $request)
    {
        $data = json_decode($request->getContent());


        $result = $this->get("doctrine")->getManager()->getRepository(Course::class)->AllCourse(1, 1000);
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Course registration service
     * @Route("/register_course", name="register_course")
     */
    public function register_course(Request $request)
    {
        $data = json_decode($request->getContent());

//        Find Student
        $em = $this->get("doctrine")->getManager();
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);
//        dump($data->courseid);
//        die();
        $alreadyRegistered = 0;
        $alreadySchduled = 0;
        foreach ($data->courseid as $courseid) {
//              Get course
            $course = $this->get("doctrine")->getManager()->getRepository(Course::class)->find($courseid);
//              Get Exams            
            $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->findBy([
                'course' => $course
            ]);
//              Get Academic year            
            $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
                'status' => 'active'
            ]);

            $coursesRegistered = $this->get("doctrine")->getManager()->getRepository(StudentCourse::class)->findBy([
                'student' => $student,
                'course' => $course,
                'academicYear' => $academicYear
            ]);

            if (empty($coursesRegistered)) {
                $registertNewCourse = new StudentCourse();
                $registertNewCourse->setCourse($course)
                    ->setStudent($student)
                    ->setAcademicYear($academicYear);
                $em->persist($registertNewCourse);

//                Check if the student is an online studentand if the exam exist
                /*if ($data->role === 'ONLINE' && $exam != null) {

                    // Check if no exam has been scheduled already
                    $checkSchedule = $this->get("doctrine")->getManager()->getRepository(ScheduledExam::class)->findOneBy([
                        'student' => $student,
                        'academicYear' => $academicYear
                    ]);


                    if ($checkSchedule == null) {
                        $date = new \DateTime('now');
                        $duration1 = new \DateInterval('P2M');
//                Notification date is 20 days to the exam date
                        $duration2 = new \DateInterval('P20D');
                        $examdate = $date->add($duration1)->format('Y/m/d');
                        $notificationDate = $date->sub($duration2)->format('Y/m/d');

//                dump($date->format('Y/m/d'),$examdate, $notificationDate);
//                die();
                        $scheduleExam = new ScheduledExam();
                        $scheduleExam->setStudent($student)
                            ->setExams($exam)
                            ->setExamDate(new \DateTime($examdate))
                            ->setNotificationDate(new \DateTime($notificationDate));
                        $em->persist($scheduleExam);
                    } else {
                        $alreadySchduled = true;
                    }

                }*/
                $em->flush();

                $message = "Your course registration process was successfully";

            } else {
                $alreadyRegistered++;

            }
        }
        if ($alreadyRegistered > 0) {
            $message = $alreadyRegistered . " amongst the " . count($data->courseid) . " courses you selected has already been assigned to you";

        }

        $view = $this->view($message)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Search library service
     * @Route("/serach_library", name="serach_library")
     */
    public function serach_library(Request $request)
    {
        $data = json_decode($request->getContent());

        $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->getSearchLib($data->searchkey);
//        dump($result);
//        die();
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Search by Author and Department
     * @Route("/filter_library", name="filter_library")
     */
    public function filter_library(Request $request)
    {
        $data = json_decode($request->getContent());

        if (!empty($data->authorName)) {
            $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->getLibAssetByDepartmentAuthor(
                $data->department,
                $data->authorName
            );
        } else {
            $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->getLibAssetByDepartment($data->department);
        }

        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get liste of departments
     * @Route("/departmentList", name="departmentList")
     */
    public function departmentList(Request $request)
    {
        $result = $this->get("doctrine")->getManager()->getRepository(Department::class)->findAll();
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get liste of Authors
     * @Route("/authorsList", name="authorsList")
     */
    public function authorsList(Request $request)
    {
        $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->getAuthors();
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get liste of agents
     * @Route("/agentsList", name="agentsList")
     */
    public function agentsList(Request $request)
    {
        $result = $this->get("doctrine")->getManager()->getRepository(Staffs::class)->getlisteAgents();
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

//    /**
//     * @Route("/filter_library", name="filter_library")
//     */
//    public function filter_library(Request $request)
//    {
//        $data = json_decode($request->getContent());
//
////        Find Student
//        $result = $this->get("doctrine")->getManager()->getRepository(LibraryAssets::class)->findBy([
//
//        ]);
//
//        $view = $this->view($result)
//            ->setStatusCode(200)
//            ->setHeaders($this->header)
//            ->setFormat("json");
//
//        $this->finalView = $view;
//
//        return $this->handleView($this->finalView);
//    }

    /**
     * @Route("/exam_details", name="exam_details")
     */
    public function exam_details(Request $request)
    {
        $data = json_decode($request->getContent());
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricno
        ]);
//        if ($student->getAdmissionType() == 'online') {
        $course = $this->get("doctrine")->getManager()->getRepository(Course::class)->find($data->id);
        $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->findOneBy([
            'course' => $course
        ]);
        $today_date = new \DateTime();
//        dump($course,$exam);
//        die();
        if ($exam) {
            $scheduledExam = $this->get("doctrine")->getManager()->getRepository(ScheduledExam::class)->getSchduledExams($student, $exam, $today_date->format('Y-m-d'));
//            dump($scheduledExam, $exam->getId());
//            die();
            if (!empty($scheduledExam)) {
                $result = ['message' => 'Available',
                    'exam' => $exam
                ];

            } elseif ($exam->getId() == 4) {
                $result = ['message' => 'Available',
                    'exam' => $exam
                ];

            } else {
                $result = ['message' => 'Not Available',
                    'exam' => 'Null'];
            }
        } else {
            $result = ['message' => 'Not Available',
                'exam' => 'Null'];
        }

        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * @Route("/exam_questions", name="exam_questions")
     */
    public function exam_questions(Request $request)
    {
        $data = json_decode($request->getContent());
        $course = $this->get("doctrine")->getManager()->getRepository(Course::class)->find($data->courseid);
        $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->findOneBy(['course' => $course]);
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);
        $today_date = new \DateTime();
        $scheduledExam = $this->get("doctrine")->getManager()->getRepository(ScheduledExam::class)->getSchduledExams($student, $exam, $today_date->format('Y-m-d'));
        if (!empty($scheduledExam) || $exam->getTitle() == 'DEMO') {
            if (!empty($scheduledExam)) {
                $scheduledExam[0]->setIsdeleted(true)
                ->setAttemptDate(new \DateTime());
                $this->get("doctrine")->getManager()->persist($scheduledExam[0]);

                $this->get("doctrine")->getManager()->flush();
            }
            $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->findOneBy([
                'course' => $course
            ]);
            $question = $this->get("doctrine")->getManager()->getRepository(ExamQuestion::class)->findBy([
                'exam' => $exam,
                'isdeleted' => 0
            ]);
            $lesqueustion = [];
            foreach ($question as $q) {
                $answers = $this->get("doctrine")->getManager()->getRepository(ExamAnswers::class)->findBy([
                    'question' => $q
                ]);

                $lesqueustion[] = [
                    'qId' => $q->getId(),
                    'question' => $q->getQuestion(),
                    'points' => $q->getPoints(),
                    'duration' => $q->getDuration(),
                    'created' => $q->getCreated(),
                    'answers' => $this->shuffle_assoc($answers)
                ];
            }

            $result = $lesqueustion;
        } else {
            $result = null;
        }

        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    function shuffle_assoc($list)
    {
        if (!is_array($list)) return $list;

        $keys = array_keys($list);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key)
            $random[$key] = $list[$key];

        return $random;
    }

    /**
     * My score
     * @Route("/myscore", name="myscore")
     */
    public function myscore(Request $request)
    {
        $data = json_decode($request->getContent());
        $score= $this->get("doctrine")->getManager()->getRepository(StudentExams::class)->getScoreBoard($data->studId);
        $view = $this->view($score)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);

    }
    /**
     * Analyse answer
     * @Route("/analys_answers", name="analys_answers")
     */
    public function analyseAnswers(Request $request)
    {
        $data = json_decode($request->getContent());
        $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->find($data->exam);
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->studentMatric
        ]);

        $academiYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        if ($exam != null) {
            $studentpoint = 0;
            $questions = $data->questions;
            $questionNotAnswered = 0;
            $totalpoint = 0;
            foreach ($questions as $key => $value) {
                $question = $this->get("doctrine")->getManager()->getRepository(ExamQuestion::class)->find($value);
                $totalpoint += $question->getPoints();
                if ($data->answers[$key] != null) {
                    $answer = $this->get("doctrine")->getManager()->getRepository(ExamAnswers::class)->find($data->answers[$key]);
                    if ($answer->getIsanswer() != null) {
                        $studentpoint = $studentpoint + $question->getPoints();
                    }
                } else {
                    $questionNotAnswered = $questionNotAnswered + 1;
                }
            }
            if ($exam->getId() != 4) {

                $studentExam = $this->get("doctrine")->getManager()->getRepository(StudentExams::class)->findOneBy([
                    'exam' => $exam,
                    'students' => $student
                ]);

                $em = $this->getDoctrine()->getManager();

                if ($studentExam == null) {
                    $studentExam = new StudentExams();
                    $studentExam->setStudents($student)
                        ->setExam($exam)
                        ->setCarryOver(false)
                        ->setNoOfCarryOver(0)
                        ->setStudentpoint($studentpoint)
                        ->setTotalpoint($totalpoint)
                        ->setAcademicYear($academiYear);

                } else {
                    $newnoofcarryover = $studentExam->getNoOfCarryOver() + 1;
                    $studentExam->setCarryOver(true)
                        ->setNoOfCarryOver($newnoofcarryover)
                        ->setStudentpoint($studentpoint)
                        ->setTotalpoint($totalpoint)
                        ->setAcademicYear($academiYear);
                }

                $em->persist($studentExam);
//            Remove the scheduled exam

                $em->flush();

            }
        }
//        dump($exam,$student,$studentExam);die();

        $result = [
            'exam' => $exam,
            'studentpoint' => $studentpoint,
            'examppoint' => $totalpoint,
            'questionNotAnswered' => $questionNotAnswered,
        ];
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Send student information for modification
     * @Route("/get_student_data", name="get_student_data")
     */
    public function get_student_data(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        // dump($data);die();
        $studenttype = "AppBundle\Entity\Students";
        $studentContacttype = "AppBundle\Entity\StudentContact";
        $studentGuardiantype = "AppBundle\Entity\StudentGuardian";
        $studentMediatype = "AppBundle\Entity\StudentMedia";
//        Get all Student information and unserialize them
        $serializer = $this->get('jms_serializer');
        $studentSent = $serializer->deserialize($data->student, $studenttype, 'json');
        $studentContactSent = $serializer->deserialize($data->studentContact, $studentContacttype, 'json');
        $studentGuardianSent = $serializer->deserialize($data->studentGuardian, $studentGuardiantype, 'json');
        $studentMediaSent = $serializer->deserialize($data->studentMedia, $studentMediatype, 'json');
        $student = $em->getRepository(Students::class)->find($studentSent->getId());
        $student->setFirstname($studentSent->getFirstname())
            ->setLastname($studentSent->getLastname())
            ->setDateOfBirth($studentSent->getDateOfBirth())
            ->setNationality($studentSent->getNationality())
            ->setGender($studentSent->getGender())
            ->setOLevelResult($studentSent->getOLevelResult())
            ->setNoOfSitting($studentSent->getNoOfSitting());
        $em->persist($student);

        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $studentSent
        ]);
        $studentContact->setTelephone($studentContactSent->getTelephone())
            ->setAddress($studentContactSent->getAddress())
            ->setEmail($studentContactSent->getEmail());
        $em->persist($studentContact);

        $studentGuardian = $em->getRepository(StudentGuardian::class)->findOneBy([
            'student' => $studentSent
        ]);
        $studentGuardian->setFullname($studentGuardianSent->getFullname())
            ->setTelephone($studentGuardianSent->getTelephone())
            ->setEmail($studentGuardianSent->getEmail())
            ->setAddress($studentGuardianSent->getAddress())
            ->setRelationshipType($studentGuardianSent->getRelationshipType());
        $em->persist($studentGuardian);

        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $studentSent
        ]);
        if ($studentMedia == null) {
            $studentMedia = new StudentMedia();
            $studentMedia->setStudent($student)
                ->setPhoto($studentMediaSent->getPhoto())
                ->setLinkphoto($studentMediaSent->getLinkphoto())
                ->setCertificate($studentMediaSent->getCertificate())
                ->setLinkcertificate($studentMediaSent->getLinkcertificate());
        } else {
            $studentMedia->setStudent($student)
                ->setPhoto($studentMediaSent->getPhoto())
                ->setLinkphoto($studentMediaSent->getLinkphoto())
                ->setCertificate($studentMediaSent->getCertificate())
                ->setLinkcertificate($studentMediaSent->getLinkcertificate());
        }
        $em->persist($studentMedia);
        $em->flush();

        $view = $this->view('success')
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Send student information for modification
     * @Route("/send_student_data", name="send_student_data")
     */
    public function send_student_data(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
//        Get all Student information qnd serialize them
        $serializer = $this->get('jms_serializer');
        $student = $em->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);

        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student
        ]);

        $studentGuardian = $em->getRepository(StudentGuardian::class)->findOneBy([
            'student' => $student
        ]);

        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student
        ]);

// Serialize info
        $studentMediaJson = $serializer->serialize($studentMedia, 'json');
        $studentJson = $serializer->serialize($student, 'json');
        $studentContactJson = $serializer->serialize($studentContact, 'json');
        $studentGuardianJson = $serializer->serialize($studentGuardian, 'json');

        $studentInfo = [
            'student' => $studentJson,
            'studentContact' => $studentContactJson,
            'studentGuardian' => $studentGuardianJson,
            'studentMedia' => $studentMediaJson,
        ];
        // dump($studentInfo);die();

        $view = $this->view($studentInfo)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get registration data froom website
     * @Route("/get_registration_data", name="get_registration_data")
     */
    public function get_registration_data(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        // dump($data);die();

//        Sava data into student table first
        $country = $em->getRepository(Countries::class)->findOneBy([
            'code' => $data->nationality
        ]);
        $department = $em->getRepository(Department::class)->findOneBy([
            'id' => $data->department
        ]);
        $student = new Students();
        $student->setFirstname($data->firstname);
        $student->setLastname($data->lastname);
        $student->setDateOfBirth(new \DateTime($data->dateofbirth));
        $student->setNationality($country);
        $student->setGender($data->gender);
        $student->setAdmissionType($data->registrationtype);
        $student->setAdmission($data->discipline);
        $student->setLevelOfEntry($data->levelofentry);
        $student->setLevel($data->levelofentry);
        $student->setWaiting(1);
        $student->setAdmissionType($data->registrationtype);
        $student->setOLevelResult($data->certificate);
        $student->setNoOfSitting($data->noofsitting);
        $student->setDepartment($department);
        $em->persist($student);
//        Save data into student contacts
        // Check for duplicate entry
        $checkmail = $em->getRepository(StudentContact::class)->findOneBy([
            'email' => $data->email
        ]);
        $checkphone = $em->getRepository(StudentContact::class)->findOneBy([
            'telephone' => $data->telephone
        ]);
        // dump($checkphone,$checkmail);die();
        if ($checkmail != null || $checkphone != null) {
            $result = [
                'status' => 'error',
                'message' => 'The contact details you enter already exist!'
            ];
            $view = $this->view($result)
                ->setStatusCode(200)
                ->setHeaders($this->header)
                ->setFormat("json");

            $this->finalView = $view;

            return $this->handleView($this->finalView);
        }
        $studentContact = new StudentContact();
        $studentContact->setEmail($data->email);
        $studentContact->setTelephone((int)$data->telephone);
        $studentContact->setAddress($data->address);
        $studentContact->setStudent($student);
        $em->persist($studentContact);

//        Save data into students guardian table
        $studentguardian = new StudentGuardian();
        $studentguardian->setStudent($student);
        $studentguardian->setFullname($data->gfullname);
        $studentguardian->setTelephone((int)$data->gnumber);
        $studentguardian->setEmail($data->gemail);
        $studentguardian->setAddress($data->gaddress);
        $studentguardian->setRelationshipType($data->grelation);
        $em->persist($studentguardian);

//        Save data into student media
        $studentMedia = new StudentMedia();
        $studentMedia->setCertificate($data->result);
        $studentMedia->setPhoto($data->pphoto);
        $studentMedia->setLinkphoto($data->photolink);
        $studentMedia->setLinkcertificate($data->resultlink);
        $studentMedia->setStudent($student);
        $em->persist($studentMedia);
// Check for student sponsorship
        if ($data->sponsor != null || !empty($data->sponso)) {
            $sponsor = $em->getRepository(Students::class)->findOneBy([
                'matricNo' => base64_decode($data->sponsor)
            ]);
            $sponsorStaff = $em->getRepository(Staffs::class)->findOneBy([
                'code' => base64_decode($data->sponsor)
            ]);

            if ($sponsor != null) {
                $studentSponsorship = new StudentSponsorship();
                $studentSponsorship->setStudent($student)
                    ->setSponsor($sponsor);
                $em->persist($studentSponsorship);
//        dump($studentSponsorship);die();
            } elseif ($sponsorStaff != null) {
                $studentSponsorship = new StudentSponsorship();
                $studentSponsorship->setStudent($student)
                    ->setSponsorAgent($sponsorStaff);
                $em->persist($studentSponsorship);
            } else {
                $result = [
                    'status' => 'error',
                    'message' => 'Sponsor code does not exist!'
                ];
                $view = $this->view($result)
                    ->setStatusCode(200)
                    ->setHeaders($this->header)
                    ->setFormat("json");

                $this->finalView = $view;

                return $this->handleView($this->finalView);
            }
        }
        $em->flush();

        $result = [
            'status' => 'success',
            'message' => 'success'
        ];
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get most recent payments notification
     * @Route("/get_notifications", name="get_notifications")
     */
    public function get_notifications(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());

        $student = $em->getRepository(Students::class)->findOneBy([
            'matricNo' => $data->matricNo
        ]);
        $fees = $em->getRepository(Fees::class)->findBy([
            'student' => $student
        ], ['id' => 'DESC'], 3, 0);
        $result = [];
        foreach ($fees as $key => $fee) {
            $payments = $em->getRepository(Payments::class)->findBy([
                'theFee' => $fee
            ], ['id' => 'DESC'], 2, 0);
            // dump($payments);die();

            foreach ($payments as $key => $pay) {
                $result[] = $pay;

            }
        }

        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get payment details
     * @Route("/get_payment_details", name="get_payment_details")
     */
    public function get_payment_details(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        $fee = $em->getRepository(Fees::class)->find($data->id);

        $payments = $em->getRepository(Payments::class)->findBy([
            'theFee' => $fee
        ], ['id' => ' DESC']);
        // dump($fee,$payments);die();

        $result = [
            'fee' => $fee,
            'payments' => $payments
        ];
        $view = $this->view($result)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }

    /**
     * Get fee par student
     * @Route("/get_fees_student", name="get_fees_student")
     */
    public function get_fees_student(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent());
        $student = $em->getRepository(Students::class)->findBy([
            'matricNo' => $data->matricNo
        ]);
        $fees = $em->getRepository(Fees::class)->findBy([
            'student' => $student
        ], ['id' => 'DESC']);

        $view = $this->view($fees)
            ->setStatusCode(200)
            ->setHeaders($this->header)
            ->setFormat("json");

        $this->finalView = $view;

        return $this->handleView($this->finalView);
    }
}
