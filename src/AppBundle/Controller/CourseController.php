<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Course;
use AppBundle\Entity\Department;
use AppBundle\Entity\LecturerCourse;
use AppBundle\Entity\Lecturers;
use AppBundle\Entity\Levels;
use AppBundle\Entity\StudentCourse;
use AppBundle\Entity\Students;
use AppBundle\Form\CourseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Course controller.
 *
 * @Route("course")
 */
class CourseController extends Controller
{
    /**
     * Lists all course entities.
     *
     * @Route("/", name="course_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository('AppBundle:Course')->findAll();

        return $this->render('course/index.html.twig', array(
            'courses' => $courses
        ));
    }

    /**
     * Creates a new course entity.
     *
     * @Route("/new", name="course_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($course);
            $entityManager->flush();
            $message = 'The course ' . $course->getTitle() . ' has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('course_new');
        }

        return $this->render('course/new.html.twig', array(
            'course' => $course,
            'form' => $form->createView(),
        ));
    }

    /**
     * Show list of students for this courses
     *
     * @Route("/{id}/studentList", name="studentList")
     * @Method({"GET", "POST"})
     */
    public function studentList(Request $request, Course $course)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $em = $this->getDoctrine()->getManager();
        $obj = new \DateTime();
        $year = $obj->format('Y');
        $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $studentCourse = $em->getRepository(StudentCourse::class)->findBy([
            'course' => $course,
            'academicYear' => $academicYear
        ]);
        return $this->render('course/studentCourse.html.twig', array(
            'studentsCourse' => $studentCourse,
        ));
    }

    /**
     * Displays a form to edit an existing course entity.
     *
     * @Route("/{id}/edit", name="course_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Course $course)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $deleteForm = $this->createDeleteForm($course);
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('course_index');
        }

        return $this->render('course/edit.html.twig', array(
            'course' => $course,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a course entity.
     *
     * @Route("/{id}", name="course_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Course $course)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $em = $this->getDoctrine()->getManager();

        $em->remove($course);
        $em->flush();
        $message = 'The course was deleted successful';
        $this->get('session')->getFlashBag()->add('success', $message);

        return $this->redirectToRoute('course_index');
    }

    /**
     * Assign course to lecturer
     * @Route("/link_courses", name="link_course")
     */
    public function link_course(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $unassignedcourses = $em->getRepository('AppBundle:Course')->UnassCourse(1, 550);
        $assignedcourses = $em->getRepository('AppBundle:LecturerCourse')->findBy(['academicYear' => $academicYear]);
        $lecturers = $em->getRepository(Lecturers::class)->findAll();
//        dump($assignedcourses,$lecturers);die();
        if ($request->isMethod('POST')) {
//            dump($request);die();
            $courses = $request->get('courses');
            $lecturerid = $request->get('lecturer');
            $lecturer = $em->getRepository(Lecturers::class)->find($lecturerid);
            foreach ($courses as $value) {
                // Check if already assigned
                $lectCours = $em->getRepository('AppBundle:LecturerCourse')->findOneBy([
                    'lecturers' => $lecturer,
                    'courses' => $value,
                    'academicYear' => $academicYear
                ]);
                if ($lectCours != NULL) {
                    $message = 'One of the courses has already been assigned to the selected lecturer. Please try again!';
                    $this->get('session')->getFlashBag()->add('error', $message);
                    return $this->redirectToRoute('link_course');
                }
                $course = $em->getRepository('AppBundle:Course')->findOneBy([
                    'id' => $value
                ]);
                $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
                    'status' => 'active'
                ]);
                $course->setAssigned(true);
                $lectCour = new LecturerCourse();
                $lectCour->setLecturers($lecturer)
                    ->setAcademicYear($academicYear)
                    ->setCourses($course);
                $em->persist($course);
                $em->persist($lectCour);
                $em->flush();

            }
            $message = 'The course assignment was successful';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('link_course');
        }
        return $this->render('course/assigneCourse.html.twig', array(
            'assignedcourses' => $assignedcourses,
            'unassignedcourses' => $unassignedcourses,
            'lecturers' => $lecturers
        ));
    }


    /**
     * Assign course to lecturer
     * @Route("/unlink_courses/{id}", name="unlink_courses")
     */
    public function unlink_courses(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $assignedcourse = $em->getRepository('AppBundle:LecturerCourse')->find($request->get('id'));
        $course = $em->getRepository('AppBundle:Course')->find($assignedcourse->getCourses()->getId());
        $course->setAssigned(NULL);
        $em->persist($course);
        $em->remove($assignedcourse);
        $em->flush();

        $message = 'The course was successful un-assigned';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('link_course');
    }

    /**
     * Assign course to lecturer
     * @Route("/lectuerer_courses_assignment_history", name="lectuerer_courses_assignment_history")
     */
    public function lectuerer_courses_assignment_history(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $assignedcourses = $em->getRepository('AppBundle:LecturerCourse')->findBy([], ['id' => 'DESC']);
        return $this->render('course/assigneCourseHistort.html.twig', array(
            'assigned' => $assignedcourses,
        ));
    }

    /**
     * Assign course to specified student
     * @Route("/student_course_registration/{id}", name="student_course_registration")
     */
    public function courseAssignAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You do not have access to this page');
        }
        $em = $this->getDoctrine()->getManager();

        $student = $em->getRepository('AppBundle:Students')->find($request->get('id'));
        $coursesRegistered = $em->getRepository(StudentCourse::class)->findBy([
            'student' => $student,
            'academicYear'=> $em->getRepository(AcademicYear::class)->findOneBy(['status'=>'active'])
        ], ['id' => 'DESC']);
        $course = $em->getRepository(Course::class)->findAll();
        $department = $em->getRepository(Department::class)->findAll();
        $level = $em->getRepository(Levels::class)->findAll();
        return $this->render('course/registeredCourse.html.twig', array(
            'student' => $student,
            'level' => $level,
            'department' => $department,
            'coursesRegistered' => $coursesRegistered,
            'courses' => $course,
            'assign' => true
        ));
    }

    /**
     * Assign course for specified student
     * @Route("/add_course_registration/{courseid}/{studentid}", name="add_course_registration")
     */
    public function addcourseAssignAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $em = $this->getDoctrine()->getManager();

        $student = $em->getRepository('AppBundle:Students')->find($request->get('studentid'));
        $course = $em->getRepository(Course::class)->find($request->get('courseid'));

        $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $coursesRegistered = $em->getRepository(StudentCourse::class)->findBy([
            'student' => $student,
            'course' => $course,
            'academicYear' => $academicYear
        ]);
//        dump();die();
        if (empty($coursesRegistered)) {
            $registertNewCourse = new StudentCourse();
            $registertNewCourse->setCourse($course)
                ->setStudent($student)
                ->setAcademicYear($academicYear);
            $em->persist($registertNewCourse);
            $em->flush();
            $message = "You have successfully assigned this course to this student";
            $this->get('session')->getFlashBag()->add('success', $message);
        } else {
            $message = "This course has already been assigned to this student";
            $this->get('session')->getFlashBag()->add('success', $message);
        }

        return $this->redirectToRoute('student_course_registration', ['id' => $student->getId()]);
    }

    /**
     * Remove Assign course for specified student
     * @Route("/remove_course_registration/{id}", name="remove_course_registration")
     */
    public function removecourseAssignAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $em = $this->getDoctrine()->getManager();
        $coursesRegistered = $em->getRepository(StudentCourse::class)->find($request->get('id'));
        $em->remove($coursesRegistered);
        $em->flush();
        $message = "You have successfully unassigned this course to this student";
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('student_course_registration', ['id' => $coursesRegistered->getStudent()->getId()]);
    }

    /**
     * download course sheet for specified student
     * @Route("/download_course_sheet/{id}", name="download_course_sheet", requirements={"title"="\d+"})
     */
    public function downloadCourseSheetAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $student = $this->get("doctrine")->getManager()->getRepository(Students::class)->find($request->get('id'));
        $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $result = $this->get("doctrine")->getManager()->getRepository(StudentCourse::class)->courselist(1, 300, $student->getId(), $academicYear->getID());
        $render = $this->renderView('course/printCourse.html.twig', array(
            'courses' => $result,
            'student' => $student,
            'academicYear' => $academicYear,

        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=Course-sheet.pdf',
            )
        );

    }

    /**
     * Creates a form to delete a course entity.
     *
     * @param Course $course The course entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Course $course)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('course_delete', array('id' => $course->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

//    /**
//     * Assign a course to a lecturer
//     *
//     * @Route("/assign", name="assign")
//     */
//    public function assignAction(Request $request)
//    {
//        dump('yo');
//        die();
//        $em = $this->getDoctrine()->getManager();
//        $courses = $em->getRepository('AppBundle:Course')->findAll();
//        $lecturers = $em->getRepository(Lecturers::class)->findAll();
////        if ($request->isMethod('POST')) {
////
////        }
//        return $this->render('course/assigneCourse.html.twig', array(
//            'course' => $courses,
//            'lecturers' => $lecturers
//        ));
//
//    }
}
