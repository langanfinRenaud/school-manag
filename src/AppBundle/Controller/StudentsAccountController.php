<?php

namespace AppBundle\Controller;

use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use AppBundle\Service\Email;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Requests as mobizyRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StudentsAccountController extends Controller
{
    /**
     * @Route("/createAccount/{id}", name="createAccount")
     */
    public function createAccount(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));

        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student
        ]);
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student
        ]);
        if ($student->getAdmissionType() == 'regular') {
            $role = 'ROLE_REGULAR';
        } elseif ($student->getAdmissionType() == 'online') {
            $role = 'ROLE_ONLINE';
        }
        $studentPhoto = '';
        if (
        $studentPhoto = $studentMedia != null) {
            $studentPhoto = $studentMedia->getLinkphoto();
        }
        $data = [
            'fullname' => $student->getLastname() . ' ' . $student->getFirstname(),
            'matricNo' => $student->getMatricNo(),
            'studentPhoto' => $studentPhoto,
            'email' => $studentContact->getEmail(),
            'department' => $student->getDepartment()->getDepartmentCode(),
            'role' => $role,
            'levelOfEntry' => $student->getLeveL()
        ];
//        Encode data to be sent
        $encodedata = json_encode($data);
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100
        );
        $url = $this->getParameter('esgt_site') . 'create_user_account';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
//         return new Response($response->body);
        $result = json_decode($response->body);
//        dump($result);
//        die();

        if ($result != null && $result->created == 'created') {
            $student->setHasAccount(0);
            $em->persist($student);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $result->message);
            //        Send email to student with their credentials
            $this->container->get("app.mailer")->envoiEmail("Student OnlinePortal", $studentContact->getEmail(), "emails/onlineportal-credentials.html.twig", array(
                'username' => $student->getMatricNo(),
                'password' => $student->getMatricNo(),
                'fullname' => $student->getLastname() . ' ' . $student->getFirstname(),

            ));
        } else {
            if ($result->message == 'A student with that matric number exists already') {
                $student->setHasAccount(1);
                $em->persist($student);
                $em->flush();
            }
            $this->get('session')->getFlashBag()->add('error', $result->message);
        }


        return $this->redirectToRoute('students_index');
    }


    /**
     * @Route("/changeAccountStatus/{id}", name="changeAccountStatus")
     */
    public function changeAccountStatus(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        //        Encode data to be sent
        $encodedata = json_encode($student->getMatricNo());
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100
        );
        $url = $this->getParameter('esgt_site') . 'changeuserstatus';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
//        return new Response($response->body);
        $result = json_decode($response->body);
        $this->get('session')->getFlashBag()->add('success', $result);
        return $this->redirectToRoute('students_index');
    }

    /**
     * @Route("/makeeditable/{id}", name="makeeditable")
     */
    public function makeeditableStatus(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        //        Encode data to be sent
        $encodedata = json_encode($student->getMatricNo());
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100
        );
        $url = $this->getParameter('esgt_site') . 'makeditable';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
//        return new Response($response->body);
        $result = json_decode($response->body);
        $this->get('session')->getFlashBag()->add('success', $result);
        return $this->redirectToRoute('students_index');
    }

    /**
     * @Route("/resetPassword/{id}", name="resetPassword")
     */
    public function resetPassword(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));

        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student
        ]);
        $data = ['matricNo' => $student->getMatricNo()];
        $encodedata = json_encode($data);
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100
        );
        $url = $this->getParameter('esgt_site') . 'resetPassword';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
//        return new Response($response->body);

        //        Send email to student with their credentials
        $this->container->get("app.mailer")->envoiEmail("Student Online Portal", $studentContact->getEmail(), "emails/onlineportal-reset.html.twig", array(
            'username' => $student->getMatricNo(),
            'password' => $student->getMatricNo(),
            'fullname' => $student->getLastname() . ' ' . $student->getFirstname(),

        ));
        $result = json_decode($response->body);
        $this->get('session')->getFlashBag()->add('success', $result);
        return $this->redirectToRoute('students_index');
    }
}
