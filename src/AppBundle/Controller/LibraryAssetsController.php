<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Lecturers;
use AppBundle\Entity\LibraryAssets;
use AppBundle\Entity\LibraryMedia;
use AppBundle\Form\LibraryAssetsType;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Libraryasset controller.
 *
 * @Route("libraryassets")
 */
class LibraryAssetsController extends Controller
{
    /**
     * Lists all libraryAsset entities.
     *
     * @Route("/", name="libraryassets_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();

        $libraryAssets = $em->getRepository('AppBundle:LibraryAssets')->latestAsset(1,100);
        $currentuser = $this->getUser();
        if (in_array('ROLE_LECTURER', $currentuser->getRoles())) {
            $lecture= $em->getRepository(Lecturers::class)->findOneBy([
                'code'=>$currentuser->getUserName()
            ]);
            $libraryAssets = $em->getRepository('AppBundle:LibraryAssets')->latestAssetForLectures(1,100,$lecture->getLastname().' '.$lecture->getFirstname());
        }
        return $this->render('libraryassets/index.html.twig', array(
            'libraryAssets' => $libraryAssets,
        ));
    }

    /**
     * Creates a new libraryAsset entity.
     *
     * @Route("/new", name="libraryassets_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $libraryAsset = new Libraryassets();
        $form = $this->createForm(LibraryAssetsType::class, $libraryAsset);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $dob = new \DateTime($request->get('datePublished'));
            $libraryAsset->setTitle(strtoupper($libraryAsset->getTitle()));
            if (!in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles())) {
                $lecturer = $entityManager->getRepository(Lecturers::class)->findOneBy([
                    'code' => $this->getUser()->getUserName()
                ]);
                $libraryAsset->setAuthorName(strtoupper($lecturer->getLastname() . ' ' . $lecturer->getFirstname()));
            } else {
                $libraryAsset->setAuthorName(strtoupper($libraryAsset->getAuthorName()));
            }
            $libraryAsset->setPublishedBy(1);
            $libraryAsset->setColor($request->get('assetcolor'));
            $libraryAsset->setDatePublished($dob);
            $entityManager->persist($libraryAsset);
//            Save Media
            $files = $request->files->get('medias');
//            dump($files);die();
            $target = $this->getParameter("repertoire_library");
            $lienordre = $fileUploader->uploadDoc($files, $target, $libraryAsset->getTitle());
            $link = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/libraryassets/" . $lienordre;
            $libraryMedia = new LibraryMedia();
            $libraryMedia->setAlt($link);
            $libraryMedia->setAsset($libraryAsset);
            $entityManager->persist($libraryMedia);
            $entityManager->flush();
            $message = 'The library asset has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('libraryassets_index');
        }

        return $this->render('libraryassets/new.html.twig', array(
            'libraryAsset' => $libraryAsset,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a libraryAsset entity.
     *
     * @Route("/{id}", name="libraryassets_show")
     * @Method("GET")
     */
    public function showAction(LibraryAssets $libraryAsset)
    {
        $deleteForm = $this->createDeleteForm($libraryAsset);

        return $this->render('libraryassets/show.html.twig', array(
            'libraryAsset' => $libraryAsset,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing libraryAsset entity.
     *
     * @Route("/{id}/edit", name="libraryassets_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, LibraryAssets $libraryAsset, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $deleteForm = $this->createDeleteForm($libraryAsset);
        $form = $this->createForm(LibraryAssetsType::class, $libraryAsset);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $dob = new \DateTime($request->get('datePublished'));
            $libraryAsset->setTitle(strtoupper($libraryAsset->getTitle()));
            $libraryAsset->setAuthorName(strtoupper($libraryAsset->getAuthorName()));
            $libraryAsset->setPublishedBy(1);
            $libraryAsset->setColor($request->get('assetcolor'));
            $libraryAsset->setDatePublished($dob);
            $entityManager->persist($libraryAsset);
//            Save Media
            $files = $request->files->get('media');
            if (!empty($files)) {
                $target = $this->getParameter("repertoire_library");
                $lienordre = $fileUploader->uploadDoc($files, $target, $libraryAsset->getTitle());
                $link = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/libraryassets/" . $lienordre;
                $libraryMedia = $entityManager->getRepository(LibraryMedia::class)->findOneBy([
                    'asset' => $libraryAsset
                ]);
                $libraryMedia->setAlt($link);
                $libraryMedia->setAsset($libraryAsset);
                $entityManager->persist($libraryMedia);
            }

            $entityManager->flush();
            $message = 'The library asset has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('libraryassets_index');
        }

        return $this->render('libraryassets/edit.html.twig', array(
            'libraryAsset' => $libraryAsset,
            'form' => $form->createView(),
        ));
    }

    /**
     * Deletes a libraryAsset entity.
     *
     * @Route("/{id}/delete", name="libraryassets_delete")
     */
    public function deleteAction(Request $request, LibraryAssets $libraryAsset)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $em = $this->getDoctrine()->getManager();
        $libraryMedia = $em->getRepository(LibraryMedia::class)->findOneBy([
            'asset' => $libraryAsset
        ]);
        $em->remove($libraryMedia);
        $em->remove($libraryAsset);
        $em->flush();

        $message = 'The library asset has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('libraryassets_index');
    }

    /**
     * Creates a form to delete a libraryAsset entity.
     *
     * @param LibraryAssets $libraryAsset The libraryAsset entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LibraryAssets $libraryAsset)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libraryassets_delete', array('id' => $libraryAsset->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
