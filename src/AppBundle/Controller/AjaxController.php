<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Course;
use AppBundle\Entity\Department;
use AppBundle\Entity\FeesType;
use AppBundle\Entity\Levels;
use AppBundle\Entity\ScoreSheet;
use AppBundle\Entity\StudentExams;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     **
     * @Route("/filterCourseForRegistration", options={"expose"=true}, name="filterCourseForRegistration")
     * @Method({"GET", "POST"})
     */
    public function filterCourseForRegistration(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
//        dump($request->get('level'),$request->get('department'));die();
        if (!empty($request->get('level')) && !empty($request->get('department'))) {
            $level = $em->getRepository(Levels::class)->find($request->get('level'));
            $department = $em->getRepository(Department::class)->find($request->get('department'));
            $courses = $em->getRepository('AppBundle:Course')->findBy([
                'level' => $level,
                'department' => $department
            ]);
        } elseif (!empty($request->get('level')) && empty($request->get('department'))) {
            $level = $em->getRepository(Levels::class)->find($request->get('level'));
            $courses = $em->getRepository('AppBundle:Course')->findBy([
                'level' => $level,
            ]);
        } elseif (empty($request->get('level')) && !empty($request->get('department'))) {
            $department = $em->getRepository(Department::class)->find($request->get('department'));
            $courses = $em->getRepository('AppBundle:Course')->findBy([
                'department' => $department
            ]);
        } elseif (empty($request->get('level')) && empty($request->get('department'))) {
            $courses = null;
        }


        return $this->container->get('templating')->renderResponse('course/list.html.twig', [
            'courses' => $courses,
            'assign' => true
        ]);
    }

    /**
     **
     * @Route("/filterStudentByDepartmentNlevel", options={"expose"=true}, name="filterStudentByDepartmentNlevel")
     * @Method({"GET", "POST"})
     */
    public function filterStudentByDepartmentNlevel(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
        if (empty($request->get('level'))) {
            $student = $em->getRepository(Students::class)->findBy([
                'department' => $request->get('department'),
            ]);
        } elseif (empty($request->get('department'))) {
            $student = $em->getRepository(Students::class)->findBy([
                'levelOfEntry' => $request->get('level'),
            ]);
        } else {
            $student = $em->getRepository(Students::class)->findBy([
                'levelOfEntry' => $request->get('level'),
                'department' => $request->get('department'),
            ]);
        }
        $mutiple = $request->get('mutiple');
        return $this->container->get('templating')->renderResponse('students/include/ajax_student_select_list.html.twig', [
            'students' => $student,
            'mutiple' => $mutiple
        ]);
    }

    /**
     **
     * @Route("/filterScoreboardStudentByDepartmentNlevel", options={"expose"=true}, name="filterScoreboardStudentByDepartmentNlevel")
     * @Method({"GET", "POST"})
     */
    public function filterScoreboardStudentByDepartmentNlevel(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
        if (empty($request->get('level'))) {
            $student = $em->getRepository(Students::class)->findBy([
                'department' => $request->get('department'),
                'waiting'=>0
            ]);
        } elseif (empty($request->get('department'))) {
            $student = $em->getRepository(Students::class)->findBy([
                'levelOfEntry' => $request->get('level'),
                'waiting'=>0
            ]);
        } else {
            $student = $em->getRepository(Students::class)->findBy([
                'levelOfEntry' => $request->get('level'),
                'department' => $request->get('department'),
                'waiting'=>0
            ]);
        }
//        dump($student,$request->get('level'),$request->get('department'));die();
        $scores = [];
        foreach ($student as $stud) {
            $scores[] = $em->getRepository(StudentExams::class)->getScoreBoard($stud->getId());
        }
//        dump($scores);die();
        $department = $em->getRepository('AppBundle:Department')->find($request->get('department'));
        $level = $em->getRepository('AppBundle:Levels')->findOneBy([
            'levelcode'=> $request->get('level')
        ]);
//        dump($department,$level);die();
        return $this->container->get('templating')->renderResponse('scoreboard/list.html.twig', [
            'studentscores' => $scores,
            'department' => $department,
            'level' => $level,
        ]);
    }


    /**
     **
     * @Route("/getStudentInfo", options={"expose"=true}, name="getStudentInfo")
     * @Method({"GET", "POST"})
     */
    public function getStudentInfo(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
        $matricNo = $request->get('matricNo');
        $student = $em->getRepository(Students::class)->find($matricNo);
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student
        ]);
        //    dump($student,$studentMedia,$matricNo);die();
        return $this->container->get('templating')->renderResponse('students/include/ajax_student_info.html.twig', [
            'student' => $student,
            'media' => $studentMedia
        ]);
    }

    /**
     **
     * @Route("/getStudentInfoArch", options={"expose"=true}, name="getStudentInfoArch")
     * @Method({"GET", "POST"})
     */
    public function getStudentInfoArch(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
        $matricNo = $request->get('matricNo');
        $student = $em->getRepository(Students::class)->find($matricNo);
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student
        ]);
//        dump($studentMedia);die();
        return $this->container->get('templating')->renderResponse('students/include/ajax_student_info.html.twig', [
            'student' => $student,
            'media' => $studentMedia
        ]);
    }


    /**
     **
     * @Route("/getStudentScoreSheet", options={"expose"=true}, name="getStudentScoreSheet")
     * @Method({"GET", "POST"})
     */
    public function getStudentScoreSheet(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $em = $this->getDoctrine()->getManager();
        $matricNo = $request->get('matricNo');
        $student = $em->getRepository(Students::class)->find($matricNo);
        $academicYear = $this->get("doctrine")->getManager()->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $scoreseet = $em->getRepository(ScoreSheet::class)->findBy([
            'student' => $student
            // 'academicYear' => $academicYear
        ]);
//        dump($scoreseet);die();
        $scoresheet = [];
        foreach ($scoreseet as $key => $score) {

            $scoresheet = $this->gradingSystem($score);
        }
        return $this->container->get('templating')->renderResponse('transcript/include/generatedTranscript.html.twig', [
            'scoreseet' => $scoresheet
        ]);
    }


    public function gradingSystem($score)
    {
        if ($score->getTotal() > 80) {
            $letterGrad = 'A+';
            $credit = 5;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 75 || $score->getTotal() >= 79) {
            $letterGrad = 'A';
            $credit = 4.5;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 70 || $score->getTotal() >= 74) {
            $letterGrad = 'AB';
            $credit = 4;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 65 || $score->getTotal() >= 69) {
            $letterGrad = 'B';
            $credit = 3.5;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 60 || $score->getTotal() >= 64) {
            $letterGrad = 'BC';
            $credit = 3;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 55 || $score->getTotal() >= 59) {
            $letterGrad = 'C';
            $credit = 2.5;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 50 || $score->getTotal() >= 54) {
            $letterGrad = 'CD';
            $credit = 2;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 45 || $score->getTotal() >= 49) {
            $letterGrad = 'D';
            $credit = 1.5;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 40 || $score->getTotal() >= 44) {
            $letterGrad = 'E';
            $credit = 1;
            $gp = ($credit * $score->getCourse()->getUnits());
        } elseif ($score->getTotal() >= 0 || $score->getTotal() >= 39) {
            $letterGrad = 'F';
            $credit = 0;
            $gp = ($credit * $score->getCourse()->getUnits());
        }
        $scoresheet[] = [
            'id' => $score->getId(),
            'student' => $score->getStudent(),
            'course' => $score->getCourse(),
            'total' => $score->getTotal(),
            'letterGrad' => $letterGrad,
            'credit' => $credit,
            'gp' => $gp,
            'semester' => $score->getSemester()

        ];
        return $scoresheet;
    }

    /**
     **
     * @Route("/studentdynamicFilter", options={"expose"=true}, name="studentdynamicFilter")
     * @Method({"GET", "POST"})
     */
    public function studentdynamicFilter(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $keyword = $request->get('firstname');

        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('AppBundle:Students')->searchStudent($keyword);
//        dump($students);die();
        return $this->container->get('templating')->renderResponse('students/list.html.twig', [
            'students' => $students,
        ]);
    }

    /**
     **
     * @Route("/coursedynamicFilter", options={"expose"=true}, name="coursedynamicFilter")
     * @Method({"GET", "POST"})
     */
    public function coursedynamicFilter(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }
        $title = $request->get('title');
        $units = $request->get('units');

        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('AppBundle:Course')->searchCourses($title, $units);
//        dump($students);die();
        return $this->container->get('templating')->renderResponse('course/list.html.twig', [
            'courses' => $course,
        ]);
    }

    /**
     **
     * @Route("/auditFees", options={"expose"=true}, name="auditFees")
     * @Method({"GET", "POST"})
     */
    public function auditFees(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('PERMISSION ERROR');
        }

        $fee_type = $request->get('fee_type');
        $fee_status = $request->get('fee_status');
        $startdate = $request->get('startdate');
        $enddate = $request->get('enddate');
        $student = $request->get('student');
        $em = $this->getDoctrine()->getManager();
        $fees = $em->getRepository('AppBundle:Fees')->getAudit($fee_type, $fee_status, $startdate, $enddate, $student);
//        dump($fees);
//        die();
        return $this->container->get('templating')->renderResponse('fees/includes/ajaxList.html.twig', [
            'fees' => $fees,
        ]);
    }


    /**
     **
     * @Route("/addFees", options={"expose"=true}, name="addFees")
     * @Method({"GET", "POST"})
     */
    public function addFees(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $feeType = $em->getRepository(FeesType::class)->findAll();

        return $this->container->get('templating')->renderResponse('fees/includes/admissionFormFeeList.html.twig', [
            'fees' => $feeType,
        ]);
    }
}
