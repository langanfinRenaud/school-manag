<?php
/**
 * Created by PhpStorm.
 * User: utilisateur
 * Date: 1/12/2020
 * Time: 1:42 PM
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Students;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Requests as mobizyRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * Feestype controller.
 *
 * @Route("fimac")
 */
class FimacController extends Controller
{
    /**
     * Lists all student entities.
     *
     * @Route("/", name="fimac_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $studenttype = "AppBundle\Entity\Students";
        $serializer = $this->get('jms_serializer');
        $options = array(
            //'auth' => array('someuser', 'password')
            'timeout' => 200,
            'connect_timeout' => 100,
        );
        $url = $this->getParameter('url_fimac_site') . 'get_all_student';
        $response = mobizyRequest::post($url, $options, [], ['timeout' => 120]);
//        return new JsonResponse($response->body);
        $result = json_decode($response->body, true);
//        dump($result);die();
//        $student = $serializer->deserialize($response->body, 'array<'.$studenttype.'>', 'json');
        return $this->render('fimac/index.html.twig', array(
            'students' => $result,
        ));
    }
}