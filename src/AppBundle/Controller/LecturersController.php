<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LecturerMedia;
use AppBundle\Entity\Lecturers;
use AppBundle\Form\LecturersType;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Lecturer controller.
 *
 * @Route("lecturers")
 */
class LecturersController extends Controller
{
    /**
     * Lists all lecturer entities.
     *
     * @Route("/", name="lecturers_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $lecturers = $em->getRepository('AppBundle:Lecturers')->getLecturerList(1, 50);
//dump($lecturers);die();
        return $this->render('lecturers/index.html.twig', array(
            'lecturers' => $lecturers,
        ));
    }

    /**
     * Creates a new lecturer entity.
     *
     * @Route("/new", name="lecturers_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $lecturer = new Lecturers();
        $form = $this->createForm(LecturersType::class, $lecturer);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {


            $entityManager = $this->getDoctrine()->getManager();
            /*
             * Set data in enetity and save lecturer to database
             */
            $entityManager->persist($lecturer);
            /*
             * Get file form and move it to prescribed folder
             */
            $files = $request->files->get('media');
            if ($files != null) {
                $target = $this->getParameter("repertoire_lecturer");
                $lienordre = $fileUploader->uploadImage($files, $target);
                $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/lecturers/" . $lienordre;
                $lecturerMedia = new LecturerMedia();
                $lecturerMedia->setLecturer($lecturer)
                    ->setFilename($lienordre)
                    ->setLink($lienAbsolu);
                $entityManager->persist($lecturerMedia);
            }

            $entityManager->flush();
            $message = 'The lecturer ' . $lecturer->getFirstname() . ' ' . $lecturer->getLastname() . ' has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('lecturers_index');
        }

        return $this->render('lecturers/new.html.twig', array(
            'lecturer' => $lecturer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a lecturer entity.
     *
     * @Route("/{id}", name="lecturers_show")
     * @Method("GET")
     */
    public function showAction(Lecturers $lecturer)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($lecturer);

        return $this->render('lecturers/show.html.twig', array(
            'lecturer' => $lecturer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lecturer entity.
     *
     * @Route("/{id}/edit", name="lecturers_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Lecturers $lecturer, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($lecturer);
        $editForm = $this->createForm('AppBundle\Form\LecturersType', $lecturer);
        $editForm->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();
        $lecturerMedia = $entityManager->getRepository(LecturerMedia::class)->findOneBy([
            'lecturer' => $lecturer
        ]);
        if ($editForm->isSubmitted()) {
            /*
            * Set data in enetity and save lecturer to database
            */
            $entityManager->persist($lecturer);
            /*
             * Get file form and move it to prescribed folder
             */
            $files = $request->files->get('media');
            if ($files != null) {
                $target = $this->getParameter("repertoire_lecturer");
                $lienordre = $fileUploader->uploadImage($files, $target);
                $lienAbsolu = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/lecturers/" . $lienordre;


                if (empty($lecturerMedia)) {
                    $lecturerMedia = new LecturerMedia();
                }
                $lecturerMedia->setLecturer($lecturer)
                    ->setFilename($lienordre)
                    ->setLink($lienAbsolu);
                $entityManager->persist($lecturerMedia);
            }
            $entityManager->flush();
            $message = 'The lecturer ' . $lecturer->getFirstname() . ' ' . $lecturer->getLastname() . ' has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('lecturers_index');
        }

        return $this->render('lecturers/edit.html.twig', array(
            'lecturer' => $lecturer,
            'form' => $editForm->createView(),
            'media'=> $lecturerMedia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a lecturer entity.
     *
     * @Route("/{id}/delete", name="lecturers_delete")
     */
    public function deleteAction(Request $request, Lecturers $lecturer)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
//dump($lecturer);die();
            $em = $this->getDoctrine()->getManager();
            $lecturer->setIsDeleted(true);
            $em->persist($lecturer);
            $em->flush();

        $message = 'The lecturer ' . $lecturer->getFirstname() . ' ' . $lecturer->getLastname() . ' has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('lecturers_index');
    }

    /**
     * Creates a form to delete a lecturer entity.
     *
     * @param Lecturers $lecturer The lecturer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lecturers $lecturer)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lecturers_delete', array('id' => $lecturer->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Create account for lecturers
     *
     * @Route("/{id}/createAccount", name="createAccountlecturer")
     * @Method("GET")
     */
    public function createAccountAction(Lecturers $lecturer)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $user_manager = $this->get('fos_user.user_manager');
//        Check if user exist
        $user = $user_manager->findUserByEmail($lecturer->getEmail());
        if ($user == null) {
//        Create Lecturers user account
            $rand = substr(str_shuffle('0123456789'), 0, 6);
            $newuser = $user_manager->createUser();
            $newuser->setUsername($lecturer->getCode());
            $newuser->setUsernameCanonical($lecturer->getCode());
            $newuser->setEmail($lecturer->getEmail());
            $newuser->setEmailCanonical($lecturer->getEmail());
            $newuser->setEnabled(true);
            $newuser->addRole('ROLE_LECTURER');
            $newuser->setPlainPassword('password');

//            MODIFY LECTURER TABLE
            $em = $this->getDoctrine()->getManager();
            $user_manager->updateUser($newuser);
            $lecturer->setHasAccount(true);
            $em->persist($lecturer);
            $em->flush();

            $this->container->get("app.mailer")->envoiEmail("Lecturer OnlinePortal", $lecturer->getEmail(), "emails/lecturerCred.html.twig", array(
            'code' => $lecturer->getCode(),
            'email' => $lecturer->getEmail(),
            'password' =>'password',
            'gender' =>$lecturer->getGender(),
            'fullname' => $lecturer->getLastname() . ' ' . $lecturer->getFirstname()));
            $message = 'The lecturers account was created sucessfully. The lecturer as also been notified via email';
            $this->get('session')->getFlashBag()->add('success', $message);
        }else{
            $message = 'The email entered already belongs to a specific lecturers account. pleas try again using another email.';
            $this->get('session')->getFlashBag()->add('error', $message);
        }

        return $this->redirectToRoute('lecturers_index');
    }
}
