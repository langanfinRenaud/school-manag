<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Archive;
use AppBundle\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Archive controller.
 *
 * @Route("archive")
 */
class ArchiveController extends Controller
{
    /**
     * Lists all archive entities.
     *
     * @Route("/", name="archive_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $archives = $em->getRepository('AppBundle:Archive')->findAll();

        return $this->render('archive/index.html.twig', array(
            'archives' => $archives,
        ));
    }

    /**
     * Creates a new archive entity.
     *
     * @Route("/new", name="archive_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $archive = new Archive();
        $form = $this->createForm('AppBundle\Form\ArchiveType', $archive);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $file = $archive->getFile();
            $target = $this->getParameter("repertoire_archive");
            $fileName = $this->generateUniqueFilename() . ' ' . $file->guessExtension();
            $file->move(
                $target,
                $fileName
            );
            $archive->setFile($fileName);
            $em->persist($archive);
            $em->flush();
            $message = 'The archive has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('archive_index');
        }

        return $this->render('archive/new.html.twig', array(
            'archive' => $archive,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing archive entity.
     *
     * @Route("/{id}/edit", name="archive_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Archive $archive)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($archive);
        $editForm = $this->createForm('AppBundle\Form\ArchiveType', $archive);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($archive->getFile() !== null) {
                $file = $archive->getFile();
                $target = $this->getParameter("repertoire_archive");
                $fileName = $this->generateUniqueFilename() . ' ' . $file->guessExtension();
                $file->move(
                    $target,
                    $fileName
                );
                $archive->setFile($fileName);
            }
            $em->getManager()->flush();
            $message = 'The archive has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('archive_index');
        }

        return $this->render('archive/edit.html.twig', array(
            'archive' => $archive,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a archive entity.
     *
     * @Route("/{id}", name="archive_delete")
     */
    public function deleteAction(Request $request, Archive $archive)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($archive);
        $em->flush();

        $message = 'The archive has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('archive_index');
    }

    /**
     * Creates a form to delete a archive entity.
     *
     * @param Archive $archive The archive entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Archive $archive)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('archive_delete', array('id' => $archive->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @return string
     */
    private function generateUniqueFilename()
    {
        return md5(uniqid());
    }
}
