<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Department;
use AppBundle\Entity\Levels;
use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentGuardian;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use AppBundle\Entity\StudentCourse;
use AppBundle\Entity\ScheduledExam;
use AppBundle\Entity\StudentSponsorship;
use AppBundle\Form\StudentContactType;
use AppBundle\Form\StudentGuardianType;
use AppBundle\Form\StudentsType;
use AppBundle\Service\Email;
use AppBundle\Service\FileUploader;
use AppBundle\Service\CryptData;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Requests as mobizyRequest;

class StudentAdmissionController extends Controller
{

    /**
     * @Route("/waiting", name="waiting")
     */
    public function waiting()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $waiting = $em->getRepository(Students::class)->findBy([
            'waiting' => 1
        ]);
        return $this->render('students/waiting.html.twig', [
            'student' => $waiting,
        ]);
    }

    /**
     * @Route("/admit/{id}", name="admit")
     */
    public function admit(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        $noofstudentindepat = $em->getRepository(Students::class)->findBy([
            'department' => $student->getDepartment(),
            'levelOfEntry' => $student->getLevelOfEntry()
        ]);

//       Get Academic year
        $academicyear= $em->getRepository(AcademicYear::class)->findOneBy([
            'status'=> 'active'
        ]);
//         dump($student,$noofstudentindepat,$academicyear);die();

        $years= $academicyear->getYear();
        $exploded=explode('/',$years);

        $year= $exploded[1];

        $matricNumber = $this->getMatric($student->getDepartment()->getDepartmentCode(), count($noofstudentindepat), $student->getLevelOfEntry(),$year,null);

        $student->setMatricNo($matricNumber);
        $student->setWaiting(0);
        $student->setAcademicYear($academicyear);
        $em->persist($student);

        $studentSponsorship= $em->getRepository(StudentSponsorship::class)->findOneBy([
            'student'=> $student
        ]);
        if($studentSponsorship != null){
            $studentSponsorship->setAdmitted(true);
            $em->persist($studentSponsorship);
        }
        
        //        dump($student, count($noofstudentindepat),$matricNumber);
//        die();
        $em->flush();
        $message = 'The student has been admited sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('students_index');
    }

    /**
     * @Route("/reject/{id}", name="reject")
     */
    public function reject(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student
        ]);
        $studentGuardian = $em->getRepository(StudentGuardian::class)->findOneBy([
            'student' => $student
        ]);
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student
        ]);
        $studentSponsor = $em->getRepository(StudentSponsorship::class)->findOneBy([
            'student' => $student
        ]);
        $em->remove($student);
        $em->remove($studentContact);
        $em->remove($studentGuardian);
        $em->remove($studentMedia);
        if($studentSponsor != null){
            $em->remove($studentSponsor);
        }
        $em->flush();
        $message = 'The student has been rejected sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);

        return $this->redirectToRoute('students_index');
    }
public function getMatric($dept, $noofstudentindepat, $level,$year,$noofstudent)
    {
        if($noofstudent == null){
            // $rand= substr(mt_rand(), 0, 4);
            if ($level == '100') {
                $year = $year - 1;
            } elseif ($level == '200') {
                $year = $year - 2;
            } elseif ($level == '300') {
                $year = $year - 3;
            } elseif ($level == '400') {
                $year = $year - 4;
            }
            if ($noofstudentindepat > 1) {
                $noofstudent = $noofstudentindepat + 1;
            } else {
                $noofstudent = 1;
            }
            $matric = 'ESGT/' . substr($year,2) . '/' . $dept . '/000' . $noofstudent;
        }else{
            if ($level == '100') {
                $year = $year - 1;
            } elseif ($level == '200') {
                $year = $year - 2;
            } elseif ($level == '300') {
                $year = $year - 3;
            } elseif ($level == '400') {
                $year = $year - 4;
            }
            $noofstudent= $noofstudent+1;
            $matric = 'ESGT/' . substr($year,2) . '/' . $dept . '/000' . $noofstudent;
        }


        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->findOneBy([
            'matricNo'=> $matric
        ]);
        if($student != null){
            $matric=  $this->getMatric($dept,$noofstudentindepat,$level,$year,null);
        }
        return $matric;
    }
}
