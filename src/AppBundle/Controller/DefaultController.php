<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Course;
use AppBundle\Entity\Department;
use AppBundle\Entity\LecturerCourse;
use AppBundle\Entity\Lecturers;
use AppBundle\Entity\LibraryAssets;
use AppBundle\Entity\Students;
use AppBundle\Entity\SuperUser;
use AppBundle\Entity\Fees;
use AppBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{

   

    /**
     * @Route("/", name="verification")
     */
    public function verification()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $currentuser = $this->getUser();
        $adminStats = 0;
        $lecturerStats = [];
        $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $waiting = $em->getRepository(Students::class)->findBy([
                'waiting' => 1
            ],['id'=>'DESC']);
            
            $totalStudents = $em->getRepository(Students::class)->findAll();
            $totalCourses = $em->getRepository(Course::class)->findAll();
            $totalLibraryAsset = $em->getRepository(LibraryAssets::class)->findAll();
            $totalLecturers = $em->getRepository(Lecturers::class)->findAll();
            $adminStats = [
                'totalStudents' => count($totalStudents),
                'totalCourses' => count($totalCourses),
                'totalLibraryAsset' => count($totalLibraryAsset),
                'totalLecturers' => count($totalLecturers),
            ];
            return $this->render('default/index.html.twig', [
                'waiting' => $waiting,
                'lecturerStats' => $lecturerStats,
                'adminStats' => $adminStats
            ]);
        }
        if (in_array('ROLE_LECTURER', $currentuser->getRoles())) {
            $lecture = $em->getRepository(Lecturers::class)->findOneBy([
                'code' => $currentuser->getUsername()
            ]);
            $totalCoursr4Semster = $em->getRepository(LecturerCourse::class)->findBy([
                'lecturers' => $lecture,
                'academicYear'=> $academicYear
            ]);
            $lecturerStats = [
                'totalCoursr4Semster' => count($totalCoursr4Semster)
            ];
            return $this->render('default/index.html.twig', [
                'lecturerStats' => $lecturerStats,
                'totalCoursr4Semster' => $totalCoursr4Semster
            ]);
        }

    }

    /**
     * @Route("/user_settings", name="userSetting")
     */
    public function userSetting(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $currentuser = $this->getUser();
        $fees = $em->getRepository(Fees::class)->findBy(['createdBy'=> $currentuser->getUsername()]);
        return $this->render('default/userSetting.html.twig',[
            'fees'=> count($fees)]);
    }

    /**
     * @Route("/modify_passe",name="modify_passe")
     */
    public function modify_passeAction(Request $request,UserManagerInterface $userManager)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $session = new Session();
        $user = $this->getUser();
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
//        $userManager = $this->container->get('fos_user.user_manager');
//        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        
        $user = $userManager->findUserByUsername($user->getUsername());
//        $oldpasse = $request->get('oldpasse');
//        dump($user);die();
        $newpasse = $request->get('newpasse');
        $confirmepasse = $request->get('confirmepasse');


//        Check if new password are the same
        if ($newpasse != $confirmepasse) {
            $message = "The new passwords are not the same!";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('userSetting');
        }

//            Set new password
        $user->setPlainPassword($newpasse);
        $userManager->updateUser($user);

        $message = "Your password has been modified successfully kindly <b style='font-weight: bold;color: #ffa92b'>logout</b> to apply changes finally";
        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('userSetting');
    }
    
    /**
     * @Route("/stats", name="stats")
     */
    public function stats()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();

        return $this->render('default/include/stats.html.twig', [

        ]);
    }

    /**
     * @Route("/{id}/change_user_status", name="changeUserStatus")
     */
    public function changeUserStatus(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('username' => $request->get('id')));
        if ($user->isEnabled()) {
            $user->setEnabled(false);
            $message = 'User account has been deactivated';
        } else {
            $user->setEnabled(true);
            $message = 'User account has been activated';
        }
        $userManager->updateUser($user);
    }

    /**
     * @Route("/create_super_user", name="create_new_super_user")
     */
    public function createNewSuperUser(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $session = new Session();

        $users = $em->getRepository(User::class)->getlisteAdmin();
        if ($request->isMethod('POST')) {
            $user = $request->get('user');
            $password = $request->get('newpassword');
            $confpassword = $request->get('confpassword');

            $user =  $em->getRepository(User::class)->findOneByUserName($user);
            $existpassword = $em->getRepository(SuperUser::class)->findOneBy([
                'user' => $user
            ]);
            if (!empty($existpassword) && $existpassword != null) {
                $message = "This user already has a password! ";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('create_new_super_user');
            }
            if ($password === $confpassword) {

                $hashed_password = password_hash($password, PASSWORD_BCRYPT);
//                dump($password,$hashed_password);die();

                $transpassword = new SuperUser();
                $transpassword->setUser($user);
                $transpassword->setPassword($hashed_password);
                $em->persist($transpassword);
                $em->flush();
                $message = "You have sucessfully registered a super user password!";
                $session->getFlashBag()->add('success', $message);
                return $this->redirectToRoute('dashboard');
            }else{
                $message = "The passwords do not match";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('create_new_super_user');
            }


        }
        return $this->render('default/createnewsuperuser.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/changeSession", name="changeSession")
     */
    public function changeSession()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) ) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        // Get academic year
        $academicYear = $em->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        return $this->render('default/changesessionAuth.html.twig',[
            'academicYear'=> $academicYear
        ]);
    }

    /**
     * @Route("/confirmchangeSession", name="confirmchangeSession")
     */
    public function confirmChangeSession(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $user =  $em->getRepository(User::class)->findOneByUserName($currentuser->getUsername());
        $existpassword = $em->getRepository(SuperUser::class)->findOneBy([
            'user' => $user
        ]);
        if (!empty($existpassword) && $existpassword != null) {
            $hashedPwdCheck = password_verify($request->get('superusercode'), $existpassword->getPassword());
            if($hashedPwdCheck == FALSE){
                $message= 'Wrong password entered';
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('changeSession');
            }
        }else{
            $message= 'Kindly enter a password to proceed';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('changeSession');
        }
        $em = $this->getDoctrine()->getManager();
        // Change academic year
        $oldAcademicYear = $em->getRepository(AcademicYear::class)->findOneBy([
            'status' => 'active'
        ]);
        $oldYear = $oldAcademicYear->getYear();
        $expl = explode('/', $oldYear);
        $oldAcademicYear->setStatus('ended')
        ->setEndDate(new \DateTime('now'));
        $em->persist($oldAcademicYear);
        $newAcademicYear = new AcademicYear();
        
        // Unassign all courses
        $em->getRepository(Course::class)->unAssignCourses();

        // If Semeter field is empty
        if(!empty($request->get('semester'))){
            $newAcademicYear->setStatus('active')
            ->setYear($oldYear)
            ->setSemester($request->get('semester'))
            ->setCreated(new \DateTime('now'));
            $em->persist($newAcademicYear);
            $students = $em->getRepository(Students::class)->findAll();
            foreach ($students as $key => $student) {
               $student->setAcademicYear($newAcademicYear);
               $em->persist($student);
           }
           $em->flush();
           return $this->redirectToRoute('dashboard');
       }

       $newyear = $expl[1] . '/' . ($expl[1] + 1);
       $newAcademicYear->setStatus('active')
       ->setYear($newyear)
       ->setSemester('1st')
       ->setCreated(new \DateTime('now'));
       $em->persist($newAcademicYear);

        // Change Student level
       $students = $em->getRepository(Students::class)->findAll();
       foreach ($students as $key => $student) {
        $currentLevel = $student->getLevel();
        if ($currentLevel === '400') {
            $student->setLevel($currentLevel);
        } else {
            $currentLevel = $currentLevel + 100;
            $student->setLevel($currentLevel);
        }
        $student->setAcademicYear($newAcademicYear);
        $em->persist($student);
    }
//        dump($oldAcademicYear, $newAcademicYear,$students);die();

    $em->flush();
    return $this->redirectToRoute('dashboard');
}


}
