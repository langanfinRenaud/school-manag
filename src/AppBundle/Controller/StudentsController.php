<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AcademicYear;
use AppBundle\Entity\Levels;
use AppBundle\Entity\ScheduledExam;
use AppBundle\Entity\StudentContact;
use AppBundle\Entity\StudentGuardian;
use AppBundle\Entity\StudentMedia;
use AppBundle\Entity\Students;
use AppBundle\Entity\StudentSponsorship;
use AppBundle\Form\StudentContactType;
use AppBundle\Form\StudentGuardianType;
use AppBundle\Form\StudentsType;
use AppBundle\Service\FileUploader;
use Requests as mobizyRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Student controller.
 *
 * @Route("students")
 */
class StudentsController extends Controller
{
    /**
     * Lists all student entities.
     *
     * @Route("/", name="students_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $em = $this->getDoctrine()->getManager();

            $students = $em->getRepository('AppBundle:Students')->last100(1, 50);
            return $this->render('students/index.html.twig', array(
                'students' => $students,
            ));
        } else {
            throw $this->createNotFoundException('You cannot access this page!');
        }
    }

    /**
     * Lists of all archivd students
     *
     * @Route("/archived_students", name="archived_students")
     * @Method("GET")
     */
    public function archivedStudentsAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $em = $this->getDoctrine()->getManager();

            $students = $em->getRepository('AppBundle:Students')->archivedStudent(1, 100);
            return $this->render('students/archivedStudent.html.twig', array(
                'students' => $students,
            ));
        } else {
            throw $this->createNotFoundException('You cannot access this page!');
        }
    }

    /**
     * Creates a new student entity.
     *
     * @Route("/new", name="students_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, FileUploader $fileUploader)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $entityManager = $this->getDoctrine()->getManager();

        $level = $entityManager->getRepository(Levels::class)->findAll();

        $student = new Students();
//        $form = $this->createForm('AppBundle\Form\StudentsType', $student);
        $studentContact = new StudentContact();
        $studentGuardian = new StudentGuardian();
        $form = $this->createForm(StudentsType::class, $student);
        $form1 = $this->createForm(StudentContactType::class, $studentContact);
        $form2 = $this->createForm(StudentGuardianType::class, $studentGuardian);
//        dump($form,$form1,$form2);die();
        $form->handleRequest($request);
        $form1->handleRequest($request);
        $form2->handleRequest($request);
        if ($form->isSubmitted()) {
            //Check to see if student email or telephone exists
            $checkingEmail = $entityManager->getRepository(StudentContact::class)->findBy([
                'email' => $studentContact->getEmail(),
            ]);
            $checkingTelephone = $entityManager->getRepository(StudentContact::class)->findBy([
                'telephone' => $studentContact->getTelephone(),
            ]);
            if ($checkingEmail != null || $checkingTelephone != null) {
                $message = 'The email or telephone number belongs to an existing student';
                $this->get('session')->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('students_new');
            }
            $academicyear = $entityManager->getRepository(AcademicYear::class)->findOneBy([
                'status' => 'active',
            ]);
            $dob = new \DateTime($request->get('dob'));
            $noofstudentindepat = $entityManager->getRepository(Students::class)->findBy([
                'department' => $student->getDepartment(),
                'level' => $student->getLevelOfEntry(),
            ]);
            // dump($noofstudentindepat);die();

// Get Matric no
            $years = $academicyear->getYear();
            $exploded = explode('/', $years);

            $year = $exploded[1];

            $matricNumber = $this->getMatric($student->getDepartment()->getDepartmentCode(), count($noofstudentindepat), $student->getLevelOfEntry(), $year, null);
// check for duplicate
            $checking = $entityManager->getRepository(Students::class)->findBy([
                'matricNo' => $matricNumber,
            ]);
            if ($checking != null) {
                $matricNumber = $this->getMatric($student->getDepartment()->getDepartmentCode(), (count($noofstudentindepat) + 1), $student->getLevelOfEntry(), $year, null);
            }

            $student->setDateOfBirth($dob)
                ->setWaiting(false)
                ->setMatricNo($matricNumber)
                ->setAcademicYear($academicyear)
                ->setLevel($student->getLevelOfEntry())
                ->setIsdeleted(0);

            $entityManager->persist($student);
//            Save Media Passport picture
            $files = $request->files->get('pphoto');
            $file2 = $request->files->get('olevelresult');
            if ($files != null && $file2 != null) {
                $target = $this->getParameter("repertoire_student_photo");
                $lienordrePP = $fileUploader->uploadImageDoc($files, $target);
                $linkPP = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentspasseport/" . $lienordrePP;
//             Save Media Certificate
                $target = $this->getParameter("repertoire_student_certificate");
                $lienordreCert = $fileUploader->uploadImageDoc($file2, $target);
                $linkCert = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentscertificate/" . $lienordreCert;

//          Save data into student media
                $studentMedia = new StudentMedia();
                $studentMedia->setCertificate($lienordreCert);
                $studentMedia->setLinkcertificate($linkCert);
                $studentMedia->setPhoto($lienordrePP);
                $studentMedia->setLinkphoto($linkPP);
                $studentMedia->setStudent($student);
                $entityManager->persist($studentMedia);
            }
//            Save data into student Contact
            $studentContact->setStudent($student);
            $entityManager->persist($studentContact);

//            Save data into student Guardian
            $studentGuardian->setStudent($student);
            $entityManager->persist($studentGuardian);
// dump($student,$studentContact,$studentGuardian,$studentMedia);die();
            $entityManager->flush();
            $message = 'The student ' . $student->getFirstname() . ' ' . $student->getLastname() . ' has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('students_new');
        }

        return $this->render('students/new.html.twig', array(
            'student' => $student,
            'level' => $level,
            'form' => $form->createView(),
            'form1' => $form1->createView(),
            'form2' => $form2->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing student entity.
     *
     * @Route("/{id}/edit", name="students_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, FileUploader $fileUploader, Students $student)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($student);
        $em = $this->getDoctrine()->getManager();
        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student,
        ]);
        if ($studentContact == null) {
            $studentContact = new StudentContact();

        }
        $studentGuardian = $em->getRepository(StudentGuardian::class)->findOneBy([
            'student' => $student,
        ]);
        if ($studentGuardian == null) {
            $studentGuardian = new StudentGuardian();

        }
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student,
        ]);
        $level = $em->getRepository(Levels::class)->findAll();
        $form = $this->createForm(StudentsType::class, $student);

        $form1 = $this->createForm(StudentContactType::class, $studentContact);
        $form2 = $this->createForm(StudentGuardianType::class, $studentGuardian);
        $form->handleRequest($request);
        $form1->handleRequest($request);
        $form2->handleRequest($request);
        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $student->setLevel($request->get('level'))
                ->setDateOfBirth(new \DateTime($request->get('dob')));
            $entityManager->persist($student);
//            Save data into student Contact

            $studentContact->setStudent($student);
            $entityManager->persist($studentContact);
//            Save data into student Guardian
            $studentGuardian->setStudent($student);
            $entityManager->persist($studentGuardian);

            $files = $request->files->get('pphoto');
            $file2 = $request->files->get('olevelresult');

//          Save data into student media
            $studentMedia = $entityManager->getRepository(StudentMedia::class)->findOneBy([
                'student' => $student,
            ]);
            if (!empty($studentMedia)) {
//                dump($studentMedia);
//                die();
                if (!empty($files) || $files != null) {
                    //            Save Media Passport picture
                    $target = $this->getParameter("repertoire_student_photo");
                    $lienordrePP = $fileUploader->uploadImageDoc($files, $target);
                    $linkPP = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentspasseport/" . $lienordrePP;
//                    dump($lienordrePP,$linkPP);die();

                    $studentMedia->setPhoto($lienordrePP);
                    $studentMedia->setLinkphoto($linkPP);
                }
                if (!empty($file2) || $file2 != null) {
                    //             Save Media Certificate
                    $target = $this->getParameter("repertoire_student_certificate");
                    $lienordreCert = $fileUploader->uploadImageDoc($file2, $target);
                    $linkCert = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentscertificate/" . $lienordreCert;
                    $studentMedia->setCertificate($lienordreCert);
                    $studentMedia->setLinkcertificate($linkCert);

                }
//                $linkPP = $studentMedia->getPhoto();
                $lienordrePP = $studentMedia->getLinkphoto();
                    $studentMedia->setStudent($student);
                $entityManager->persist($studentMedia);
            } else {
                if ($file2 != null || $files != null) {
//            Save Media Passport picture
                    $target = $this->getParameter("repertoire_student_photo");

                    $lienordrePP = $fileUploader->uploadImageDoc($files, $target);
                    $linkPP = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentspasseport/" . $lienordrePP;

//             Save Media Certificate
                    $targetcert = $this->getParameter("repertoire_student_certificate");
                    $lienordreCert = $fileUploader->uploadImageDoc($file2, $targetcert);
                    $linkCert = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() . "/uploads/studentscertificate/" . $lienordreCert;

                    $studentMedia = new StudentMedia();
                    $studentMedia->setCertificate($lienordreCert);
                    $studentMedia->setLinkcertificate($linkCert);
                    $studentMedia->setPhoto($lienordrePP);
                    $studentMedia->setLinkphoto($linkPP);
                    $studentMedia->setStudent($student);
                    $entityManager->persist($studentMedia);
                }
            }

            $entityManager->flush();
//            Modify data online
            if ($student->getAdmissionType() == 'regular') {
                $role = 'ROLE_REGULAR';
            } elseif ($student->getAdmissionType() == 'online') {
                $role = 'ROLE_ONLINE';
            }
            $this->updateOnlineStudentInfo($student, $role, $lienordrePP, $studentContact);
            $message = 'The student ' . $student->getFirstname() . ' ' . $student->getLastname() . ' has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('students_index');
        }
//        dump($level);
        //        die();
        return $this->render('students/edit.html.twig', array(
            'student' => $student,
            'level' => $level,
            'form' => $form->createView(),
            'studentContact' => $studentContact,
            'studentGuardian' => $studentGuardian,
            'studentMedia' => $studentMedia,
            'form1' => $form1->createView(),
            'form2' => $form2->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/studentSponsorList", name="studentSponsorList")
     */
    public function studentSponsorList()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository(StudentSponsorship::class)->findBy([], ['id' => 'DESC']);
//        dump($list);die();
        return $this->render('students/sponsorlist.html.twig', [
            'list' => $list,
        ]);
    }

    /**
     * @Route("/paystudentSponsorList/{id}", name="paystudentSponsorList")
     */
    public function paystudentSponsor(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $action = $em->getRepository(StudentSponsorship::class)->find($request->get('id'));
        $action->setPaid(true)
        ->setPaidDate(new \DateTime());
        $em->persist($action);
//        dump($action);die();
        $em->flush();
        $message = 'Student has been paid for sponsorship';
        $this->get('session')->getFlashBag()->add('success', $message);
//        dump($list);die();
        return $this->redirectToRoute('studentSponsorList');
    }

    /**
     * Lists of all archivd students
     *
     * @Route("/archived_students_list", name="archived_students")
     * @Method("GET")
     */
    public function archivedStudentsListAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $em = $this->getDoctrine()->getManager();

            $students = $em->getRepository('AppBundle:Students')->archivedStudent(1, 100);
            return $this->render('students/archivedStudent.html.twig', array(
                'students' => $students,
            ));
        } else {
            throw $this->createNotFoundException('You cannot access this page!');
        }
    }

    /**
     * Restore student info
     *
     * @Route("/restore_archived_students/{id}", name="restore_archived_students")
     * @Method("GET")
     */
    public function restoreArchivedStudentsAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $em = $this->getDoctrine()->getManager();

            $students = $em->getRepository('AppBundle:Students')->findOneBy([
                'id' => $request->get('id'),
            ]);
            $students->setIsdeleted(null);
            $em->persist($students);
            $em->flush();

            return $this->redirectToRoute('students_index');
        } else {
            throw $this->createNotFoundException('You cannot access this page!');
        }
    }

    /**
     * Archive a student entity.
     *
     * @Route("/archived_student/{id}", name="archived_student")
     * @Method("GET")
     */
    public function archived_studentsAction(Request $request, Students $student)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        $student->setIsdeleted(1);
        // Check if student doesnt have an online portal account
        $data = [
            'matricNo' => $student->getMatricNo(),
        ];
        //        Encode data to be sent
        $encodedata = json_encode($data);
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100,
        );
        $url = $this->getParameter('esgt_site') . 'deactivateuser';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
        $result = json_decode($response->body);

        $em->flush();
        $message = 'The student has been archived sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);

        return $this->redirectToRoute('students_index');
    }

    /**
     * Deletes a student entity.
     *
     * @Route("/delete/{id}", name="students_delete")
     */
    public function deleteAction(Request $request, Students $student)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->find($request->get('id'));
        $student->setIsdeleted(1)
            ->setHasAccount(null);

        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student,
        ]);

        $studentGuardian = $em->getRepository(StudentGuardian::class)->findOneBy([
            'student' => $student,
        ]);
        $studentMedias = $em->getRepository(StudentMedia::class)->findBy([
            'student' => $student,
        ]);
        $studentSponsors = $em->getRepository(StudentSponsorship::class)->findOneBy([
            'student' => $student,
        ]);

        $studentScheduledExams = $em->getRepository(ScheduledExam::class)->findBy([
            'student' => $student,
        ]);
        $em->persist($student);

        if ($studentContact != null) {
            $em->remove($studentContact);
        }
        if ($studentGuardian != null) {
            $em->remove($studentGuardian);
        }
        if ($studentScheduledExams != null) {
            foreach ($studentScheduledExams as $studentScheduledExam) {
                $em->remove($studentScheduledExam);
            }
        }
        if ($studentMedias != null) {
            foreach ($studentMedias as $studentMedia) {
                $em->remove($studentMedia);
            }
            $em->remove($studentMedia);
        }
        if ($studentSponsors != null) {
            foreach ($studentSponsors as $studentSponsor) {
                $em->remove($studentSponsor);
            }
        }
        // Check if student doesnt have an online portal account
        $data = [
            'matricNo' => $student->getMatricNo(),
        ];
        //        Encode data to be sent
        $encodedata = json_encode($data);
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100,
        );
        $url = $this->getParameter('esgt_site') . 'deleteStudent';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
        $result = json_decode($response->body);

        $em->flush();
        $message = 'The student has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);

        return $this->redirectToRoute('students_index');
    }

    /**
     * Displays a form to edit an existing student entity.
     *
     * @Route("/generate/student/{id}/ID-card", name="generateID")
     * @Method({"GET", "POST"})
     */
    public function generateIDAction(Request $request, Students $student)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $studentContact = $em->getRepository(StudentContact::class)->findOneBy([
            'student' => $student,
        ]);
        $studentMedia = $em->getRepository(StudentMedia::class)->findOneBy([
            'student' => $student,
        ]);
        if ($studentMedia == null || $studentMedia->getPhoto() == null) {
            $message = 'The student doesn\'t have a picture';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('students_index');
        }
        // dump($studentMedia);die();
        $render = $this->renderView('students/idCardTemp.html.twig', array(
            'student' => $student,
            'studentContact' => $studentContact,
            'studentMedia' => $studentMedia,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render, array(
                'orientation' => 'portrait',
                'page-height' => 92.4,
                'page-width' => 114.6,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 300,

            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=StudentID.pdf',
            )
        );

    }

    /**
     * Displays a form to edit an existing student entity.
     *
     * @Route("/min2", name="min2")
     */
    public function min2Action(Request $request, Students $student)
    {
        $render = $this->renderView('default/minist2.html.twig');
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($render, array(
                'orientation' => 'portrait',
                // 'page-height' => 92.4,
                // 'page-width'  => 114.6,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 300,

            )),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename=mins2.pdf',
            )
        );
    }

    /**
     * Creates a form to delete a student entity.
     *
     * @param Students $student The student entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Students $student)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('students_delete', array('id' => $student->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function getMatric($dept, $noofstudentindepat, $level, $year, $noofstudent)
    {
        if ($noofstudent == null) {
            // $rand= substr(mt_rand(), 0, 4);
            if ($level == '100') {
                $year = $year - 1;
            } elseif ($level == '200') {
                $year = $year - 2;
            } elseif ($level == '300') {
                $year = $year - 3;
            } elseif ($level == '400') {
                $year = $year - 4;
            }
            if ($noofstudentindepat > 1) {
                $noofstudent = $noofstudentindepat + 1;
            } else {
                $noofstudent = 1;
            }
            $matric = 'ESGT/' . substr($year, 2) . '/' . $dept . '/000' . $noofstudent;
        } else {
            if ($level == '100') {
                $year = $year - 1;
            } elseif ($level == '200') {
                $year = $year - 2;
            } elseif ($level == '300') {
                $year = $year - 3;
            } elseif ($level == '400') {
                $year = $year - 4;
            }
            $noofstudent = $noofstudent + 1;
            $matric = 'ESGT/' . substr($year, 2) . '/' . $dept . '/000' . $noofstudent;
        }

        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository(Students::class)->findOneBy([
            'matricNo' => $matric,
        ]);
        if ($student != null) {
            $matric=  $this->getMatric($dept,$noofstudentindepat,$level,$year,$noofstudent);
        }
        return $matric;
    }

    public function updateOnlineStudentInfo($student, $role, $studentPhoto, $studentContact)
    {
        $data = [
            'fullname' => $student->getLastname() . ' ' . $student->getFirstname(),
            'matricNo' => $student->getMatricNo(),
            'studentPhoto' => $studentPhoto,
            'email' => $studentContact->getEmail(),
            'department' => $student->getDepartment()->getDepartmentCode(),
            'role' => $role,
            'levelOfEntry' => $student->getLeveL()
        ];
        $encodedata = json_encode($data);
//        Send data
        $options = array(
            'timeout' => 200,
            'connect_timeout' => 100
        );
        $url = $this->getParameter('esgt_site') . 'modifyOnEdit';
        $response = mobizyRequest::post($url, $options, $encodedata, ['timeout' => 120]);
        return true;
    }
}
