<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Course;
use AppBundle\Entity\Exams;
use AppBundle\Entity\ScoreSheet;
use AppBundle\Entity\StudentExams;
use AppBundle\Entity\Students;
use AppBundle\Entity\StudentCourse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * ScoreSheet controller.
 *
 * @Route("score_sheets")
 */
class ScoreSheetController extends Controller
{
    /**
     * Lists all score_sheets entities.
     *
     * @Route("/", name="list_all")
     * @Method("GET")
     */
    public function list_all()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();

        $list = $em->getRepository(ScoreSheet::class)->findAll('');
        return $this->render('scoresheets/listAll.html.twig', array(
            'list' => $list,
        ));
    }

    /**
     * Generate score_sheets entities.
     *
     * @Route("/generate_scoresheet/{course}", name="generate_scoresheet")
     * @Method("GET")
     */
    public function generate_scoresheet(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $courseid = $request->get('course');
        $course = $em->getRepository(Course::class)->find($courseid);
//        Check if scoresheet doesnt exist
        $obj = new \DateTime();
        $year = $obj->format('Y');

        $academicYear = $em->getRepository('AppBundle:AcademicYear')->findOneBy([
            'status' => 'active'
        ]);
        $scoreseet = $em->getRepository(ScoreSheet::class)->findBy([
            'course' => $course,
            'academicYear' => $academicYear
        ]);
        $exam = $this->get("doctrine")->getManager()->getRepository(Exams::class)->findOneBy([
           'course'=> $course
        ]);
        $studentExam = $this->get("doctrine")->getManager()->getRepository(StudentExams::class)->findBy([
            'exam' => $exam,
        ]);
        $studentlist = $em->getRepository(StudentCourse::class)->findBy([
            'course' => $course,
            'academicYear' => $academicYear,
        ]);
        foreach ($studentlist as $student) {
            $studentExam = $this->get("doctrine")->getManager()->getRepository(StudentExams::class)->findOneBy([
                'exam' => $exam,
                'students' => $student->getStudent(),
                'academicYear' => $academicYear,
            ]);
            if(!empty($studentExam)){
                $student->{'onlinescore'}= $studentExam->getStudentpoint();
                $student->{'onlineexam'}= true;
            }else{
                $student->{'onlinescore'}= 0;
                $student->{'onlineexam'}= false;
            }
        }
//        dump($studentlist[1]->onlinescore,$exam);die();
//                dump($studentExam,$scoreseet,$studentlist);die();
        return $this->render('scoresheets/listAll.html.twig', [
            'list' => $studentlist,
            'generatedlist' => $scoreseet,
            'course' => $course,
        ]);
    }

    /**
     * GeneratSavee score_sheets entities.
     *
     * @Route("/save_scoresheet", name="save_scoresheet")
     */
    public function save_scoresheet(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $students = $request->get('students');
        $testscores = $request->get('testscores');
        $examscores = $request->get('examscores');
        $attendancescores = $request->get('attendancescores');
        $assignmentscore = $request->get('assignmentscore');
        $semester = $request->get('semester');
        $courseid = $request->get('course');
        $course = $em->getRepository(Course::class)->find($courseid);
        $currentuser = $this->getUser();
        $academicYear = $em->getRepository('AppBundle:AcademicYear')->findOneBy([
            'status' => 'active'
        ]);
        foreach ($students as $key => $s) {
            // dump($students,$s);die();

            $student = $em->getRepository(Students::class)->find($s);

            $scoreSheet = $em->getRepository(ScoreSheet::class)->findOneBy([
                'course' => $course,
                'academicYear' => $academicYear,
                'student' => $student
            ]);
            if (empty($scoreSheet)) {
                $scoreSheet = new ScoreSheet();
            }
            $scoreSheet->setStudent($student)
            ->setSemester($semester)
            ->setAcademicYear($academicYear)
            ->setCourse($course)
            ->setTest($testscores[$key])
            ->setExam($examscores[$key])
            ->setAttendance($attendancescores[$key])
            ->setAssignment($assignmentscore[$key])
            ->setTotal($testscores[$key] + $examscores[$key] + $attendancescores[$key] + $assignmentscore[$key])
            ->setCreatedBy($currentuser);
            $em->persist($scoreSheet);
            $em->flush();

        }
        $message = 'The scoresheet for course ' . $course->getTitle() . ' has been generated sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('generate_scoresheet',['course'=>$courseid]);
    }
}
