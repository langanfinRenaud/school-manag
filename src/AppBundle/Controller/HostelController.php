<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Hostel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Hostel controller.
 *
 * @Route("hostel")
 */
class HostelController extends Controller
{
    /**
     * Lists all hostel entities.
     *
     * @Route("/", name="hostel_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hostels = $em->getRepository('AppBundle:Hostel')->findAll();

        return $this->render('hostel/index.html.twig', array(
            'hostels' => $hostels,
        ));
    }

    /**
     * Creates a new hostel entity.
     *
     * @Route("/new", name="hostel_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $hostel = new Hostel();
        $form = $this->createForm('AppBundle\Form\HostelType', $hostel);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hostel);
            $em->flush();
$message = 'Hostel bed '.$hostel->getBedNo().' has been created and assigned to '.$hostel->getStudent()->getLastname().' '.$hostel->getStudent()->getFirstname();
$this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('hostel_index');
        }

        return $this->render('hostel/new.html.twig', array(
            'hostel' => $hostel,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing hostel entity.
     *
     * @Route("/{id}/edit", name="hostel_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Hostel $hostel)
    {
        $editForm = $this->createForm('AppBundle\Form\HostelType', $hostel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $this->getDoctrine()->getManager()->flush();
$message = 'Hostel bed '.$hostel->getBedNo().' has been modified and assigned to '.$hostel->getStudent()->getLastname().' '.$hostel->getStudent()->getFirstname();
$this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('hostel_index');
        }

        return $this->render('hostel/edit.html.twig', array(
            'hostel' => $hostel,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a hostel entity.
     *
     * @Route("/delete/{id}", name="hostel_delete")
     */
    public function deleteAction(Request $request, Hostel $hostel)
    {
        

            $em = $this->getDoctrine()->getManager();
            $em->remove($hostel);
            $em->flush();
$message = 'Hostel bed has been deleted sucesfully';
$this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('hostel_index');
    }

    /**
     * Creates a form to delete a hostel entity.
     *
     * @param Hostel $hostel The hostel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Hostel $hostel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hostel_delete', array('id' => $hostel->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
