<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fees;
use AppBundle\Entity\Payments;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fee controller.
 *
 * @Route("fees")
 */
class FeesController extends Controller
{
    /**
     * Lists all fee entities.
     *
     * @Route("/", name="fees_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $students = $em->getRepository('AppBundle:Students')->findBy([], ['id' => 'DESC']);
        $fees = $em->getRepository('AppBundle:Fees')->findBy([], ['id' => 'DESC']);
        $feestype = $em->getRepository('AppBundle:FeesType')->findBy([], ['id' => 'DESC']);

        return $this->render('fees/index.html.twig', array(
            'fees' => $fees,
            'feestype' => $feestype,
            'students' => $students,
        ));
    }

    /**
     * Creates a new fee entity.
     *
     * @Route("/new", name="fees_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $fee = new Fees();
        $form = $this->createForm('AppBundle\Form\FeesType', $fee);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
//            dump($fee->getTypeId(),$request);die();
            $em = $this->getDoctrine()->getManager();
            if ($fee->getType()->getId() != 5) {
                $checkFees = $em->getRepository('AppBundle:Fees')->findBy([
                    'student' => $fee->getStudent(),
                    'type' => $fee->getType(),
                    'academicYear' => $fee->getAcademicYear(),
                    'semester' => $fee->getSemester()
                ]);

                if ($checkFees != null) {
                    $message = 'Sorry but the same fees, for the same student, can not be created, in the same academic year';
                    $this->get('session')->getFlashBag()->add('error', $message);
                    return $this->redirectToRoute('fees_index');
                }
            }
            $fee->setCreatedBy($currentuser);
//            ->setTypeId();
            $em->persist($fee);
            $em->flush();
            $message = 'The fees for student ' . $fee->getStudent()->getFirstname() . ' ' . $fee->getStudent()->getLastname() . ' has been created sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('fees_index');
        }

        return $this->render('fees/new.html.twig', array(
            'fee' => $fee,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fee entity.
     *
     * @Route("/show/{id}", name="fees_show")
     * @Method("GET")
     */
    public function showAction(Fees $fee)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        // $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        $deleteForm = $this->createDeleteForm($fee);
        $em = $this->getDoctrine()->getManager();

        $payments = $em->getRepository('AppBundle:Payments')->findBy([
            'theFee' => $fee
        ], ['id' => ' DESC']);
        $totalPaid = 0;
        if ($fee->getTotalPaid() == null) {
            $allPaymentsPrepay = $em->getRepository('AppBundle:Payments')->findBy([
                'theFee' => $fee,
            ], ['id' => ' DESC']);

            foreach ($allPaymentsPrepay as $paid) {
                $totalPaid += $paid->getAmount();
            }
            $fee->setTotalPaid($totalPaid);
            $em->persist($fee);
            $em->flush();
        }
        return $this->render('fees/show.html.twig', array(
            'fee' => $fee,
            'payments' => $payments,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fee entity.
     *
     * @Route("/{id}/edit", name="fees_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Fees $fee)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        $deleteForm = $this->createDeleteForm($fee);
        $editForm = $this->createForm('AppBundle\Form\FeesType', $fee);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $fee->setModifiedBy($currentuser)
                ->setModifiedOn(new \DateTime());
            $em->persist($fee);
            $em->flush();
            $message = 'The fees for student ' . $fee->getStudent()->getFirstname() . ' ' . $fee->getStudent()->getLastname() . ' has been modified sucessfully';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('fees_index');

        }

        return $this->render('fees/edit.html.twig', array(
            'fee' => $fee,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fee entity.
     *
     * @Route("/delete/fees/{id}", name="fees_delete")
     */
    public function deleteAction(Request $request, Fees $fee)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        // if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) || !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
        //     throw $this->createNotFoundException('You cannot access this page!');
        // }
        // $form = $this->createDeleteForm($fee);
        // $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        // Delete payments as well
        $payments = $em->getRepository('AppBundle:Payments')->findBy(['theFee' => $fee]);
        foreach ($payments as $payment) {
            $em->remove($payment);
        }
        $em->remove($fee);
        $em->flush();
        $message = 'The fees was deleted succefully!';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('fees_index');
    }

    /**
     * Creates a form to delete a fee entity.
     *
     * @param Fees $fee The fee entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fees $fee)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fees_delete', array('id' => $fee->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
