<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FAQ;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Faq controller.
 *
 * @Route("faq")
 */
class FAQController extends Controller
{
    /**
     * Lists all fAQ entities.
     *
     * @Route("/", name="faq_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $fAQs = $em->getRepository('AppBundle:FAQ')->findAll();

        return $this->render('faq/index.html.twig', array(
            'fAQs' => $fAQs,
        ));
    }

    /**
     * Creates a new fAQ entity.
     *
     * @Route("/new", name="faq_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $fAQ = new Faq();
        $form = $this->createForm('AppBundle\Form\FAQType', $fAQ);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $fAQ->setAnswer($request->get('answer'));
            $em->persist($fAQ);
            $em->flush();
            $message= 'The FAQ has been successfully created';
$this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('faq_index');
        }

        return $this->render('faq/new.html.twig', array(
            'fAQ' => $fAQ,
            'form' => $form->createView(),
        ));
    }

    
    /**
     * Displays a form to edit an existing fAQ entity.
     *
     * @Route("/{id}/edit", name="faq_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, FAQ $fAQ)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($fAQ);
        $editForm = $this->createForm('AppBundle\Form\FAQType', $fAQ);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
             $em = $this->getDoctrine()->getManager();
            $fAQ->setAnswer($request->get('answer'));
            $em->persist($fAQ);
            $em->flush();
            $message= 'The FAQ has been successfully modified';
$this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('faq_index');
        }

        return $this->render('faq/edit.html.twig', array(
            'fAQ' => $fAQ,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fAQ entity.
     *
     * @Route("/{id}", name="faq_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, FAQ $fAQ)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles()) && !in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            throw $this->createAccessDeniedException('You cannot access this page!');
        }
        // $form = $this->createDeleteForm($fAQ);
        // $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $em->remove($fAQ);
            $em->flush();

        $message= 'The FAQ has been successfully deleted';
$this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('faq_index');
    }

    /**
     * Creates a form to delete a fAQ entity.
     *
     * @param FAQ $fAQ The fAQ entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FAQ $fAQ)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('faq_delete', array('id' => $fAQ->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
