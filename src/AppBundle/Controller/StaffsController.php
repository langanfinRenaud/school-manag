<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Staffs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Staff controller.
 *
 * @Route("staffs")
 */
class StaffsController extends Controller
{
    /**
     * Lists all staff entities.
     *
     * @Route("/", name="staffs_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $em = $this->getDoctrine()->getManager();

        $staffs = $em->getRepository('AppBundle:Staffs')->findAll();

        return $this->render('staffs/index.html.twig', array(
            'staffs' => $staffs,
        ));
    }

    /**
     * Creates a new staff entity.
     *
     * @Route("/new", name="staffs_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $staff = new Staffs();
        $form = $this->createForm('AppBundle\Form\StaffsType', $staff);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($staff);
            $em->flush();
//            $link= 'http://localhost/esgt_website/web/app_dev.php/application-form/'.base64_encode($staff->getCode());
            $link = 'https://esgt-benin.com/application-form/' . base64_encode($staff->getCode());
            $this->container->get("app.mailer")->envoiEmail("Referal Link", $staff->getEmail(), "emails/staffAlert.html.twig", array(
                'link' => $link,
                'staff' => $staff
            ));
            $message = 'Staff has be create successfully !';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('staffs_index');
        }

        return $this->render('staffs/new.html.twig', array(
            'staff' => $staff,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a staff entity.
     *
     * @Route("/{id}", name="staffs_show")
     * @Method("GET")
     */
    public function showAction(Staffs $staff)
    {
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $deleteForm = $this->createDeleteForm($staff);

        return $this->render('staffs/show.html.twig', array(
            'staff' => $staff,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing staff entity.
     *
     * @Route("/{id}/edit", name="staffs_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Staffs $staff)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $editForm = $this->createForm('AppBundle\Form\StaffsType', $staff);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            $role = [$staff->getFonction()];
            $userManager = $this->get('fos_user.user_manager');
            $admin = $userManager->findUserBy(array('username' => $staff->getCode()));
            if ($admin != null) {
                $admin->setRoles($role);
                $userManager->updateUser($admin);
            }

            $entityManager->flush();
            $message = 'Staff has be modified successfully !';
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('staffs_index');
        }

        return $this->render('staffs/edit.html.twig', array(
            'staff' => $staff,
            'form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a staff entity.
     *
     * @Route("/delete/{id}", name="staffs_delete")
     */
    public function deleteAction(Request $request, Staffs $staff)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            throw $this->createNotFoundException('You cannot access this page!');
        }
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('username' => $staff->getCode()));
        if ($user != null || !empty($user)) {
            $userManager->deleteUser($user);
        }
        $message = 'Staff has been deleted sucessfully';

        $em = $this->getDoctrine()->getManager();
        $em->remove($staff);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('staffs_index');
    }

    /**
     * Creates a form to delete a staff entity.
     *
     * @param Staffs $staff The staff entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Staffs $staff)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($staff);
        $em->flush();
        $message = 'The staff ' . $staff->getFirstname() . ' ' . $staff->getLastname() . ' has been deleted sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('staffs_index');

    }

    /**
     * Create account for lecturers
     *
     * @Route("/{id}/createAccount", name="createAccountstaff")
     * @Method("GET")
     */
    public function createAccountAction(Staffs $staff)
    {

        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $currentuser = $this->getUser();
        if (!in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }

        $user_manager = $this->get('fos_user.user_manager');
//        Check if user exist
        $user = $user_manager->findUserByEmail($staff->getEmail());
        if ($user == null) {
//            Create Lecturers user account
            $rand = substr(str_shuffle('0123456789'), 0, 6);
            $newuser = $user_manager->createUser();
            $newuser->setUsername($staff->getCode());
            $newuser->setUsernameCanonical($staff->getCode());
            $newuser->setEmail($staff->getEmail());
            $newuser->setEmailCanonical($staff->getEmail());
            $newuser->setEnabled(true);
            $newuser->addRole($staff->getFonction());
            $newuser->setPlainPassword('password');

//            MODIFY LECTURER TABLE
            $em = $this->getDoctrine()->getManager();
            $user_manager->updateUser($newuser);
            $staff->setHasAccount(true);
            $em->persist($staff);
            $em->flush();

            $this->container->get("app.mailer")->envoiEmail("YOUR CREDENTIALS!", $staff->getEmail(), "emails/lecturerCred.html.twig", array(
                'code' => $staff->getCode(),
                'email' => $staff->getEmail(),
                'firstname' => $staff->getEmail(),
                'lastname' => $staff->getEmail(),
            ));
        } else {
            $message = 'The staff acccount already exists';
            $this->get('session')->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('staffs_index');
        }

        $message = 'The staff acccount has been created sucessfully';
        $this->get('session')->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('staffs_index');
    }
}
