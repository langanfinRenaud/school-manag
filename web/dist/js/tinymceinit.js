function applyMCE() {
    tinyMCE.init({
        mode: "textareas",
        editor_selector: "tinymce-enabled-message",
        // selector: '.area{{ counter }}',
        height: 100,
        theme: 'modern',
        // language: 'fr_FR',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | image',
        image_list: [
    {title: 'My image 1', value: 'https://www.tinymce.com/my1.gif'},
    {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
  ],
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
//            file_browser_callback_types: 'file image media',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
}

applyMCE();

function AddRemoveTinyMce(editorId) {
    if (tinyMCE.get(editorId)) {
        tinyMCE.EditorManager.execCommand('mceFocus', false, editorId);
        tinyMCE.EditorManager.execCommand('mceRemoveEditor', true, editorId);

    } else {
        tinymce.EditorManager.execCommand('mceAddEditor', false, editorId);
    }
}

$('#addquestion').click(function () {
    var counter = $('#counter');

    var questionfld = '<div class="exam-question-space border border-success pr-3 pl-3 pb-3">\n' +
        '                                        <div class="row">\n' +
        '                                            <div class="col-md-12 pr-0 text-center w-100">\n' +
        '                                                <div class="d-flex flex-row bd-highlight mb-3 w-100">\n' +
        '                                                    <div class="col-sm-12 col-md-3">\n' +
        '                                                        <h4 class=" font-weight-bold text-left">\n' +
        '                                                            Qusetion ' + counter.text() + '</h4>\n' +
        '                                                    </div>\n' +
        '                                                    <div class="col-sm-12 col-md-4 mt-3 mt-3">\n' +
        '                                                        <div class="input-group mb-3">\n' +
        '                                                            <div class="input-group-prepend">\n' +
        '                                                                <span class="input-group-text" id="basic-addon1"> <i\n' +
        '                                                                            class="fa fa-clock"></i></span>\n' +
        '                                                            </div>\n' +
        '                                                            <input type="number" class="form-control"\n' +
        '                                                                   placeholder="Question point"\n' +
        '                                                                   aria-label="Username" name="points[]"\n' +
        '                                                                   aria-describedby="basic-addon1" required>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                    <div class="col-sm-12 col-md-4 mt-3 mt-3">\n' +
        '                                                        <div class="input-group mb-3">\n' +
        '                                                            <div class="input-group-prepend">\n' +
        '                                                                <span class="input-group-text" id="basic-addon1"> <i\n' +
        '                                                                            class="fa fa-clock"></i></span>\n' +
        '                                                            </div>\n' +
        '                                                            <input type="number" class="form-control"\n' +
        '                                                                   placeholder="Duration (in seconds)"\n' +
        '                                                                   aria-label="Username" name="duration[]"\n' +
        '                                                                   aria-describedby="basic-addon1" required>\n' +
        '                                                        </div>\n' +
        '                                                    </div>\n' +
        '                                                </div>\n' +
        '                                            </div>\n' +
        '                                            <div class="col-md-12">\n' +
        '                                                <textarea name="question[]" class="tinymce-enabled-message"\n' +
        '                                                          id="editor' + counter.text() + '"></textarea>\n' +
        '                                            </div>\n' +
        '\n' +
        '                                        </div>\n' +
        '                                        <hr>\n' +
        '                                        <h5 class="font-weight-bold">Enter Answer below</h5>\n' +
        '                                        <hr>\n' +
        '                                        <div class="row">\n' +
        '                                            <div class="col-md-1 pl-0">\n' +
        '                                                <input type="radio" class="form-control" name="realAnswerQ1" id="realAnswerQ' + counter.text() + '" value="0">\n' +
        '                                            </div>\n' +
        '                                            <div class="col-md-10 pl-0">\n' +
        '                                                <input type="text" required placeholder="" class="form-control"\n' +
        '                                                       name="answerQ' + counter.text() + '[]">\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                        <div class="row pt-2">\n' +
        '                                            <div class="col-md-1 pl-0">\n' +
        '                                                <input type="radio" class="form-control" name="realAnswerQ' + counter.text() + '" value="1">\n' +
        '                                            </div>\n' +
        '                                            <div class="col-md-10 pl-0">\n' +
        '                                                <input type="text" required placeholder="" class="form-control"\n' +
        '                                                       name="answerQ' + counter.text() + '[]">\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                        <div class="row pt-2">\n' +
        '                                            <div class="col-md-1 pl-0">\n' +
        '                                                <input type="radio" class="form-control" name="realAnswerQ' + counter.text() + '" value="2">\n' +
        '                                            </div>\n' +
        '                                            <div class="col-md-10 pl-0">\n' +
        '                                                <input type="text" required placeholder="" class="form-control"\n' +
        '                                                       name="answerQ' + counter.text() + '[]">\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                        <div class="row pt-2">\n' +
        '                                            <div class="col-md-1 pl-0">\n' +
        '                                                <input type="radio" class="form-control" name="realAnswerQ' + counter.text() + '" value="3">\n' +
        '                                            </div>\n' +
        '                                            <div class="col-md-10 pl-0">\n' +
        '                                                <input type="text" required placeholder="" class="form-control"\n' +
        '                                                       name="answerQ' + counter.text() + '[]">\n' +
        '                                            </div>\n' +
        '                                        </div>\n' +
        '                                    </div>';
    var p = parseInt(counter.text()) + 1;
    counter.text(p);
    $('#examquestions').append(questionfld);
    tinymce.EditorManager.createEditor('editor' + counter.text(), applyMCE());
    // tinyMCE.EditorManager.execCommand('mceAddEditor', false,'editor'+counter.text() );


});