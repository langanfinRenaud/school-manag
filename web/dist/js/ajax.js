var ajax = {
    createExamField: function () {
        var counter = $('#counter').text();
        $.ajax({
            type: "POST",
            url: Routing.generate('createExamField'),
            data: {
                counter: counter
            },
            success: function (data) {
                var p = parseInt(counter) + 1;
                $('#counter').text(p);

                $('#examquestions').append(data);
                AddRemoveTinyMce('txt_' + p);
            }
        });
    },
    filterCourseForRegistration: function () {
        var level = $('#level').val();
        var department = $('#department').val();
        $.ajax({
            type: "POST",
            url: Routing.generate('filterCourseForRegistration'),
            data: {
                level: level,
                department: department,
            },
            success: function (data) {
                $('#course-list').html(data);
            }
        });
    },

    filterStudentByDepartmentNlevel: function (mutiple= false) {
        var level = $('#level').val();
        var department = $('#department').val();
        $('#studentSelection').html('Please wait...');
        $.ajax({
            type: "POST",
            url: Routing.generate('filterStudentByDepartmentNlevel'),
            data: {
                level: level,
                department: department,
                mutiple: mutiple
            },
            success: function (data) {
                $('#studentSelection').html(data);
            }
        });
    },

    scoreBoardFilterStudentByDepartmentNlevel: function (mutiple= false) {
        var level = $('#level').val();
        var department = $('#department').val();
        $('#scoreArea').html('Please wait...');
        $.ajax({
            type: "POST",
            url: Routing.generate('filterScoreboardStudentByDepartmentNlevel'),
            data: {
                level: level,
                department: department,
                mutiple: mutiple
            },
            success: function (data) {
                $('#scoreArea').html(data);
            }
        });
    },

    getStudentInFo: function () {
        var matricNo = $('.studentSelection').val();
        console.log(matricNo);
        $('#displayStudentInfo').html('Please wait...');
        $.ajax({
            type: "POST",
            url: Routing.generate('getStudentInfo'),
            data: {
                matricNo: matricNo
            },
            success: function (data) {
                $('#displayStudentInfo').html(data);
            }
        });
    },
    getStudentInfoArch: function () {
        var matricNo = $('#appbundle_archive_student').val();
        $('#displayStudentInfo').html('Please wait...');
        // console.
        $.ajax({
            type: "POST",
            url: Routing.generate('getStudentInfoArch'),
            data: {
                matricNo: matricNo
            },
            success: function (data) {
                $('#displayStudentInfo').html(data);
            }
        });
    },
    getStudentScoreSheet: function () {
        var matricNo = $('#studentList').val();
        // console.
                $('#transPrev').html('Please wait...');

        $.ajax({
            type: "POST",
            url: Routing.generate('getStudentScoreSheet'),
            data: {
                matricNo: matricNo
            },
            success: function (data) {
                $('#transPrev').html(data);
            }
        });
    },

    filterStudent: function () {
        var matric=$('#matric').val();
        var lastname=$('#lastname').val();
        var firstname=$('#firstname').val();
        var adminType=$('#adminType').val();

        // console.log(ids,values)
        $('#output').html('Please wait...');

        $.ajax({
            type: "POST",
            url: Routing.generate('studentdynamicFilter'),
            data: {
                firstname: firstname,
                lastname: lastname,
                matric: matric,
                adminType: adminType,
            },
            success: function (data) {
                $('#output').html(data);
            }
        });
    },

    filterCourse: function () {
        var title=$('#title').val();
        var level=$('#level').val();
        var units=$('#units').val();

        // console.log(ids,values)
        $('#output').html('Please wait...');

        $.ajax({
            type: "POST",
            url: Routing.generate('coursedynamicFilter'),
            data: {
                title: title,
                level: level,
                units: units,
            },
            success: function (data) {
                $('#output').html(data);
            }
        });
    },

    addfees: function () {
        $.ajax({
            type: "POST",
            url: Routing.generate('addFees'),
            data: {},
            success: function (data) {
                $('#feeslist').append(data);
            }
        });
    }
    ,
    getAudit: function () {
        var fee_type=$('#fee_type').val();
        var fee_status=$('#fee_status').val();
        var startdate=$('#startdate').val();
        var enddate=$('#enddate').val();
        var student=$('#student').val();

        // console.log(student)
        $('#output').html('Please wait...');

        $.ajax({
            type: "POST",
            url: Routing.generate('auditFees'),
            data: {
                fee_type: fee_type,
                fee_status: fee_status,
                startdate: startdate,
                enddate: enddate,
                student: student,
            },
            success: function (data) {
                $('#output').html(data);
            }
        });
    },
};