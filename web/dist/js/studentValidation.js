function validateform() {
    var student_contact_email = document.forms["student"]["student_contact[email]"];

    if (student_contact_email.value.indexOf("@", 0) < 0 && student_contact_email.value.indexOf(".", 0) < 0) {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',

        });
        return false;
    }else{
        return true;
    }


}