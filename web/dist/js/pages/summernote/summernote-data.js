/**
 *  Document   : summernote-init.js
 *  Author     : redstar
 *  Description: script for set summernote properties
 *
 **/
$('#summernote').summernote({
        placeholder: '',
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
    ],
        tabsize: 2,
        height: 200
      });

$('.summernote_editor_descriptions').summernote({
    placeholder: '',
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
    ],
    tabsize: 2,
    height: 200
});
$('#formsummernote').summernote({
    placeholder: '',
    tabsize: 2,
    height: 350
  });